CREATE DATABASE asap_candidatos;
USE asap_candidatos;
CREATE TABLE IF NOT EXISTS Candidates (
    candidate_id INT AUTO_INCREMENT,
    candidate_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (candidate_id)
);