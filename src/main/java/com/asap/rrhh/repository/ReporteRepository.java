package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.inout.Diferencial;
import com.asap.rrhh.model.Actividad;

@Repository
public interface ReporteRepository extends JpaRepository<Actividad, Integer> {

	@Query(value = "CALL DIFERENCIAL_SEMANAL( :busquedaId )", nativeQuery = true)
	public List<Diferencial> getDiferenciaSemanal(@Param("busquedaId") int busquedaId);

	@Query(value = "CALL DIFERENCIAL_30DIAS( :busquedaId )", nativeQuery = true)
	public List<Diferencial> getDiferencia30Dias(@Param("busquedaId") int busquedaId);

	@Query(value = "CALL DIFERENCIAL_TOTAL( :busquedaId )", nativeQuery = true)
	public List<Diferencial> getDiferenciaTotal(@Param("busquedaId") int busquedaId);

}
