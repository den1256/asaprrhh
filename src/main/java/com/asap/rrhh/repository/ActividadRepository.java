package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Actividad;

@Repository
public interface ActividadRepository extends JpaRepository <Actividad, Integer> {

	@Query(value = "SELECT  *  FROM actividad WHERE busqueda_id = :busquedaId", nativeQuery = true)
	List<Actividad> getActividadPorBusqueda(@Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT * FROM actividad WHERE (fecha_hora >  CAST(:diaAnterior AS DATE)) and (fecha_hora < CAST(:diaPosterior AS DATE)) and busqueda_id = :busquedaId", nativeQuery = true)
	List<Actividad> getActividadesDiarias(@Param("diaAnterior") String diaAnterior, @Param("diaPosterior") String diaPosterior, @Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT busqueda_id FROM actividad WHERE (fecha_hora >  CAST(:diaAnterior AS DATE)) and (fecha_hora < CAST(:diaPosterior AS DATE)) GROUP BY busqueda_id", nativeQuery = true)
	List<Integer> getBusquedasIdDeActividades(@Param("diaAnterior") String diaAnterior, @Param("diaPosterior") String diaPosterior);
}
