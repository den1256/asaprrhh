package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Busqueda;

@Repository
public interface BusquedaRepository extends JpaRepository<Busqueda, Integer> {

	@Query(value = "call ELIMINARBUSQUEDA(:busquedaId)", nativeQuery = true)
	public int eliminarBusqueda(@Param("busquedaId") int busquedaId);
	
	@Query(value ="SELECT busqueda_id from busqueda WHERE busqueda_posicion LIKE :palabra", nativeQuery = true)
	List<Integer> getBusquedasIdByPosicion(@Param("palabra") String palabra);

}
