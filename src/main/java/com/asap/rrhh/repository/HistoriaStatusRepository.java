package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.HistoriaStatus;

@Repository
public interface HistoriaStatusRepository extends JpaRepository<HistoriaStatus, Integer>{

	@Query(value = "SELECT historia_status_id FROM  historia_status WHERE  candidato_busqueda_id = :candidatoBusquedaId", nativeQuery = true)
	public List<Object[]> getHistoriaCandidatoByBusq(@Param("candidatoBusquedaId") int candidatoBusquedaId);
	
	@Query(value = "call ELIMINARHISTORIACANDIDATO(:candidatoBusquedaId)", nativeQuery = true)
	public void borrarHistoriaCandidato(@Param("candidatoBusquedaId") int candidatoBusquedaId);
	
}
