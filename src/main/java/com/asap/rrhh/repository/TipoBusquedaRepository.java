package com.asap.rrhh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoBusqueda;

@Repository
public interface TipoBusquedaRepository extends JpaRepository<TipoBusqueda, Integer> {

}
