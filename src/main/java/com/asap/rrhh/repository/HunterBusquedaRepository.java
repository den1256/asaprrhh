package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.HunterBusqueda;

@Repository
public interface HunterBusquedaRepository extends JpaRepository<HunterBusqueda, Integer> {

	
	@Query(value = "SELECT hunter_id FROM hunter_busqueda where busqueda_id = :busquedaId AND hunter_busqueda_activo = 1", nativeQuery = true)
	List<Integer> getHuntersIdActuales(@Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT hunter_id FROM hunter_busqueda where busqueda_id = :busquedaId AND hunter_busqueda_activo = 0", nativeQuery = true)
	List<Integer> getHuntersIdInactivos(@Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT hunter_id FROM hunter_busqueda where busqueda_id =  :busquedaId", nativeQuery = true)
	List<Integer> getHuntersIdActualesTodos(@Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT * FROM hunter_busqueda where busqueda_id =  :busquedaId AND hunter_id = :hunterId", nativeQuery = true)
	List<HunterBusqueda> getHunterBusquedaPorIds(@Param("busquedaId") int busquedaId , @Param("hunterId") int hunterId);
	
	@Query(value = "SELECT hunter_busqueda_id FROM hunter_busqueda where busqueda_id =  :busquedaId AND hunter_id = :hunterId", nativeQuery = true)
	Integer getHunterBusquedaId(@Param("busquedaId") int busquedaId , @Param("hunterId") int hunterId);
	
	@Query(value = "SELECT busqueda_id FROM hunter_busqueda WHERE hunter_id = :hunterId AND hunter_busqueda_activo = 1", nativeQuery = true)
	List<Integer> getListaIdsBusquedasActivas(@Param("hunterId") int hunterId);
	
	@Query(value = "SELECT busqueda_id FROM hunter_busqueda WHERE hunter_id = :hunterId", nativeQuery = true)
	List<Integer> getListaIdsBusquedas(@Param("hunterId") int hunterId);

}
