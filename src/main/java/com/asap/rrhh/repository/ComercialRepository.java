package com.asap.rrhh.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Comercial;
import com.asap.rrhh.security.entity.Usuario;

@Repository
public interface ComercialRepository extends JpaRepository<Comercial, Integer> {

	Optional<Comercial> findByUsuario(Usuario usuarioId);

	@Query(value = "SELECT c.comercial_id, u.nombre, u.apellido, u.email, c.comercial_activo from comercial as c, usuario as u where  c.usuario_id = u.id order by u.nombre", nativeQuery = true)
	List<Object[]> getComerciales();

	@Query(value = "SELECT c.comercial_id, u.nombre, u.apellido, u.email, c.comercial_activo from comercial as c, usuario as u where  c.usuario_id = u.id  and comercial_activo = 1 order by u.nombre", nativeQuery = true)
	List<Object[]> getComercialesActivos();

}
