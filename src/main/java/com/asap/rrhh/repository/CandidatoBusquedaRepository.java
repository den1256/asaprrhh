package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.CandidatoBusqueda;

@Repository
public interface CandidatoBusquedaRepository extends JpaRepository<CandidatoBusqueda, Integer> {

	@Query(value = "SELECT busqueda_id, count(candidato_id) FROM candidato_busqueda WHERE  candidato_busqueda_activo = 1  GROUP BY busqueda_id", nativeQuery = true)
	List<Object[]> getCandidatosBusqueda();

	@Query(value = "SELECT count(candidato_id) FROM candidato_busqueda WHERE busqueda_id = :busquedaId AND candidato_busqueda_activo = 1", nativeQuery = true)
	int getCantCandidatosUnaBusqueda(@Param("busquedaId") int busquedaId);

	@Query(value = "SELECT  hunter_id, candidato_id  FROM candidato_busqueda WHERE busqueda_id = :busquedaId", nativeQuery = true)
	List<Object[]> getHunterCandidatosPorBusqueda(@Param("busquedaId") int busquedaId);

	// Duplicado de getHunterCandidatosPorBusqueda pero para solo los activos
	@Query(value = "SELECT  hunter_id, candidato_id  FROM candidato_busqueda WHERE busqueda_id = :busquedaId AND candidato_busqueda_activo = 1", nativeQuery = true)
	List<Object[]> getCandidatosDelHunter(@Param("busquedaId") int busquedaId);

	@Query(value = "SELECT candidato_busqueda_id FROM candidato_busqueda WHERE busqueda_id = :busquedaId AND candidato_id = :candidatoId AND hunter_id = :hunterId", nativeQuery = true)
	int getIdCandBusq(@Param("busquedaId") int busquedaId, @Param("candidatoId") int candidatoId, @Param("hunterId") int hunterId);

	@Query(value = "SELECT tipo_status_id FROM candidato_busqueda WHERE busqueda_id = :busquedaId AND candidato_id = :candidatoId AND hunter_id = :hunterId", nativeQuery = true)
	List<Object[]> getIdCandStatus(@Param("busquedaId") int busquedaId, @Param("candidatoId") int candidatoId, @Param("hunterId") int hunterId);

	@Query(value = "SELECT candidato_busqueda_id FROM candidato_busqueda WHERE busqueda_id = :busquedaId AND candidato_id = :candidatoId AND hunter_id = :hunterId", nativeQuery = true)
	List<Object[]> getIdCandidatoStatus(@Param("busquedaId") int busquedaId, @Param("candidatoId") int candidatoId, @Param("hunterId") int hunterId);

	@Query(value = "SELECT  busqueda_id FROM candidato_busqueda WHERE candidato_id = :candidatoId AND candidato_busqueda_activo = 1 GROUP BY busqueda_id", nativeQuery = true)
	List<Integer> getBusquedasPorCandidatoId(@Param("candidatoId") int candidatoId);

	@Query(value = "SELECT hunter_id FROM candidato_busqueda WHERE busqueda_id = :busquedaId AND candidato_id = :candidatoId GROUP BY hunter_id;", nativeQuery = true)
	List<Integer> getHuntersPorBusquedaYCandidato(@Param("busquedaId") int busquedaId, @Param("candidatoId") int candidatoId);

	@Query(value = "SELECT candidato_id FROM candidato_busqueda WHERE hunter_id = :hunterId AND busqueda_id = :busquedaId", nativeQuery = true)
	List<Integer> getCandidatosIdsDelHunter(@Param("busquedaId") int busquedaId, @Param("hunterId") int hunterId);

	@Query(value = "SELECT DISTINCT candidato_busqueda_id FROM asap_cv.candidato_busqueda \r\n" + "JOIN candidato \r\n"
			+ "WHERE (candidato_nombre LIKE %:param% OR candidato_apellido LIKE %:param%) \r\n" + "ORDER BY candidato_busqueda_id", nativeQuery = true)
	List<Integer> findBusquedaCandidato(@Param("param") String param);

	Integer countAllByCandidatoBusquedaBusquedaId_BusquedaIdAndCandidatoBusquedaStatusId_TipoStatusId(int candidatoBusquedaBusquedaId_busquedaId,
			int candidatoBusquedaStatusId_tipoStatusId);

	@Query(value = "select fecha_ingreso from candidato_busqueda where busqueda_id = :busquedaId and candidato_id = :candidatoId and hunter_id = :hunterId", nativeQuery = true)
	List<Object[]> getFechaDeIngreso(@Param("busquedaId") int busquedaId, @Param("hunterId") int hunterId, @Param("candidatoId") int candidatoId);


//	@Query(value = "SELECT DISTINCT cb.busqueda_id, hs.tipo_status_id, cb.candidato_id FROM candidato_busqueda cb, historia_status hs WHERE cb.candidato_busqueda_id = hs.candidato_busqueda_id AND candidato_busqueda_activo = 1 and hs.tipo_status_id IN (3,4)", nativeQuery = true)
	@Query(value = "SELECT DISTINCT cb.busqueda_id, cb.tipo_status_id, cb.candidato_id FROM candidato_busqueda cb WHERE candidato_busqueda_activo = 1 and cb.tipo_status_id IN (3,4)", nativeQuery = true)
	List<Object[]> getCandidatosStatusActual();
	
	@Query(value = "SELECT ts.tipo_status_descripcion FROM candidato_busqueda as cb,  tipo_status as ts WHERE cb.busqueda_id = :busquedaId AND cb.candidato_id = :candidatoId AND cb.hunter_id = :hunterId AND cb.tipo_status_id = ts.tipo_status_id", nativeQuery = true)
	List<Object[]> getCandidatoTipoStatusDescripcion (@Param("busquedaId") int busquedaId, @Param("candidatoId") int candidatoId, @Param("hunterId") int hunterId);

}
