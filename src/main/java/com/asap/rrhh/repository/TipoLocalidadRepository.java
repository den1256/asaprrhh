package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoLocalidad;

@Repository
public interface TipoLocalidadRepository  extends JpaRepository <TipoLocalidad, Integer>  { 

	 @Query(value ="SELECT * FROM tipo_localidad where localidad_activa = 1", nativeQuery = true)
		List<TipoLocalidad> getLocalidades();

}
