package com.asap.rrhh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoEstado;

@Repository
public interface TipoEstadoRepository  extends JpaRepository <TipoEstado, Integer> {

}
