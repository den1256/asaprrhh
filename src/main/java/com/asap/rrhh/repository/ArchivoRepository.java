package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Archivo;

@Repository
public interface ArchivoRepository extends JpaRepository<Archivo, Integer> {

	@Query(value = "SELECT  archivo_id  FROM archivo_busqueda WHERE busqueda_id = :busquedaId", nativeQuery = true)
	List<Object[]> getArchivoPorBusqueda(@Param("busquedaId") int busquedaId);

	@Query(value = "SELECT archivo_nombre FROM archivo WHERE archivo_id = :archivoId", nativeQuery = true)
	String getArchivoNombre(@Param("archivoId") int archivoId);

}