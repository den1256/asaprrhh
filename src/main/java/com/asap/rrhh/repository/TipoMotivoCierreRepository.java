package com.asap.rrhh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoMotivoCierre;

@Repository
public interface TipoMotivoCierreRepository extends JpaRepository<TipoMotivoCierre, Integer> {

}
