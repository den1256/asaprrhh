package com.asap.rrhh.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.ArchivoCvCandidato;

@Repository
public interface ArchivoCvCandidatoRepository extends JpaRepository<ArchivoCvCandidato, Integer>{
	
	@Query(value = "SELECT  archivo_id  FROM archivocv_candidato WHERE candidato_busqueda_id = :candidatoBusquedaId", nativeQuery = true)
	Optional <List<Integer>> getArchivoscv(@Param("candidatoBusquedaId") int candidatoBusquedaId);
	
	@Query(value = "call ELIMINAR_ARCHIVO_CANDIDATO(:archivoId)", nativeQuery = true)
	public void eliminarArchivoCandidato(@Param("archivoId") int archivoId);

}
