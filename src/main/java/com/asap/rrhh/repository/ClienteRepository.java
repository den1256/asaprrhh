package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Cliente;

@Repository
public interface ClienteRepository  extends JpaRepository<Cliente, Integer>  {

	@Query(value ="SELECT b.busqueda_id FROM cliente as c, busqueda as b WHERE c.cliente_id = b.cliente_id AND c.cliente_nombre LIKE :palabra", nativeQuery = true)
	List<Integer> getBusquedasIdByClienteNomb(@Param("palabra") String palabra);
	
	@Query(value ="SELECT * FROM cliente order by cliente_nombre", nativeQuery = true)
	List<Cliente> getClientes();
	
	@Query(value ="SELECT * FROM cliente where cliente_activo = 1 order by cliente_nombre", nativeQuery = true)
	List<Cliente> getClientesActivos();

}
