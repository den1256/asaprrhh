package com.asap.rrhh.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.ArchivoBusqueda;

@Repository
public interface ArchivoBusquedaRepository extends JpaRepository<ArchivoBusqueda, Integer> {
	
	@Query(value = "SELECT  archivo_id  FROM archivo_busqueda WHERE busqueda_id = :busquedaId", nativeQuery = true)
	List<Object[]> getArchivoPorBusqueda(@Param("busquedaId") int busquedaId);

	Optional<List<ArchivoBusqueda>> findAllByBusqueda_BusquedaId(int busqueda_busquedaId);
	
	@Query(value = "call ELIMINAR_ARCHIVO_BUSQUEDA(:archivoId)", nativeQuery = true)
	public void eliminarArchivoBusqueda(@Param("archivoId") int archivoId);

}
