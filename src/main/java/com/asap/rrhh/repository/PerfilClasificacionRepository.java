package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.PerfilClasificacion;

@Repository
public interface PerfilClasificacionRepository extends JpaRepository<PerfilClasificacion, Integer>{
	

	@Query(value = "select * from perfil_clasificacion where perfil_id = :perfilId order by  perfil_clasificacion_nombre", nativeQuery = true)
	List<PerfilClasificacion> getPerfilesPorClasificacion(@Param("perfilId") int perfilId);
	
}
