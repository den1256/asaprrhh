package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoStatus;

@Repository
public interface TipoStatusRepository extends JpaRepository<TipoStatus, Integer> {

	@Query(value = "select * from tipo_status order by tipo_status_orden", nativeQuery = true)
	List<TipoStatus> getTipoStatus();
	
}
