package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoTecnologia;

@Repository
public interface TecnologiaRepository extends JpaRepository<TipoTecnologia, Integer>{
	
	@Query(value = "select * from tipo_tecnologia order by tecnologia_nombre", nativeQuery = true)
	List<TipoTecnologia> getTipoTecnologia();

}
