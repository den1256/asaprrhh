package com.asap.rrhh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoSeniority;

@Repository
public interface SeniorityRepository extends JpaRepository<TipoSeniority, Integer> {

}
