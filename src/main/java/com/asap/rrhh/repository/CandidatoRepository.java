package com.asap.rrhh.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Candidato;

@Repository
public interface CandidatoRepository extends JpaRepository<Candidato, Integer> {

	@Query(value = "SELECT * FROM candidato WHERE candidato_linkedin = :LinkedIn", nativeQuery = true)
	Optional<List<Candidato>> findByLinkedIn(@Param("LinkedIn") String linkedIn);

	Candidato findByCandidatoEmail(String candidatoEmail);

	Candidato findByCandidatoCelularLike(String candidatoCelular);
	
	@Query(value = "SELECT * FROM candidato WHERE candidato_nombre LIKE :palabra OR candidato_apellido LIKE :palabra OR candidato_email LIKE :palabra OR candidato_celular LIKE :palabra OR candidato_linkedin LIKE :palabra", nativeQuery = true)
	Optional<List<Candidato>> buscarCandidatos(@Param("palabra") String palabra);
	
	@Query(value = "SELECT cb.busqueda_id FROM candidato as c, candidato_busqueda as cb WHERE cb.candidato_id = c.candidato_id AND (candidato_nombre LIKE :palabra OR candidato_apellido LIKE :palabra) GROUP BY cb.busqueda_id", nativeQuery = true)
	List<Integer> getBusquedasIdByCandidato(@Param("palabra") String palabra);
	
}