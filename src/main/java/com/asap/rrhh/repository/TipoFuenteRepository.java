package com.asap.rrhh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoFuente;

@Repository
public interface TipoFuenteRepository extends JpaRepository<TipoFuente, Integer>{

}
