package com.asap.rrhh.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Hunter;

@Repository
public interface HunterRepository  extends JpaRepository<Hunter, Integer> {

    Hunter findByUsuario_Id(int idUsuario_id);

    Optional<Hunter> findByUsuario_NombreUsuario(String usuario_nombreUsuario);
    
    @Query(value ="SELECT h.hunter_id, u.nombre, u.apellido, u.email, h.hunter_activo from hunter as h, usuario as u where  h.usuario_id = u.id  order by u.nombre", nativeQuery = true)
	List<Object[]> getHunters();
    
    @Query(value ="SELECT h.hunter_id, u.nombre, u.apellido, u.email, h.hunter_activo from hunter as h, usuario as u where  h.usuario_id = u.id  and hunter_activo = 1 order by u.nombre", nativeQuery = true)
	List<Object[]> getHuntersActivos();
    
    @Query(value ="SELECT hb.busqueda_id  FROM usuario as u, hunter as h, hunter_busqueda as hb WHERE u.id = h.usuario_id  AND h.hunter_id = hb.hunter_id AND (u.nombre LIKE :palabra OR u.apellido LIKE :palabra) GROUP BY hb.busqueda_id", nativeQuery = true)
	List<Integer> getBusquedasIdByHunterNombreApellido(@Param("palabra") String palabra);
}
