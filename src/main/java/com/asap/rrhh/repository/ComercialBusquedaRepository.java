package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.Comercial;
import com.asap.rrhh.model.ComercialBusqueda;

@Repository
public interface ComercialBusquedaRepository extends JpaRepository<ComercialBusqueda, Integer>{

	@Query(value ="SELECT * from comercial where comercial_activo = 1", nativeQuery = true)
	List<Comercial> getComercialesActivos();
	
	@Query(value = "SELECT * FROM comercial_busqueda where busqueda_id =  :busquedaId AND comercial_id = :comercialId", nativeQuery = true)
	ComercialBusqueda getComercialBusquedaByIDs(@Param("busquedaId") int busquedaId , @Param("comercialId") int comercialId);
	
	@Query(value = "SELECT comercial_id FROM comercial_busqueda where busqueda_id = :busquedaId AND comercial_busqueda_activo = 1", nativeQuery = true)
	List<Integer> getComercialesIdActuales(@Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT comercial_id FROM comercial_busqueda where busqueda_id = :busquedaId AND comercial_busqueda_activo = 0", nativeQuery = true)
	List<Integer> getComercialesIdInactivos(@Param("busquedaId") int busquedaId);
	
	@Query(value = "SELECT * FROM comercial_busqueda where busqueda_id =  :busquedaId AND comercial_id = :comercialId", nativeQuery = true)
	List<ComercialBusqueda> getComercialBusquedaPorIds(@Param("busquedaId") int busquedaId , @Param("comercialId") int comercialId);
	
	@Query(value = "SELECT busqueda_id FROM comercial_busqueda WHERE comercial_id = :comercialId AND comercial_busqueda_activo = 1", nativeQuery = true)
	List<Integer> getListaIdsBusquedasActivas(@Param("comercialId") int comercialId);
}
