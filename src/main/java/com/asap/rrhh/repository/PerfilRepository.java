package com.asap.rrhh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.model.TipoPerfil;

@Repository
public interface PerfilRepository extends JpaRepository <TipoPerfil, Integer>{

	@Query(value = "select * from tipo_perfil order by perfil_nombre", nativeQuery = true)
	List<TipoPerfil> getTipoPerfil();
}
