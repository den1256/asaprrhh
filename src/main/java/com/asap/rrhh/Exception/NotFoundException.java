package com.asap.rrhh.Exception;

import com.asap.rrhh.inout.ExceptionDetail.CodigoError;

public class NotFoundException extends RuntimeException {
	

	private static final long serialVersionUID = 4666164493966419509L;
	private CodigoError codigo;

	public NotFoundException(CodigoError codigo) {
		super(codigo.getMessage());
		this.codigo = codigo;
	}

	public CodigoError getCodigo() {
		return codigo;
	}
	
	@Override
	public String getMessage() {
		return this.getCodigo().getMessage();
	}

}
