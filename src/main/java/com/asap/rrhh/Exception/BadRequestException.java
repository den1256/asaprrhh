package com.asap.rrhh.Exception;

import com.asap.rrhh.inout.ExceptionDetail;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;

public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = -7405579429387716682L;
    private ExceptionDetail.CodigoError codigo;

    public BadRequestException(CodigoError codigo) {
        super();
        this.codigo = codigo;
    }

    public CodigoError getCodigo() {
        return codigo;
    }
}
