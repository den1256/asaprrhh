package com.asap.rrhh.security.service;

import java.util.Optional;
import java.util.UUID;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.repository.UsuarioRepository;

@Service
public class ForgotPasswordService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Value("${app.url}")
	private String appUrl;

	@Autowired
	private EmailService emailService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	Logger logger = LogManager.getLogger(getClass());

	public void processForgotPassword(String userEmail) throws Exception {

		// Lookup user in database by e-mail
		Optional<Usuario> optional = usuarioRepository.findAutenticableByEmail(userEmail);

		if (!optional.isPresent()) {
			throw new NotFoundException(CodigoError.EMAIL_RESET_NOT_EXIST);
		} else {

			// Generate random 36-character string token for reset password
			Usuario user = optional.get();
			user.setResetToken(UUID.randomUUID().toString());

			// Save token to database
			usuarioRepository.save(user);
			logger.info(String.format("%s ha solicitado resetear constraseña", user));
			// Email message
			String subject = "Solicitud de recuperación de contraseña";
			String body = "<p>Para resetear tu contraseña, presiona sobre el link:\n" + "<a href=\""+ this.appUrl + "/#/reset-password/" + user.getResetToken() + "\">Resetear Contraseña</a></p>";

			emailService.sendEmail(user.getEmail(), subject, body);
		}
	}

	public void setNewPassword(String token, String password) throws Exception {
		// Find the user associated with the reset token
		Optional<Usuario> user = usuarioRepository.findByResetToken(token);

		// This should always be non-null but we check just in case
		if (user.isPresent()) {

			Usuario resetUser = user.get();

			// Set new password
			resetUser.setPassword(passwordEncoder.encode(password));

			// Set the reset token to null so it cannot be used again
			resetUser.setResetToken(null);

			// Save user
			usuarioRepository.save(resetUser);
			logger.info(String.format("%s ha cambiado constraseña", user));

		} else {
			throw new NotFoundException(CodigoError.TOKEN_RESET_NOT_EXIST);
		}

	}

}
