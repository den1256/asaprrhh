package com.asap.rrhh.security.service;

import java.util.Objects;
import java.util.Optional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.repository.UsuarioRepository;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	UsuarioRepository usuarioRepository;

	Logger logger = LogManager.getLogger(getClass());

	public Optional<Usuario> getByNombreUsuario(String nombreUsuario) {
		return usuarioRepository.findByNombreUsuario(nombreUsuario);
	}

	public boolean existsByNombreUsuario(String nombreUsuario) {
		return usuarioRepository.existsByNombreUsuario(nombreUsuario);
	}

	public boolean existsByEmail(String email) {
		return usuarioRepository.existsByEmail(email);
	}

	public void save(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	public Usuario getUserData(String nombreUsuario) throws NotFoundException {
		Usuario usuario = usuarioRepository.findByNombreUsuario(nombreUsuario)
				.orElseThrow(() -> new NotFoundException(CodigoError.USER_NOT_FOUND));
		logger.trace(String.format("getUserData(%s)", usuario));
		return usuario;
	}

	public void setNuevaContrasena(String nombreUsuario, String contrasenaAnterior, String contrasenaNueva) throws Exception {
		Objects.requireNonNull(contrasenaNueva);
		authenticate(nombreUsuario, contrasenaAnterior);

		Usuario usuario = usuarioRepository.findByNombreUsuario(nombreUsuario).get();
		usuario.setPassword(passwordEncoder.encode(contrasenaNueva));
		usuario = usuarioRepository.save(usuario);

		logger.trace(String.format("%s ha cambiado su contraseña!", usuario));
	}

	private void authenticate(String username, String password) throws BadCredentialsException {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		logger.trace(String.format("Autenticando usuario '%s'", username));
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	}

	public Usuario findById (int idUsuario) throws NotFoundException {
		return usuarioRepository.findById(idUsuario).orElseThrow(() -> new NotFoundException(CodigoError.USER_NOT_FOUND));
	}
}
