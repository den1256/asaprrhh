package com.asap.rrhh.security.service;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

@Service("emailService")
public class SendGridEmailService implements EmailService {

	@Value("${email.from_email}")
	private String from_email;

	@Value("${email.from_name}")
	private String from_name;

	@Value("${email.debug.enabled}")
	private boolean emailDebug;

	@Value("${email.debug.to_email}")
	private String emailDebugToEmail;

	@Autowired
	SendGrid sendgrid;

	Logger logger = LogManager.getLogger(SendGridEmailService.class);

	public void sendEmail(String to_email, String subject, String body) throws Exception {
//		if (this.emailDebug && !this.emailDebugToEmail.isEmpty()) {
//			logger.warn(String.format("Usando destinatario %s en lugar de %s", this.emailDebugToEmail, to_email));
//			to_email = this.emailDebugToEmail;
//		}

		Email from = new Email(this.from_email, this.from_name);
		Email to = new Email(to_email);
		Content content = new Content("text/html", body);
		Mail email = new Mail(from, subject, to, content);

		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(email.build());
			Response response = sendgrid.api(request);
			logger.info(String.format("Se ha enviado un email a %s", to_email));
			logger.debug(response.getStatusCode());
			logger.debug(response.getBody());
			logger.debug(response.getHeaders());
		} catch (IOException ex) {
			logger.error(ex); 
			throw ex;
		}
	}

}
