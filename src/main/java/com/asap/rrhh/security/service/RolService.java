package com.asap.rrhh.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asap.rrhh.security.entity.TipoRol;
import com.asap.rrhh.security.enums.RolNombre;
import com.asap.rrhh.security.repository.RolRepository;

@Service
@Transactional
public class RolService {

	@Autowired
	RolRepository rolRepository;

	public Optional<TipoRol> getByRolNombre(RolNombre rolNombre) {
		return rolRepository.findByRolNombre(rolNombre);
	}
	
	public void save(TipoRol tipoRol) {
		rolRepository.save(tipoRol);
	}

}
