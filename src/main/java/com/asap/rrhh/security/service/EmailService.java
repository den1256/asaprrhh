package com.asap.rrhh.security.service;

public interface EmailService {
	
	public void sendEmail(String to_email, String subject, String body) throws Exception;

}
