package com.asap.rrhh.security.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.dto.Mensaje;
import com.asap.rrhh.inout.MessageResponse;
import com.asap.rrhh.security.dto.JwtDto;
import com.asap.rrhh.security.dto.LoginUsuario;
import com.asap.rrhh.security.dto.NuevoUsuario;
import com.asap.rrhh.security.entity.TipoRol;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.enums.RolNombre;
import com.asap.rrhh.security.inout.ResetPasswordRequest;
import com.asap.rrhh.security.jwt.JwtProvider;
import com.asap.rrhh.security.service.ForgotPasswordService;
import com.asap.rrhh.security.service.RolService;
import com.asap.rrhh.security.service.UsuarioService;

@RestController
@RequestMapping("/auth")
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class AuthController {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private ForgotPasswordService forgotPasswordService;

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	RolService rolService;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/crearUsuario")
	public ResponseEntity<?> crearUsuario(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult) {

		nuevoUsuario.setNombre(nuevoUsuario.getNombre().trim());
		nuevoUsuario.setApellido(nuevoUsuario.getApellido().trim());
		nuevoUsuario.setEmail(nuevoUsuario.getEmail().trim());
		nuevoUsuario.setNombreUsuario(nuevoUsuario.getNombreUsuario().trim());

		if (bindingResult.hasErrors())
			return new ResponseEntity<Mensaje>(new Mensaje("campos o email invalidos"), HttpStatus.BAD_REQUEST);

		if (usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
			return new ResponseEntity<Mensaje>(new Mensaje("el nombre de usuario ya existe"), HttpStatus.BAD_REQUEST);

		if (usuarioService.existsByEmail(nuevoUsuario.getEmail()))
			return new ResponseEntity<Mensaje>(new Mensaje("la dirección email ya existe"), HttpStatus.BAD_REQUEST);

		Usuario usuario = new Usuario(nuevoUsuario.getNombre().trim(), nuevoUsuario.getApellido(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(),
				passwordEncoder.encode(nuevoUsuario.getPassword()));

		Set<TipoRol> roles = new HashSet<>();

		if (nuevoUsuario.getRoles().contains("admin"))
			roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());

		if (nuevoUsuario.getRoles().contains("user"))
			roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());

		if (nuevoUsuario.getRoles().contains("comercial"))
			roles.add(rolService.getByRolNombre(RolNombre.ROLE_COMERCIAL).get());

		usuario.setRoles(roles);

		usuarioService.save(usuario);

		return new ResponseEntity<Mensaje>(new Mensaje("usuario guardado"), HttpStatus.CREATED);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/login")
	public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult) {

		if (bindingResult.hasErrors())
			return new ResponseEntity(new Mensaje("campos invalidos"), HttpStatus.BAD_REQUEST);

		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generatedToken(authentication);

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());

		return new ResponseEntity(jwtDto, HttpStatus.OK);
	}

	@PostMapping(value = "/setForgotPassword")
	public ResponseEntity<?> processForgotPassword(@RequestParam("email") String userEmail) throws Exception {
		forgotPasswordService.processForgotPassword(userEmail);
		return ResponseEntity.ok(new MessageResponse("Se envió un link de reseteo de contraseña a " + userEmail + "."));
	}

	@PostMapping(value = "/setNuevaContrasena")
	ResponseEntity<?> setNuevaContrasena(Authentication auth, @RequestParam String contrasenaAnterior, @RequestParam String contrasenaNueva) throws Exception {
		Usuario usuario = usuarioService.getUserData(auth.getName());
		usuarioService.setNuevaContrasena(usuario.getNombreUsuario(), contrasenaAnterior, contrasenaNueva);
		return new ResponseEntity<MessageResponse>(new MessageResponse("La contraseña se ha actualizado"), HttpStatus.OK);
	}

	@PostMapping(value = "/setResetPassword")
	public ResponseEntity<?> setNewPassword(@RequestBody ResetPasswordRequest resetPasswordRequest) throws Exception {
		forgotPasswordService.setNewPassword(resetPasswordRequest.getToken(), resetPasswordRequest.getPassword());
		return ResponseEntity.ok(new MessageResponse("Se ha cambiado la contraseña con éxito."));
	}

}
