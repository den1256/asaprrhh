package com.asap.rrhh.security.inout;

public class ResetPasswordRequest {

	protected String token;
	protected String password;

	public ResetPasswordRequest() {
		super();
	}

	public ResetPasswordRequest(String token, String password) {
		super();
		this.token = token;
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public String getPassword() {
		return password;
	}

}
