package com.asap.rrhh.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sendgrid.SendGrid;

@Configuration
public class EmailConfig {

	@Value("${email.api_key}")
	private String api_key;
	
	@Bean
	public SendGrid getSendGrid() {
		return new SendGrid(this.api_key);
	}
	
	
}
