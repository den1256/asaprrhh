package com.asap.rrhh.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.security.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	Optional<Usuario> findByNombreUsuario(String nombreUsuario);

	boolean existsByNombreUsuario(String nombreUsuario);

	boolean existsByEmail(String email);
	
	Optional<Usuario> findAutenticableByEmail(String email);
	
	//Optional<Usuario> findAutenticableByNombreUsuario(String nombreUsuario);
	
	Optional<Usuario> findByResetToken(String resetToken);

}
