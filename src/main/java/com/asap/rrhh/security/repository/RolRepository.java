package com.asap.rrhh.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asap.rrhh.security.entity.TipoRol;
import com.asap.rrhh.security.enums.RolNombre;

@Repository
public interface RolRepository extends JpaRepository<TipoRol, Integer> {

	Optional<TipoRol> findByRolNombre(RolNombre rolNombre);

	boolean existsByRolNombre(RolNombre rolNombre);
	
}
