package com.asap.rrhh.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class DateUtil {

	public Timestamp timeNow() {
		// ZonedDateTime nowInBsAs = ZonedDateTime.now(ZoneId.of("America/Argentina/Buenos_Aires"));
		LocalDateTime nowInBsAs = LocalDateTime.now();
		return Timestamp.valueOf(nowInBsAs);
	}

}
