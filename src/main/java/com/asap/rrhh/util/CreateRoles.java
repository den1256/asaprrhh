package com.asap.rrhh.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.asap.rrhh.security.entity.TipoRol;
import com.asap.rrhh.security.enums.RolNombre;
import com.asap.rrhh.security.repository.RolRepository;

@Component
public class CreateRoles implements CommandLineRunner {

	@Autowired
	RolRepository rolRepository;

	@Override
	public void run(String... args) {

		/*
		 * rolService.getByRolNombre(RolNombre.ROLE_COMERCIAL).orElseGet(() -> { return rolRepository.save(new Rol(RolNombre.ROLE_COMERCIAL)); });
		 */

		if (!rolRepository.existsByRolNombre(RolNombre.ROLE_COMERCIAL)) {
			rolRepository.save(new TipoRol(RolNombre.ROLE_COMERCIAL));
		}
	}
}
