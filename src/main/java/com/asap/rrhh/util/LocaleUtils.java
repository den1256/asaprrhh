package com.asap.rrhh.util;

import java.text.NumberFormat;
import java.util.Locale;

public class LocaleUtils {

	static Locale[] locales = null;
	static NumberFormat form = null;

	public String formatDecimal(double val) {
		if (locales == null) {
			locales = NumberFormat.getAvailableLocales();
			for (int i = 0; i < locales.length; ++i) {
				if (locales[i].getCountry().length() == 0) {
					continue; // Skip language-only locales
				}
				if (locales[i].getDisplayName().trim().toLowerCase().indexOf("argentina") > 0) {
					form = NumberFormat.getCurrencyInstance(locales[i]);
					form.setMinimumFractionDigits(0);
					break;
				}
			}
		}
		return form.format(val);
	}

	public String getNullIfEmpty(String val) {
		String resultado = null;
		if (val != null && !val.trim().isEmpty())
			resultado = val.trim();
		return resultado;
	}

}
