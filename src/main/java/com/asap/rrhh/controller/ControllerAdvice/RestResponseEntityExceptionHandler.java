package com.asap.rrhh.controller.ControllerAdvice;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.asap.rrhh.Exception.BadRequestException;
import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ExceptionDetail;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;

@ControllerAdvice
@RestController
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private Environment env;

	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<ExceptionDetail> handleNotFoundException(NotFoundException ex, WebRequest request) {
		ExceptionDetail ed = new ExceptionDetail(HttpStatus.NOT_FOUND, ex.getCodigo(), ex.getMessage(), ex.getStackTrace());
		logthis(ex);
		return new ResponseEntity<ExceptionDetail>(ed, ed.getStatus());
	}

	@ExceptionHandler(BadCredentialsException.class)
	public final ResponseEntity<ExceptionDetail> handleBadCredentialsException(BadCredentialsException ex, WebRequest request) {
		ExceptionDetail ed = new ExceptionDetail(HttpStatus.UNAUTHORIZED, CodigoError.BAD_CREDENTIAL_EXCEPTION, ex.getMessage(), ex.getStackTrace());
		logthis(ex);
		return new ResponseEntity<ExceptionDetail>(ed, ed.getStatus());
	}

	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<ExceptionDetail> handleBadRequestException(BadRequestException ex, WebRequest request) {
		ExceptionDetail ed = new ExceptionDetail(HttpStatus.BAD_REQUEST, ex.getCodigo(), ex.getMessage(), ex.getStackTrace());
		logthis(ex);
		return new ResponseEntity<ExceptionDetail>(ed, ed.getStatus());
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		ExceptionDetail ed = new ExceptionDetail(HttpStatus.BAD_REQUEST, CodigoError.ARGUMENT_NOT_VALID_EXCEPTION, ex.getMessage(), ex.getStackTrace());
		logthis(ex);
		return new ResponseEntity<Object>(ed, ed.getStatus());
	}

	private void logthis(Exception ex) {
		String ambiente = env.getProperty("ambiente");
		logger.error(ambiente + " " + String.format("Internal server error caused by: %s", ex));
		logger.error(ambiente + " " + ExceptionUtils.getStackTrace(ex));

	}

}
