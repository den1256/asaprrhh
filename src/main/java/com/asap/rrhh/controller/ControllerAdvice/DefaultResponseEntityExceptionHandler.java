package com.asap.rrhh.controller.ControllerAdvice;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.asap.rrhh.inout.ExceptionDetail;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;

@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class DefaultResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	Logger logger = LogManager.getLogger(getClass());

	@Autowired
	private Environment env;

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionDetail> handleAllException(Exception ex, WebRequest request) {
		ExceptionDetail ed = new ExceptionDetail(HttpStatus.INTERNAL_SERVER_ERROR, CodigoError.INTERNAL_SERVER_ERROR, ex.getMessage(), ex.getStackTrace());

		String ambiente = env.getProperty("ambiente");
		logger.error(ambiente + " " + String.format("Internal server error caused by: %s", ex));
		logger.error(ambiente + " " + ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<ExceptionDetail>(ed, ed.getStatus());
	}
}
