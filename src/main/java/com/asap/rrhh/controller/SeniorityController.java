package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.TipoSeniority;
import com.asap.rrhh.repository.SeniorityRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class SeniorityController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	SeniorityRepository seniorityRepository;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getSeniority")
	public ResponseEntity<?> getSeniority(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getSeniority");

		List<TipoSeniority> tipoSeniority = seniorityRepository.findAll();

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(tipoSeniority);
		return ResponseEntity.ok(response);
	}

}
