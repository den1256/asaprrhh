package com.asap.rrhh.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ActividadResponse;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.service.ActividadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class ActividadController extends BaseController {

	@Autowired
	ActividadService actividadService;

	@Autowired
	ObjectMapper objectMapper;

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER') OR hasRole('COMERCIAL')")
	@GetMapping("/getActividad")
	public ResponseEntity<?> getActividad(@RequestParam int busquedaId, HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getActividad");

		ActividadResponse actividades = actividadService.getActividadesBusqueda(busquedaId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(actividades);
		return ResponseEntity.ok(response);
	}
}
