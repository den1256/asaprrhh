package com.asap.rrhh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.dto.Mensaje;
import com.asap.rrhh.inout.AsignarArchivoBusquedaRequest;
import com.asap.rrhh.inout.AsignarCandidatoBusqueda;
import com.asap.rrhh.inout.AsignarHunterBusqueda;
import com.asap.rrhh.inout.BuscarStringRequest;
import com.asap.rrhh.inout.DesactivarCandidatoRequest;
import com.asap.rrhh.inout.DetalleBusqueda;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.inout.HistoriaStatusCandidato;
import com.asap.rrhh.inout.HunterCandidato;
import com.asap.rrhh.inout.RemplazoHunterResquest;
import com.asap.rrhh.inout.TotalDetalleBusqueda;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.Archivo;
import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.CandidatoBusqueda;
import com.asap.rrhh.model.TipoBusqueda;
import com.asap.rrhh.model.TipoEstado;
import com.asap.rrhh.repository.ArchivoBusquedaRepository;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.CandidatoBusquedaRepository;
import com.asap.rrhh.repository.CandidatoRepository;
import com.asap.rrhh.repository.HistoriaStatusRepository;
import com.asap.rrhh.repository.HunterBusquedaRepository;
import com.asap.rrhh.repository.HunterRepository;
import com.asap.rrhh.repository.TipoBusquedaRepository;
import com.asap.rrhh.repository.TipoEstadoRepository;
import com.asap.rrhh.repository.TipoStatusRepository;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.repository.UsuarioRepository;
import com.asap.rrhh.service.ActividadService;
import com.asap.rrhh.service.BusquedaService;
import com.asap.rrhh.service.CandidatoBusquedaService;
import com.asap.rrhh.util.DateUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://35.230.70.42", "http://localhost:4200",
		"http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class BusquedaController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	HunterBusquedaRepository hunterBusquedaRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	BusquedaRepository busquedaRepository;

	@Autowired
	CandidatoRepository candidatoRepository;

	@Autowired
	TipoEstadoRepository tipoEstadoRepository;

	@Autowired
	CandidatoBusquedaRepository candidatoBusquedaRepository;

	@Autowired
	TipoStatusRepository tipoStatusRepository;

	@Autowired
	TipoBusquedaRepository tipoBusquedaRepository;

	@Autowired
	HistoriaStatusRepository historiaStatusRepository;

	@Autowired
	ArchivoBusquedaRepository archivoBusquedaRepository;

	@Autowired
	ActividadService actividadService;

	@Autowired
	BusquedaService busquedaService;

	@Autowired
	CandidatoBusquedaService candidatoBusquedaService;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getBusquedas")
	public ResponseEntity<?> getBusquedas(HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getBusquedas");

		Usuario usuarioActivo = usuarioRepository.findByNombreUsuario(nombreUsuario).orElseThrow(() -> new NotFoundException(CodigoError.USER_NOT_FOUND));
		List<Integer> noBusquedasId = new ArrayList<>();
		TotalDetalleBusqueda totalDetalleBusqueda = busquedaService.getTotalDetalleBusqueda(usuarioActivo, noBusquedasId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(totalDetalleBusqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/postBusqueda")
	public ResponseEntity<?> crearBusqueda(@Valid @RequestBody Busqueda busqueda, HttpServletRequest request) throws NotFoundException {
		LOG_info(request, "PETICION: /postBusqueda");

		busqueda.setPosicion(busqueda.getPosicion().trim());
		boolean nuevaBusqueda = false;
		if (busqueda.getBusquedaFechaInicio() == null) {
			busqueda.setBusquedaFechaInicio(new DateUtil().timeNow());
		}

		if (busqueda.getBusquedaId() != 0) {
			int cantCandidatos = candidatoBusquedaService.contarCandidatos(busqueda.getBusquedaId());
			busqueda.setCantidadCandidatosSeleccionados(cantCandidatos);
		} else {
			nuevaBusqueda = true;
		}

		busquedaRepository.save(busqueda);
		int busquedaID = busqueda.getBusquedaId();
		if (nuevaBusqueda) {
			actividadService.guardarActividad(
					busquedaID,
					busqueda.getPosicion() + " ( " + busqueda.getBusquedaId() + " )",
					getHunterNombreCompleto(request) + " creó la Búsqueda");
		}

		return ResponseEntity.ok(busquedaID);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/asignarHuntersBusqueda")
	public ResponseEntity<?> asignarHuntersBusqueda(@Valid @RequestBody AsignarHunterBusqueda asignarHunterBusqueda, HttpServletRequest request) {
		LOG_info(request, "PETICION: /asignarHuntersBusqueda");

		Integer cantidadHunterNoAsignado = busquedaService.getHunterCandidate(asignarHunterBusqueda);

		if (cantidadHunterNoAsignado > 0) {
			return new ResponseEntity<Mensaje>(new Mensaje("Uno o más Recruiters ya existen en la búsqueda"), HttpStatus.OK);
		}

		return new ResponseEntity<Mensaje>(new Mensaje("El Recruiter se asignó a la búsqueda"), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getBusqueda/{busquedaId}")
	public ResponseEntity<?> getBusqueda(@PathVariable int busquedaId, HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getBusqueda por Id: " + busquedaId);

		DetalleBusqueda detalleBusqueda = busquedaService.getDetalleBusqueda(busquedaId);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(detalleBusqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getDetalleBusqueda/{busquedaId}")
	public ResponseEntity<?> getDetalleBusqueda(@PathVariable(value = "busquedaId") int busquedaId, HttpServletRequest request)
			throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getDetalleBusqueda: Hunters por Id Busqueda: " + busquedaId);

		List<HunterCandidato> listaFinal = busquedaService.getHunterCandidatos(busquedaId, request);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(listaFinal);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/getTipoEstadoBusq")
	public ResponseEntity<?> getTipoEstadoBusq(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getTipoEstadoBusq");

		List<TipoEstado> tipoEstado = tipoEstadoRepository.findAll();
		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(tipoEstado);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getTipoBusqueda")
	public ResponseEntity<?> getTipoBusqueda(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getTipoBusqueda");

		List<TipoBusqueda> tipoBusqueda = tipoBusquedaRepository.findAll();
		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(tipoBusqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER')")
	@PostMapping("/asignarCandidatosBusqueda")
	public ResponseEntity<?> asignarCandidatosBusqueda(@RequestBody AsignarCandidatoBusqueda asignarcandidatoBusqueda, HttpServletRequest request) {
		LOG_info(request, "PETICION: /asignarCandidatosBusqueda");

		List<Integer> candidatos = candidatoBusquedaRepository.getCandidatosIdsDelHunter(
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaBusquedaId().getBusquedaId(),
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaHunterId().getHunterId());

		Integer candidatoId = asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaCandidatoId().getCandidatoId();
		int activo = 2;
		try {
			int candBusqId = candidatoBusquedaRepository.getIdCandBusq(
					asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaBusquedaId().getBusquedaId(),
					candidatoId,
					asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaHunterId().getHunterId());
			CandidatoBusqueda candidatoBusqueda = candidatoBusquedaRepository.getOne(candBusqId);
			activo = candidatoBusqueda.getCandidatoBusquedaActivo();
		} catch (Exception e) {

		}

		if (!busquedaService.buscarEnLista(candidatos, candidatoId)) {
			busquedaService.candidatoBusqueda(asignarcandidatoBusqueda);
			return new ResponseEntity<Mensaje>(new Mensaje("El candidato se asignó a la búsqueda"), HttpStatus.CREATED);
		} else if (activo == 0) {
			busquedaService.activarCandidatoBusqueda(asignarcandidatoBusqueda);
			return new ResponseEntity<Mensaje>(new Mensaje("El candidato se reasignó a la búsqueda"), HttpStatus.CREATED);
		}
		return new ResponseEntity<Mensaje>(new Mensaje("El candidato ya esta asignado a esta búsqueda"), HttpStatus.ALREADY_REPORTED);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@PostMapping("/cambiarStatusCandidatosBusqueda")
	public ResponseEntity<?> cambiarStatusCandidatosBusqueda(@Valid @RequestBody HistoriaStatusCandidato historiaStatusCandidato, HttpServletRequest request)
			throws NotFoundException {
		LOG_info(request, "PETICION: /cambiarStatusCandidatosBusqueda");

		busquedaService.newHistoriaCandidato(historiaStatusCandidato, getHunterNombreCompleto(request), request);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/desactivarCandBusqueda")
	public ResponseEntity<?> desactivarCandBusqueda(@RequestBody DesactivarCandidatoRequest desactivarCandidatoRequest, HttpServletRequest request) {
		LOG_info(
				request,
				"PETICION: /desactivarCandBusqueda/" + desactivarCandidatoRequest.getBusquedaId() + "/" + desactivarCandidatoRequest.getHunterId() + "/"
						+ desactivarCandidatoRequest.getCandidatoId());

		busquedaService.desactivarCandidatoDeBusqueda(desactivarCandidatoRequest, request);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/cerrarBusqueda")
	public ResponseEntity<?> cerrarBusqueda(@Valid @RequestBody Busqueda busqueda, HttpServletRequest request) throws NotFoundException {
		LOG_info(request, "PETICION: /cerrarBusqueda");

		busquedaService.cerrarBusqueda(busqueda, getHunterNombreCompleto(request));
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	@PostMapping("/asignarArchivoBusqueda")
	public ResponseEntity<?> asignarArchivoBusqueda(@Valid @RequestBody AsignarArchivoBusquedaRequest archivoBusqueda, HttpServletRequest request)
			throws NotFoundException {
		LOG_info(request, "PETICION: /asignarArchivoBusqueda");

		busquedaService.asignarBusqueda(archivoBusqueda);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/deshabilitarHunterBusqueda")
	public ResponseEntity<?> deshabilitarHunterBusqueda(@RequestBody RemplazoHunterResquest reasignarInhabilitar, HttpServletRequest request) {
		LOG_info(request, "PETICION: /deshabilitarHunterBusqueda");

		busquedaService.candidatosReasignar(reasignarInhabilitar);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/busquedaNombreOrApellido")
	public ResponseEntity<?> busquedaNombreOrApellido(@RequestParam String param, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /busquedaNombreOrApellido");

		List<Integer> idBusqueda = busquedaService.findIdBusqueda(param);

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(idBusqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getBusquedaById/{idBusqueda}")
	public ResponseEntity<?> getBusquedaById(@PathVariable int idBusqueda, HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getBusquedaById/" + idBusqueda);

		Busqueda busqueda = busquedaService.findById(idBusqueda);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(busqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getArchivosBusquedaById/{idBusqueda}")
	public ResponseEntity<?> getArchivosBusquedaById(@PathVariable int idBusqueda, HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getArchivosBusquedaById/" + idBusqueda);

		List<Archivo> archivoBusqueda = busquedaService.findArchivosById(idBusqueda);

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(archivoBusqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/eliminarBusqueda")
	public ResponseEntity<?> eliminarBusqueda(@Valid @RequestParam int busquedaId, HttpServletRequest request) {
		LOG_info(request, "PETICION: /eliminarBusqueda id: " + busquedaId);

		busquedaService.eliminarBusqueda(busquedaId, request);
		return ResponseEntity.ok(busquedaId);
	}

	private String getHunterNombreCompleto(HttpServletRequest request) throws NotFoundException {
		Usuario usuarioActivo = usuarioRepository.findByNombreUsuario(nombreUsuario).orElseThrow(() -> new NotFoundException(CodigoError.USER_NOT_FOUND));

		return usuarioActivo.getNombre() + " " + usuarioActivo.getApellido();
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/postBusquedasIdByString")
	public ResponseEntity<?> postBusquedasIdByString(@RequestBody BuscarStringRequest buscar, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /postBusquedasIdByString");

		Usuario usuarioActivo = usuarioRepository.findByNombreUsuario(nombreUsuario).orElseThrow(() -> new NotFoundException(CodigoError.USER_NOT_FOUND));

		List<Integer> resultadoBusquedasIds = busquedaService.buscarEntreLasBusquedas(buscar.getTexto());

		if (resultadoBusquedasIds.isEmpty()) {
			resultadoBusquedasIds.add(0);
		}

		TotalDetalleBusqueda totalDetalleBusqueda = busquedaService.getTotalDetalleBusqueda(usuarioActivo, resultadoBusquedasIds);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(totalDetalleBusqueda);
		return ResponseEntity.ok(response);
	}

}
