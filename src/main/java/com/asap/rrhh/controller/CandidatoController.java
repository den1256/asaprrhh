package com.asap.rrhh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.BuscarStringRequest;
import com.asap.rrhh.inout.CandidatoHistoriaRespuesta;
import com.asap.rrhh.inout.TotalCandidatoAsignaciones;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.Candidato;
import com.asap.rrhh.model.HistoriaStatus;
import com.asap.rrhh.model.TipoFuente;
import com.asap.rrhh.model.TipoLocalidad;
import com.asap.rrhh.model.TipoStatus;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.CandidatoBusquedaRepository;
import com.asap.rrhh.repository.CandidatoRepository;
import com.asap.rrhh.repository.HistoriaStatusRepository;
import com.asap.rrhh.repository.HunterRepository;
import com.asap.rrhh.repository.TipoFuenteRepository;
import com.asap.rrhh.repository.TipoLocalidadRepository;
import com.asap.rrhh.repository.TipoStatusRepository;
import com.asap.rrhh.service.CandidatoService;
import com.asap.rrhh.util.LocaleUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class CandidatoController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	CandidatoRepository candidatoRepository;

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	BusquedaRepository busquedaRepository;

	@Autowired
	TipoStatusRepository tipoStatusRepository;

	@Autowired
	TipoLocalidadRepository tipoLocalidadRepository;

	@Autowired
	TipoFuenteRepository tipoFuenteRepository;

	@Autowired
	HistoriaStatusRepository historiaStatusRepository;

	@Autowired
	CandidatoBusquedaRepository candidatoBusquedaRepository;

	@Autowired
	CandidatoService candidatoService;

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/getCandidatos")
	public ResponseEntity<?> getCandidatos(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getCandidatos");

		List<Candidato> candidatos = candidatoRepository.findAll();
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(candidatos);

		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@PostMapping("/postCandidato")
	public ResponseEntity<?> postCandidato(@Valid @RequestBody Candidato candidato, HttpServletRequest request) {
		LOG_info(request, "PETICION: /postCandidato");
		LOG_info(request, "candidato : " + gson.toJson(candidato));
		LOG_info(request, "linkedin : " + candidato.getCandidatoLinkedin());

		candidato.setCandidatoNombre(candidato.getCandidatoNombre().trim());
		candidato.setCandidatoApellido(candidato.getCandidatoApellido().trim());

		candidato.setCandidatoCelular(new LocaleUtils().getNullIfEmpty(candidato.getCandidatoCelular()));
		candidato.setCandidatoEmail(new LocaleUtils().getNullIfEmpty(candidato.getCandidatoEmail()));
		candidato.setCandidatoLinkedin(new LocaleUtils().getNullIfEmpty(candidato.getCandidatoLinkedin()));

		candidatoRepository.save(candidato);
		int candidatoId = candidato.getCandidatoId();
		return ResponseEntity.ok(candidatoId);
	}

	@PreAuthorize("hasRole('USER')")
	@PutMapping("/putCandidato")
	public ResponseEntity<?> putCandidato(@Valid @RequestBody Candidato candidato, HttpServletRequest request) {
		LOG_info(request, "PETICION: /putCandidato");
		LOG_info(request, "candidato : " + gson.toJson(candidato));
		LOG_info(request, "linkedin : " + candidato.getCandidatoLinkedin());

		candidato.setCandidatoCelular(new LocaleUtils().getNullIfEmpty(candidato.getCandidatoCelular()));
		candidato.setCandidatoEmail(new LocaleUtils().getNullIfEmpty(candidato.getCandidatoEmail()));
		candidato.setCandidatoLinkedin(new LocaleUtils().getNullIfEmpty(candidato.getCandidatoLinkedin()));

		candidatoRepository.save(candidato);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/getCandidato/{candidatoId}")
	public ResponseEntity<?> getCandidatoById(@PathVariable(value = "candidatoId") int candidatoId, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getCandidato por ID/" + candidatoId);

		Candidato candidato = candidatoRepository.getOne(candidatoId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(candidato);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getLocalidades")
	public ResponseEntity<?> getLocalidad(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getLocalidades");

		List<TipoLocalidad> localidades = tipoLocalidadRepository.getLocalidades();
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(localidades);

		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getTipoStatusCandidato")
	public ResponseEntity<?> getTipoStatusCandidato(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getTipoStatusCandidato");

		List<TipoStatus> tipoStatus = tipoStatusRepository.getTipoStatus();

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(tipoStatus);

		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getFuenteCandidato")
	public ResponseEntity<?> getFuenteCandidato(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getFuenteCandidato");

		List<TipoFuente> tipoFuente = tipoFuenteRepository.findAll();
		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(tipoFuente);

		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getCandidatoByMail/{email}")
	public ResponseEntity<?> getCandidatoByMail(@PathVariable(value = "email") String email, HttpServletRequest request) throws JsonProcessingException {

		Candidato candidato = candidatoService.findByEmail(email.toLowerCase().trim());
		String candId = "null";
		if (candidato == null) {
			candidato = new Candidato();
		} else {
			candId = String.valueOf(candidato.getCandidatoId());
		}
		LOG_info(request, "PETICION: /getCandidatoByMail/" + email.toLowerCase().trim() + " : " + candId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(candidato);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getCandidatoByCelular/{celular}")
	public ResponseEntity<?> getCandidatoByCelular(@PathVariable(value = "celular") String celular, HttpServletRequest request) throws JsonProcessingException {

		Candidato candidato = candidatoService.findByCelular(celular.trim());
		String candId = "null";
		if (candidato == null) {
			candidato = new Candidato();
		} else {
			candId = String.valueOf(candidato.getCandidatoId());
		}

		LOG_info(request, "PETICION: /getCandidatoByCelular/" + celular.trim() + " : " + candId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(candidato);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@PostMapping("/getCandidatoByLinkedin")
	public ResponseEntity<?> getCandidatoByLinkedin(@Valid @RequestBody Candidato candidato, HttpServletRequest request) throws JsonProcessingException {

		Optional<List<Candidato>> candidatos = candidatoRepository.findByLinkedIn(candidato.getCandidatoLinkedin().trim());

		Candidato candidatoEncontrado = new Candidato();
		String candId = "null";
		if (candidatos.isPresent()) {
			if (candidatos.get().size() == 1) {
				candidatoEncontrado = candidatos.get().get(0);
				candId = String.valueOf(candidatoEncontrado.getCandidatoId());
			}
		}

		LOG_info(request, "PETICION: /getCandidatoByLinkedin/" + candidato.getCandidatoLinkedin().trim() + " : " + candId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(candidatoEncontrado);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getCandidatoHistoria/{busquedaId}/{hunterId}/{candidatoId}")
	public ResponseEntity<?> getCandidatoHistoriaBusqueda(
			@PathVariable int busquedaId,
			@PathVariable int hunterId,
			@PathVariable int candidatoId,
			HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getCandidatoHistoria/" + busquedaId + " / " + hunterId + " / " + candidatoId);

		List<HistoriaStatus> historiasOrdenadas = candidatoService.getHistoriaStatuses(busquedaId, hunterId, candidatoId);
		String fechaIngreso = candidatoService.getFechaIngreso(busquedaId, hunterId, candidatoId);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(new CandidatoHistoriaRespuesta(fechaIngreso, historiasOrdenadas));
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getCandidatoHistAsignaciones/{candidatoId}")
	public ResponseEntity<?> getCanditatoHistAsignaciones(@PathVariable int candidatoId, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getCanditatoHistAsignaciones por Id de Candidato/" + candidatoId);

		TotalCandidatoAsignaciones totalAsignaciones = candidatoService.getTotalCandidatoAsignaciones(candidatoId);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(totalAsignaciones);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@PostMapping("/candidatosByBusqueda")
	public ResponseEntity<?> candidatosByBusqueda(@RequestBody BuscarStringRequest buscarCandidato, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /candidatosByBusqueda/" + buscarCandidato);

		List<Candidato> candidatosEncontrado = new ArrayList<>();

		candidatosEncontrado = candidatoService.buscarCandidatos((String) buscarCandidato.getTexto());

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(candidatosEncontrado);
		return ResponseEntity.ok(response);
	}

}