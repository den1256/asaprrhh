package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.PerfilClasificacion;
import com.asap.rrhh.model.TipoPerfil;
import com.asap.rrhh.repository.PerfilClasificacionRepository;
import com.asap.rrhh.repository.PerfilRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class PerfilController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	PerfilRepository perfilRepository;

	@Autowired
	PerfilClasificacionRepository perfilClasificacionRepository;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getPerfil")
	public ResponseEntity<?> getPerfil(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getPerfil");

		List<TipoPerfil> tipoPerfil = perfilRepository.getTipoPerfil();

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(tipoPerfil);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getClasificacion")
	public ResponseEntity<?> getClasificacion(@RequestParam("perfilId") int perfilId, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getClasificacion");

		List<PerfilClasificacion> clasificacion = perfilClasificacionRepository.getPerfilesPorClasificacion(perfilId);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(clasificacion);
		return ResponseEntity.ok(response);
	}
}
