package com.asap.rrhh.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.service.NotificacionesService;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class NotificacionesController{

	@Autowired
	NotificacionesService notificacionesService;
	
	Logger logger = LogManager.getLogger(getClass());
	
	@GetMapping("/enviarNotificaciones")
	public ResponseEntity<?> enviarNotificaciones() throws Exception {
		logger.info("EJECUCION: --->> Proceso de Envio de Correos de Notificaciones <<---");

		notificacionesService.enviarNotificacionesDiarias();
		
		return ResponseEntity.ok(true);
	}

}
