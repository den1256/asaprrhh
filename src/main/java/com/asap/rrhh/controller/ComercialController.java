package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.dto.Mensaje;
import com.asap.rrhh.inout.AsignarComercialRequest;
import com.asap.rrhh.inout.CambioEstadoUsuarioRequest;
import com.asap.rrhh.inout.ComercialBusquedaRequest;
import com.asap.rrhh.inout.ComercialResponse;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.Comercial;
import com.asap.rrhh.model.ComercialBusqueda;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.ComercialBusquedaRepository;
import com.asap.rrhh.repository.ComercialRepository;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.service.UsuarioService;
import com.asap.rrhh.service.ComercialService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class ComercialController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	ComercialService comercialService;

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	ComercialRepository comercialRepository;

	@Autowired
	ComercialBusquedaRepository comercialBusquedaRepository;

	@Autowired
	BusquedaRepository busquedaRepository;

	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	@GetMapping("/getComercial/{idComercial}")
	public ResponseEntity<?> getComercial(@PathVariable int idComercial, HttpServletRequest request) throws NotFoundException, JsonProcessingException {
		LOG_info(request, "PETICION: /getComercial/" + idComercial);

		ComercialResponse comercial = comercialService.findByid(idComercial);

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(comercial);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	@GetMapping("/getComerciales")
	public ResponseEntity<?> getComerciales(HttpServletRequest request) throws NotFoundException, JsonProcessingException {
		LOG_info(request, "PETICION: /getComerciales");

		List<Object[]> comerciales = comercialRepository.getComerciales();
		List<ComercialResponse> comercialResponses = comercialService.convertirListaComercial(comerciales);

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(comercialResponses);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	@GetMapping("/getComercialesActivos")
	public ResponseEntity<?> getComercialesActivos(HttpServletRequest request) throws NotFoundException, JsonProcessingException {
		LOG_info(request, "PETICION: /getComercialesActivos");

		List<Object[]> comerciales = comercialRepository.getComercialesActivos();
		List<ComercialResponse> comercialResponses = comercialService.convertirListaComercial(comerciales);

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(comercialResponses);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/postComercial")
	public ResponseEntity<?> postComercial(@RequestParam String nombreUsuario, HttpServletRequest request) {
		LOG_info(request, "PETICION: /postComercial");

		Usuario usuario = usuarioService.getUserData(nombreUsuario);

		comercialService.nuevoComercial(usuario);

		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/putComercial")
	public ResponseEntity<?> putComercial(@RequestBody CambioEstadoUsuarioRequest comercialRequest, HttpServletRequest request) {
		LOG_info(request, "PETICION: /putComercial");

		Comercial comercial = comercialRepository.getOne(comercialRequest.getEntityId());

		comercial.setComercialActivo(comercialRequest.getEntityActivo());

		comercialRepository.save(comercial);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	@PostMapping("/asignarComercialBusqueda")
	public ResponseEntity<?> asignarComercialBusqueda(@RequestBody AsignarComercialRequest asignarComercialBusqueda, HttpServletRequest request) {
		LOG_info(request, "PETICION: /asignarComercialBusqueda");
		
		
		Integer cantidadComercialesNoAsignado = comercialService.asignarComerciales(asignarComercialBusqueda);

		if (cantidadComercialesNoAsignado > 0) {
			return new ResponseEntity<Mensaje>(new Mensaje("Uno o más Comerciales ya existen en la búsqueda"), HttpStatus.OK);
		}

		return new ResponseEntity<Mensaje>(new Mensaje("El Comercial se asignó a la búsqueda"), HttpStatus.OK);
		
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/deshabilitarComercialBusqueda")
	public ResponseEntity<?> deshabilitarComercialBusqueda(@RequestBody ComercialBusquedaRequest deshabilitarComercialBusqueda, HttpServletRequest request) {
		LOG_info(request, "PETICION: /deshabilitarComercialBusqueda");

		ComercialBusqueda deshabilitarComercial = comercialBusquedaRepository
				.getComercialBusquedaByIDs(deshabilitarComercialBusqueda.getBusquedaId(), deshabilitarComercialBusqueda.getComercialId());

		deshabilitarComercial.setComercialBusquedaActivo(0);

		comercialBusquedaRepository.save(deshabilitarComercial);

		return ResponseEntity.ok(true);
	}

}
