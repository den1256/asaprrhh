package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.Cliente;
import com.asap.rrhh.repository.ClienteRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class ClienteController extends BaseController {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	ObjectMapper objectMapper;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getClientes")
	public ResponseEntity<?> getClientes(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getClientes");

		List<Cliente> clientes = clienteRepository.getClientes();

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(clientes);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getClientesActivos")
	public ResponseEntity<?> getClientesActivos(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getClientesActivos");

		List<Cliente> clientes = clienteRepository.getClientesActivos();

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(clientes);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@PostMapping("/postCliente")
	public ResponseEntity<?> postCliente(@Valid @RequestBody Cliente cliente, HttpServletRequest request) {

		cliente.setClienteNombre(cliente.getClienteNombre().trim());
		cliente.setClienteContacto(cliente.getClienteContacto().trim());
		cliente.setClienteDireccion(cliente.getClienteDireccion().trim());

		LOG_info(request, "PETICION: /postCliente");
		clienteRepository.save(cliente);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@PutMapping("/putCliente")
	public ResponseEntity<?> putCliente(@Valid @RequestBody Cliente cliente, HttpServletRequest request) {
		LOG_info(request, "PETICION: /putCliente");
		clienteRepository.save(cliente);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/getCliente/{clienteId}")
	public ResponseEntity<?> getClienteById(@PathVariable(value = "clienteId") int clienteId, HttpServletRequest request) throws JsonProcessingException {

		LOG_info(request, "PETICION: /getCliente por ID/" + clienteId);

		Cliente cliente = clienteRepository.getOne(clienteId);
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(cliente);

		return ResponseEntity.ok(response);
	}

}
