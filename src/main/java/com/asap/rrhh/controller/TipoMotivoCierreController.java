package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.TipoMotivoCierre;
import com.asap.rrhh.repository.TipoMotivoCierreRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class TipoMotivoCierreController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	TipoMotivoCierreRepository tipoMotivoCierreRepository;

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getMotivosCierre")
	public ResponseEntity<?> getMotivosCierre(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getMotivosCierre");

		List<TipoMotivoCierre> motivos = tipoMotivoCierreRepository.findAll();

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(motivos);
		return ResponseEntity.ok(response);
	}

}
