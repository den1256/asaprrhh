package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.Diferencial;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.service.ReporteService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class ReporteController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	ReporteService reporteService;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/diferencialSemanal/{busquedaId}")
	public ResponseEntity<?> diferencialSemanal(@PathVariable int busquedaId, HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /diferencialSemanal por Id: " + busquedaId);

		List<Diferencial> reporteBusqueda = reporteService.getDiferencialSemanal(busquedaId);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(reporteBusqueda);
		return ResponseEntity.ok(response);
	}

}
