package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.TipoTecnologia;
import com.asap.rrhh.repository.TecnologiaRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class TecnologiaController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	TecnologiaRepository tecnologiaRepository;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getTecnologias")
	public ResponseEntity<?> getTecnologias(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getTecnologias");

		List<TipoTecnologia> tipoTecnologia;
		tipoTecnologia = tecnologiaRepository.getTipoTecnologia();

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(tipoTecnologia);
		return ResponseEntity.ok(response);
	}
}
