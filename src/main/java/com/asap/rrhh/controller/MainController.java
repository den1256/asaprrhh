package com.asap.rrhh.controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.inout.Version;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://35.230.70.42", "http://localhost:4200",
		"http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
@RequestMapping("/")
public class MainController extends BaseController {

	// Get All Candidatos
	@GetMapping("/version")
	public Version _getVersion() {
		return getVersion();
	}

	@GetMapping("/loc")
	public String loc() {
		String salida = "";
		Locale[] locales = NumberFormat.getAvailableLocales();
		double myNumber = -1234.56;
		NumberFormat form;
		for (int j = 0; j < 4; ++j) {
			salida = ("FORMAT") + '\n';
			for (int i = 0; i < locales.length; ++i) {
				if (locales[i].getCountry().length() == 0) {
					continue; // Skip language-only locales
				}
				salida += (locales[i].getDisplayName());
				switch (j) {
				case 0:
					form = NumberFormat.getInstance(locales[i]);
					break;
				case 1:
					form = NumberFormat.getIntegerInstance(locales[i]);
					break;
				case 2:
					form = NumberFormat.getCurrencyInstance(locales[i]);
					break;
				default:
					form = NumberFormat.getPercentInstance(locales[i]);
					break;
				}
				if (form instanceof DecimalFormat) {
					salida += (": " + ((DecimalFormat) form).toPattern());
				}
				salida += (" -> " + form.format(myNumber));
				try {
					salida += (" -> " + form.parse(form.format(myNumber))) + '\n';
				} catch (ParseException e) {
				}
			}
		}
		return salida;
	}

}