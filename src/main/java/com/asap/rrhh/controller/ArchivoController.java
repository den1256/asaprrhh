package com.asap.rrhh.controller;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.asap.rrhh.model.Archivo;
import com.asap.rrhh.payload.Response;
import com.asap.rrhh.service.ArchivoService;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class ArchivoController extends BaseController {

	@Autowired
	private ArchivoService archivoService;

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/descargarArchivo/{fileId:.+}")
	public ResponseEntity<Resource> descargarArchivo(@PathVariable int fileId, HttpServletRequest request) throws FileNotFoundException {
		LOG_info(request, "--------****---------PETICION: /descargarArchivo---------****---------");

		Archivo dbFile = archivoService.getFile(fileId);

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(dbFile.getArchivoTipo()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getArchivoNombre() + "\"")
				.body(new ByteArrayResource(dbFile.getArchivoDatos()));
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/guardarArchivo")
	public Response uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		LOG_info(request, "--------****---------PETICION: /guardarArchivo---------****---------");

		Archivo nombreArchivo = archivoService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/opt/asap_cv/files/").path(nombreArchivo.getArchivoNombre())
				.toUriString();

		return new Response(nombreArchivo.getArchivoId(), nombreArchivo.getArchivoNombre(), fileDownloadUri, file.getContentType(), file.getSize());
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/guardarMultiplesArchivos")
	public List<Response> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files, HttpServletRequest request) {
		LOG_info(request, "--------****---------PETICION: /guardarMultiplesArchivos---------****---------");
		return Arrays.asList(files).stream().map(file -> uploadFile(file, request)).collect(Collectors.toList());
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/eliminarArchivoCandidato")
	public ResponseEntity<?> eliminarArchivoCandidato(@RequestParam int archivoId, HttpServletRequest request) {
		LOG_info(request, "PETICION: /eliminarArchivoCandidato");

		archivoService.eliminarArchivoCandidato(archivoId, request);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/eliminarArchivoBusqueda")
	public ResponseEntity<?> eliminarArchivoBusqueda(@RequestParam int archivoId, HttpServletRequest request) {
		LOG_info(request, "PETICION: /eliminarArchivoBusqueda");

		archivoService.eliminarArchivoBusqueda(archivoId, request);
		return ResponseEntity.ok(true);
	}

	@GetMapping("/respaldarArchivosActuales")
	public ResponseEntity<?> respaldarArchivosActuales(HttpServletRequest request) throws FileNotFoundException {
		LOG_info(request, "---****---PETICION: /respaldarArchivosActuales en /assets/archivos ---****---");

		List<String> resultadoRespaldo = archivoService.respaldarArchivos();

		return ResponseEntity.ok(resultadoRespaldo);
	}

	@GetMapping("/limpiarArchivosBaseDatos")
	public ResponseEntity<?> limpiarArchivosBaseDatos(HttpServletRequest request) throws FileNotFoundException {
		LOG_info(request, "---****---PETICION: /limpiarArchivosBaseDatos ---****---");

		archivoService.eliminarArchivosBaseDatos();

		return ResponseEntity.ok(true);
	}

}
