package com.asap.rrhh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.asap.rrhh.RRHHAplication;
import com.asap.rrhh.inout.Version;
import com.asap.rrhh.security.jwt.JwtProvider;
import com.asap.rrhh.security.jwt.JwtTokenFilter;
import com.google.gson.Gson;

public class BaseController {

	@Autowired
	Environment env;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	JwtTokenFilter jwtTokenFilter;

	public static boolean signatureLog = false;
	protected static Logger LOG = LoggerFactory.getLogger(RRHHAplication.class);
	protected Gson gson = new Gson();
	String nombreUsuario;

	@PostConstruct
	public void BaseControllerPost() {
		if (!signatureLog) {
			for (String signature : getApiSignature()) {
				LOG.info(signature);
			}
			signatureLog = true;
		}
	}

	public String getAmbiente() {
		return env.getProperty("ambiente");
	}

	public String getVersionValue() {
		return env.getProperty("version");
	}

	public Version getVersion() {
		return new Version("asaprrhh_backend_final", getVersionValue(), getAmbiente());
	}

	public String[] getApiSignature() {
		List<String> signature = new ArrayList<String>(0);
		signature.add("================================================================================ ");
		signature.add("= ASAP-CV BackEnd - Smart Heads© ----- " + getVersionValue() + " @ " + getAmbiente() + " -- ");
		signature.add("================================================================================ ");
		return signature.toArray(new String[0]);
	}

	protected void LOG_info(HttpServletRequest req, String logval) {
		String token = jwtTokenFilter.getToken(req);
		nombreUsuario = jwtProvider.getNombreUsuarioFromToken(token);
		LOG.info(getAmbiente() + " @" + nombreUsuario + "-" + logval);
	}
}
