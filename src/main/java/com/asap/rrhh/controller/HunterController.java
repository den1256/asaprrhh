package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.CambioEstadoUsuarioRequest;
import com.asap.rrhh.inout.HunterResponse;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.Hunter;
import com.asap.rrhh.repository.HunterRepository;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.jwt.JwtProvider;
import com.asap.rrhh.security.jwt.JwtTokenFilter;
import com.asap.rrhh.security.repository.UsuarioRepository;
import com.asap.rrhh.security.service.RolService;
import com.asap.rrhh.security.service.UsuarioService;
import com.asap.rrhh.service.HunterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
@RequestMapping("/")
public class HunterController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	JwtTokenFilter jwtTokenFilter;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	RolService rolService;

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	HunterService hunterService;

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/getHunters")
	public ResponseEntity<?> getAllHunters(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getHunters");

		List<Object[]> hunters = hunterRepository.getHunters();
		List<HunterResponse> hunterResponses = hunterService.convertirListaHunter(hunters);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(hunterResponses);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getHuntersActivos")
	public ResponseEntity<?> getHuntersActivos(HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getHunters");

		List<Object[]> huntersActivos = hunterRepository.getHuntersActivos();

		List<HunterResponse> hunterResponses = hunterService.convertirListaHunter(huntersActivos);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(hunterResponses);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/postHunter")
	public ResponseEntity<?> postHunter(@RequestParam String nombreUsuario, HttpServletRequest request) {
		LOG_info(request, "PETICION: /postHunter " + nombreUsuario);

		Usuario usuario = usuarioService.getUserData(nombreUsuario);

		hunterService.nuevoHunter(usuario);

		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/putHunter")
	public ResponseEntity<?> putHunter(@RequestBody CambioEstadoUsuarioRequest hunterRequest, HttpServletRequest request) {
		LOG_info(request, "PETICION: /putHunter");

		Hunter hunter = hunterRepository.getOne(hunterRequest.getEntityId());

		hunter.setHunterActivo(hunterRequest.getEntityActivo());

		hunterRepository.save(hunter);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/getHunter/{hunterId}")
	public ResponseEntity<?> getHunterById(@PathVariable(value = "hunterId") int hunterId, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getHunter por ID/" + hunterId);

		Hunter hunter = hunterRepository.getOne(hunterId);
		HunterResponse hunterResponse = hunterService.convertirHunter(hunter);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(hunterResponse);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/getHunterIdLogueado")
	public ResponseEntity<?> getHunterIdLogueado(HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getHunterIdLogueado");

		Hunter hunter = hunterService.getHunter(request);
		HunterResponse hunterResponse = hunterService.convertirHunter(hunter);

		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(hunterResponse);
		return ResponseEntity.ok(response);
	}

}
