package com.asap.rrhh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.AsignarCvRequest;
import com.asap.rrhh.inout.CandidatoDetalleResponse;
import com.asap.rrhh.inout.Views;
import com.asap.rrhh.model.Archivo;
import com.asap.rrhh.service.CandidatoBusquedaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin({ "http://192.168.100.71:90", "http://192.168.100.71:92", "http://localhost", "http://juego.asap-consulting.com.ar", "http://35.230.70.42",
		"http://localhost:4200", "http://apprrhh.wizard.com.ar", "http://apprrhh.asap-consulting.com.ar" })
public class CandidatoBusquedaController extends BaseController {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	CandidatoBusquedaService candidatoBusquedaService;

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getCandidatoEnBusquedas")
	public ResponseEntity<?> getCandidatoEnBusquedas(@RequestParam int candidatoId, HttpServletRequest request) throws JsonProcessingException {
		LOG_info(request, "PETICION: /getCandidatoEnBusquedas");

		if (!candidatoBusquedaService.getCandidatoEnBusquedas(candidatoId)) {
			return ResponseEntity.ok(false);
		}

		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getDetallesCandidatoBusqueda")
	public ResponseEntity<?> getDetallesCandidatoBusqueda(
			@RequestParam int busquedaId,
			@RequestParam int candidatoId,
			@RequestParam int hunterId,
			HttpServletRequest request) throws JsonProcessingException {

		CandidatoDetalleResponse detallesCandidatoBusqueda = candidatoBusquedaService.getDetallesCandidatoBusqueda(busquedaId, candidatoId, hunterId);

		LOG_info(
				request,
				"PETICION: /getDetallesCandidatoBusqueda /busq: " + busquedaId + " /cand: " + candidatoId + " /hunt: " + hunterId + " /resp: "
						+ gson.toJson(detallesCandidatoBusqueda));
		String response = objectMapper.writerWithView(Views.Detail.class).writeValueAsString(detallesCandidatoBusqueda);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@PostMapping("/asignarCvBusqueda")
	public ResponseEntity<?> asignarCvBusqueda(@RequestBody AsignarCvRequest asignarCv, HttpServletRequest request) throws NotFoundException {
		LOG_info(request, "PETICION: /asignarCvBusqueda");

		candidatoBusquedaService.asignarCV(asignarCv);
		return ResponseEntity.ok(true);
	}

	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@GetMapping("/getArchivosCvCandidato")
	public ResponseEntity<?> getArchivosCvCandidato(
			@RequestParam int busquedaId,
			@RequestParam int hunterId,
			@RequestParam int candidatoId,
			HttpServletRequest request) throws JsonProcessingException, NotFoundException {
		LOG_info(request, "PETICION: /getArchivosCvCandidato/ busq: " + busquedaId + "/ cand: " + hunterId + "/hunt: " + candidatoId);

		List<Archivo> archivoCandidato = candidatoBusquedaService.findArchivosCvCandidatoById(busquedaId, hunterId, candidatoId);

		String response = objectMapper.writerWithView(Views.Simple.class).writeValueAsString(archivoCandidato);
		return ResponseEntity.ok(response);
	}
}
