package com.asap.rrhh.inout;

import java.util.List;

import com.asap.rrhh.model.Actividad;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
public class ActividadResponse {
	
	protected String nombreBusqueda;
	protected String nombreCliente;
	protected List<Actividad> actividadesBusqueda;
	
	@JsonView(Views.Simple.class)
	public String getNombreBusqueda() {
		return nombreBusqueda;
	}
	@JsonView(Views.Simple.class)
	public String getNombreCliente() {
		return nombreCliente;
	}
	@JsonView(Views.Simple.class)
	public List<Actividad> getActividadesBusqueda() {
		return actividadesBusqueda;
	}
	
	

}
