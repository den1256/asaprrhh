package com.asap.rrhh.inout;

import java.io.Serializable;

public class Version implements Serializable {

	private static final long serialVersionUID = 58545206L;

	private String product;
	private String ver;
	private String environment;

	public Version() {
	}

	public Version(String product, String ver, String environment) {
		this.ver = ver;
		this.product = product;
		this.environment = environment;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

}
