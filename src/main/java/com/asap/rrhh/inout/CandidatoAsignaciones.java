package com.asap.rrhh.inout;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

public class CandidatoAsignaciones {

	private Integer busquedaId;
	private String busquedaNombre;
	private String busquedaStatus;
	private String clienteNombre;

	private List<HistoriaCandidatoStatusResponse> statusPorHunter;

	public CandidatoAsignaciones() {
	}

	public CandidatoAsignaciones(Integer busquedaId, String busquedaNombre, String busquedaStatus, String clienteNombre,
			List<HistoriaCandidatoStatusResponse> statusPorHunter) {
		this.busquedaId = busquedaId;
		this.busquedaNombre = busquedaNombre;
		this.busquedaStatus = busquedaStatus;
		this.clienteNombre = clienteNombre;
		this.statusPorHunter = statusPorHunter;
	}

	@JsonView(Views.Simple.class)
	public Integer getBusquedaId() {
		return busquedaId;
	}

	public void setBusquedaId(Integer busquedaId) {
		this.busquedaId = busquedaId;
	}

	@JsonView(Views.Simple.class)
	public String getBusquedaNombre() {
		return busquedaNombre;
	}

	public void setBusquedaNombre(String busquedaNombre) {
		this.busquedaNombre = busquedaNombre;
	}

	@JsonView(Views.Simple.class)
	public List<HistoriaCandidatoStatusResponse> getStatusPorHunter() {
		return statusPorHunter;
	}

	public void setStatusPorHunter(List<HistoriaCandidatoStatusResponse> statusPorHunter) {
		this.statusPorHunter = statusPorHunter;
	}

	@JsonView(Views.Simple.class)
	public String getBusquedaStatus() {
		return busquedaStatus;
	}

	public void setBusquedaStatus(String busquedaStatus) {
		this.busquedaStatus = busquedaStatus;
	}

	@JsonView(Views.Simple.class)
	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

}
