package com.asap.rrhh.inout;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ComercialResponse {

	protected int comercialId;
	protected String comercialNombre;
	protected String comercialApellido;
	protected String comercialEmail;
	protected int comercialActivo;

	@JsonView(Views.Simple.class)
	public int getComercialId() {
		return comercialId;
	}

	@JsonView(Views.Simple.class)
	public String getComercialNombre() {
		return comercialNombre;
	}

	@JsonView(Views.Simple.class)
	public String getComercialApellido() {
		return comercialApellido;
	}

	@JsonView(Views.Simple.class)
	public String getComercialEmail() {
		return comercialEmail;
	}

	@JsonView(Views.Simple.class)
	public int getComercialActivo() {
		return comercialActivo;
	}

}
