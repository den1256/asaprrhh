package com.asap.rrhh.inout;

import com.fasterxml.jackson.annotation.JsonView;

public class HistoriaCandidatoStatusResponse {
	protected int hunterId;
	protected String hunterNombre;
	protected String candidatoStatus;

	public HistoriaCandidatoStatusResponse() {

	}

	@JsonView(Views.Simple.class)
	public int getHunterId() {
		return hunterId;
	}

	public void setHunterId(int hunterId) {
		this.hunterId = hunterId;
	}

	@JsonView(Views.Simple.class)
	public String getHunterNombre() {
		return hunterNombre;
	}

	public void setHunterNombre(String hunterNombre) {
		this.hunterNombre = hunterNombre;
	}

	@JsonView(Views.Simple.class)
	public String getCandidatoStatus() {
		return candidatoStatus;
	}

	public void setCandidatoStatus(String candidatoStatus) {
		this.candidatoStatus = candidatoStatus;
	}

}
