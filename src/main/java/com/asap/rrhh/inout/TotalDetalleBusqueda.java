package com.asap.rrhh.inout;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

public class TotalDetalleBusqueda {

	private List<DetalleBusqueda> busquedasHunterActivas;
	private List<DetalleBusqueda> busquedasHunterCerradas;
	private List<DetalleBusqueda> busquedasActivas;
	private List<DetalleBusqueda> busquedasCerradas;

	public TotalDetalleBusqueda() {
	}

	public TotalDetalleBusqueda(List<DetalleBusqueda> busquedasHunterActivas,
			List<DetalleBusqueda> busquedasHunterCerradas, List<DetalleBusqueda> busquedasActivas,
			List<DetalleBusqueda> busquedasCerradas) {
		super();
		this.busquedasHunterActivas = busquedasHunterActivas;
		this.busquedasHunterCerradas = busquedasHunterCerradas;
		this.busquedasActivas = busquedasActivas;
		this.busquedasCerradas = busquedasCerradas;
	}

	@JsonView(Views.Detail.class)
	public List<DetalleBusqueda> getBusquedasHunterActivas() {
		return busquedasHunterActivas;
	}

	public void setBusquedasHunterActivas(List<DetalleBusqueda> busquedasHunterActivas) {
		this.busquedasHunterActivas = busquedasHunterActivas;
	}

	@JsonView(Views.Detail.class)
	public List<DetalleBusqueda> getBusquedasHunterCerradas() {
		return busquedasHunterCerradas;
	}

	public void setBusquedasHunterCerradas(List<DetalleBusqueda> busquedasHunterCerradas) {
		this.busquedasHunterCerradas = busquedasHunterCerradas;
	}

	@JsonView(Views.Detail.class)
	public List<DetalleBusqueda> getBusquedasActivas() {
		return busquedasActivas;
	}

	public void setBusquedasActivas(List<DetalleBusqueda> busquedasActivas) {
		this.busquedasActivas = busquedasActivas;
	}

	@JsonView(Views.Detail.class)
	public List<DetalleBusqueda> getBusquedasCerradas() {
		return busquedasCerradas;
	}

	public void setBusquedasCerradas(List<DetalleBusqueda> busquedasCerradas) {
		this.busquedasCerradas = busquedasCerradas;
	}

}
