package com.asap.rrhh.inout;

import lombok.Data;

@Data
public class ComercialBusquedaRequest {

	protected Integer comercialId;
	protected Integer busquedaId;
	
}
