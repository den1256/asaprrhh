package com.asap.rrhh.inout;

import java.util.List;

import lombok.Data;

@Data
public class AsignarArchivoBusquedaRequest {

	protected int busquedaId;
	protected List<ArchivoRequest> archivosId;

}
