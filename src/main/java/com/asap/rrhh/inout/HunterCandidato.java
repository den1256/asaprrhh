package com.asap.rrhh.inout;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
public class HunterCandidato {

	private HunterResponse hunterResponseer;
	private List<CandidatoAmpliado> candidatos;

	public HunterCandidato(HunterResponse hunterResponseer, List<CandidatoAmpliado> candidatos) {
		super();
		this.hunterResponseer = hunterResponseer;
		this.candidatos = candidatos;
	}

	@JsonView(Views.Simple.class)
	public HunterResponse getHunter() {
		return hunterResponseer;
	}

	@JsonView(Views.Simple.class)
	public List<CandidatoAmpliado> getCandidatos() {
		return candidatos;
	}

}
