package com.asap.rrhh.inout;

import com.asap.rrhh.model.CandidatoBusqueda;

public class AsignarCandidatoBusqueda {

	private CandidatoBusqueda candidatoBusqueda;
	private String observacion;

	public AsignarCandidatoBusqueda() {
	}

	public AsignarCandidatoBusqueda(CandidatoBusqueda candidatoBusqueda, String observacion) {
		super();
		this.candidatoBusqueda = candidatoBusqueda;
		this.observacion = observacion;
	}

	public CandidatoBusqueda getCandidatoBusqueda() {
		return candidatoBusqueda;
	}

	public void setCandidatoBusqueda(CandidatoBusqueda candidatoBusqueda) {
		this.candidatoBusqueda = candidatoBusqueda;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

}
