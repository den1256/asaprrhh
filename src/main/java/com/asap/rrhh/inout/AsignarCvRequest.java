package com.asap.rrhh.inout;

import java.util.List;

import lombok.Data;

@Data
public class AsignarCvRequest {

	protected Integer busquedaId;
	protected Integer hunterId;
	protected Integer  candidatoId;
	protected List<ArchivoRequest> archivos;
	
}
