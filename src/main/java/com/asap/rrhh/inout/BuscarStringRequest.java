package com.asap.rrhh.inout;

public class BuscarStringRequest {
	
	protected String texto;

	public BuscarStringRequest() {
		
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	

}
