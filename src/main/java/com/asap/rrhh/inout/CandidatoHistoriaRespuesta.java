package com.asap.rrhh.inout;

import java.util.List;

import com.asap.rrhh.model.HistoriaStatus;
import com.fasterxml.jackson.annotation.JsonView;

public class CandidatoHistoriaRespuesta {

	private String fechaIngreso;
	private List<HistoriaStatus> historias;

	public CandidatoHistoriaRespuesta() {

	}

	public CandidatoHistoriaRespuesta(String fechaIngreso, List<HistoriaStatus> historias) {
		super();
		this.fechaIngreso = fechaIngreso;
		this.historias = historias;
	}

	@JsonView(Views.Simple.class)
	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	@JsonView(Views.Simple.class)
	public List<HistoriaStatus> getHistorias() {
		return historias;
	}

	public void setHistorias(List<HistoriaStatus> historias) {
		this.historias = historias;
	}

}
