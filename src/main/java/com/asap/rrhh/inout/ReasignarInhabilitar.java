package com.asap.rrhh.inout;

import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.Hunter;

public class ReasignarInhabilitar {

	private Busqueda busquedaReasignar;
	private Hunter hunterInhabilitar;
	private Hunter hunterRemplazo;

	public ReasignarInhabilitar() {

	}

	public ReasignarInhabilitar(Busqueda busquedaReasignar, Hunter hunterInhabilitar, Hunter hunterRemplazo) {
		super();
		this.busquedaReasignar = busquedaReasignar;
		this.hunterInhabilitar = hunterInhabilitar;
		this.hunterRemplazo = hunterRemplazo;
	}

	public Busqueda getBusquedaReasignar() {
		return busquedaReasignar;
	}

	public void setBusquedaReasignar(Busqueda busquedaReasignar) {
		this.busquedaReasignar = busquedaReasignar;
	}

	public Hunter getHunterInhabilitar() {
		return hunterInhabilitar;
	}

	public void setHunterInhabilitar(Hunter hunterInhabilitar) {
		this.hunterInhabilitar = hunterInhabilitar;
	}

	public Hunter getHunterRemplazo() {
		return hunterRemplazo;
	}

	public void setHunterRemplazo(Hunter hunterRemplazo) {
		this.hunterRemplazo = hunterRemplazo;
	}

}
