package com.asap.rrhh.inout;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

public class TotalCandidatoAsignaciones {

	private Integer cantidadBusquedas;
	private List<CandidatoAsignaciones> asignaciones;

	public TotalCandidatoAsignaciones() {
	}

	public TotalCandidatoAsignaciones(Integer cantidadBusquedas, List<CandidatoAsignaciones> asignaciones) {
		super();
		this.cantidadBusquedas = cantidadBusquedas;
		this.asignaciones = asignaciones;
	}

	@JsonView(Views.Simple.class)
	public Integer getCantidadBusquedas() {
		return cantidadBusquedas;
	}

	public void setCantidadBusquedas(Integer cantidadBusquedas) {
		this.cantidadBusquedas = cantidadBusquedas;
	}

	@JsonView(Views.Simple.class)
	public List<CandidatoAsignaciones> getAsignaciones() {
		return asignaciones;
	}

	public void setAsignaciones(List<CandidatoAsignaciones> asignaciones) {
		this.asignaciones = asignaciones;
	}

}
