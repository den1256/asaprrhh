package com.asap.rrhh.inout;

import java.util.List;

import lombok.Data;

@Data
public class AsignarComercialRequest {

	protected List<Integer> comercialId;
	protected Integer busquedaId;
	
}
