package com.asap.rrhh.inout;

import java.sql.Timestamp;

import com.asap.rrhh.model.Candidato;
import com.asap.rrhh.model.TipoStatus;
import com.fasterxml.jackson.annotation.JsonView;

public class CandidatoDetalleResponse {

	private Candidato candidato;
	private TipoStatus candidatoBusquedaStatus;
	private double candidatoBusquedaRBP;
	private Integer candidatoBusquedaActivo;
	private Timestamp fechaIngreso;

	public CandidatoDetalleResponse() {

	}

	public CandidatoDetalleResponse(Candidato candidato, TipoStatus candidatoBusquedaStatus,
			double candidatoBusquedaRBP, Integer candidatoBusquedaActivo, Timestamp fechaIngreso) {
		super();
		this.candidato = candidato;
		this.candidatoBusquedaStatus = candidatoBusquedaStatus;
		this.candidatoBusquedaRBP = candidatoBusquedaRBP;
		this.candidatoBusquedaActivo = candidatoBusquedaActivo;
		this.fechaIngreso = fechaIngreso;
	}

	@JsonView(Views.Simple.class)
	public Candidato getCandidato() {
		return candidato;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}

	@JsonView(Views.Simple.class)
	public TipoStatus getCandidatoBusquedaStatus() {
		return candidatoBusquedaStatus;
	}

	public void setCandidatoBusquedaStatus(TipoStatus candidatoBusquedaStatus) {
		this.candidatoBusquedaStatus = candidatoBusquedaStatus;
	}

	@JsonView(Views.Simple.class)
	public double getCandidatoBusquedaRBP() {
		return candidatoBusquedaRBP;
	}

	public void setCandidatoBusquedaRBP(double candidatoBusquedaRBP) {
		this.candidatoBusquedaRBP = candidatoBusquedaRBP;
	}

	@JsonView(Views.Simple.class)
	public Integer getCandidatoBusquedaActivo() {
		return candidatoBusquedaActivo;
	}

	public void setCandidatoBusquedaActivo(Integer candidatoBusquedaActivo) {
		this.candidatoBusquedaActivo = candidatoBusquedaActivo;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Timestamp fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

}
