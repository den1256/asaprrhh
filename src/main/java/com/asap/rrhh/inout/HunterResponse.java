package com.asap.rrhh.inout;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HunterResponse {
    protected int hunterId;
    protected String hunterNombre;
    protected String hunterApellido;
    protected String hunterEmail;
    protected int hunterActivo;

    @JsonView(Views.Simple.class)
    public int getHunterId() {
        return hunterId;
    }

    @JsonView(Views.Simple.class)
    public String getHunterNombre() {
        return hunterNombre;
    }

    @JsonView(Views.Simple.class)
    public String getHunterApellido() {
        return hunterApellido;
    }

    @JsonView(Views.Simple.class)
    public String getHunterEmail() {
        return hunterEmail;
    }

    @JsonView(Views.Simple.class)
    public int getHunterActivo() {
        return hunterActivo;
    }
}
