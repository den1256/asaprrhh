package com.asap.rrhh.inout;

public class MessageResponse {

	public static final String STATUS_OK = "OK";
	public static final String STATUS_ERROR = "ERROR";

	private String message;
	private String status;

	public MessageResponse(String message) {
		super();
		this.message = message;
		this.status = STATUS_OK;
	}

	public MessageResponse(String message, String status) {
		super();
		this.message = message;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public String getStatus() {
		return status;
	}
}
