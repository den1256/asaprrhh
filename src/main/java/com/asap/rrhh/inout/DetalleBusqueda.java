package com.asap.rrhh.inout;

import java.util.List;

import com.asap.rrhh.model.Busqueda;
import com.fasterxml.jackson.annotation.JsonView;

public class DetalleBusqueda {

	private int cantCandidatos;

	private int cantCanditatosEnviadosCliente;

	private int cantCanditatosEntrevistados;

	private int idArchivo; // TODO: Reemplazr por array

	private List<HunterResponse> hunters;

	private List<ComercialResponse> comerciales;

	private Busqueda busqueda;

	public DetalleBusqueda() {
		super();
	}

	public DetalleBusqueda(int cantCandidatos, int cantCanditatosEnviadosCliente, int cantCanditatosEntrevistados,
			int idArchivo, List<HunterResponse> hunters, List<ComercialResponse> comerciales, Busqueda busqueda) {
		super();
		this.cantCandidatos = cantCandidatos;
		this.cantCanditatosEnviadosCliente = cantCanditatosEnviadosCliente;
		this.cantCanditatosEntrevistados = cantCanditatosEntrevistados;
		this.idArchivo = idArchivo;
		this.hunters = hunters;
		this.comerciales = comerciales;
		this.busqueda = busqueda;
	}

	public DetalleBusqueda(int cantCandidatos, int cantCanditatosEnviadosCliente, int cantCanditatosEntrevistados,
			int idArchivo, List<HunterResponse> hunters, Busqueda busqueda) {
		super();
		this.cantCandidatos = cantCandidatos;
		this.cantCanditatosEnviadosCliente = cantCanditatosEnviadosCliente;
		this.cantCanditatosEntrevistados = cantCanditatosEntrevistados;
		this.idArchivo = idArchivo;
		this.hunters = hunters;
		this.busqueda = busqueda;
	}

	public DetalleBusqueda(int cantCandidatos, int idArchivo, List<HunterResponse> hunters, Busqueda busqueda) {
		super();
		this.cantCandidatos = cantCandidatos;
		this.idArchivo = idArchivo;
		this.hunters = hunters;
		this.busqueda = busqueda;
	}

	public DetalleBusqueda(int cantCandidatos, List<HunterResponse> hunters, Busqueda busqueda) {
		this.cantCandidatos = cantCandidatos;
		this.hunters = hunters;
		this.busqueda = busqueda;
	}

	@JsonView(Views.Detail.class)
	public int getCantCandidatos() {
		return cantCandidatos;
	}

	public void setCantCandidatos(int cantCandidatos) {
		this.cantCandidatos = cantCandidatos;
	}

	@JsonView(Views.Detail.class)
	public int getCantCanditatosEnviadosCliente() {
		return cantCanditatosEnviadosCliente;
	}

	public void setCantCanditatosEnviadosCliente(int cantCanditatosEnviadosCliente) {
		this.cantCanditatosEnviadosCliente = cantCanditatosEnviadosCliente;
	}

	@JsonView(Views.Detail.class)
	public int getCantCanditatosEntrevistados() {
		return cantCanditatosEntrevistados;
	}

	public void setCantCanditatosEntrevistados(int cantCanditatosEntrevistados) {
		this.cantCanditatosEntrevistados = cantCanditatosEntrevistados;
	}

	@JsonView(Views.Detail.class)
	public int getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(int idArchivo) {
		this.idArchivo = idArchivo;
	}

	@JsonView(Views.Detail.class)
	public List<HunterResponse> getHunters() {
		return hunters;
	}

	public void setHunters(List<HunterResponse> hunters) {
		this.hunters = hunters;
	}

	@JsonView(Views.Detail.class)
	public Busqueda getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(Busqueda busqueda) {
		this.busqueda = busqueda;
	}

	@JsonView(Views.Detail.class)
	public List<ComercialResponse> getComerciales() {
		return comerciales;
	}

	public void setComerciales(List<ComercialResponse> comerciales) {
		this.comerciales = comerciales;
	}

}
