package com.asap.rrhh.inout;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class ExceptionDetail implements Serializable{

	public enum CodigoError {
		USER_PASS_INVALID(5, "Usuario y/o Clave inválido."),
		USER_EMAIL_EXIST(6, "Correo de Usuario en uso."),
		PERFIL_NOT_FOUND(8, "No se encontró el perfil solicitado."),
		ACTION_INVALID(9, "Accion inválida."),
		LOCALIDAD_INVALID(14, "Localidad inválida."),
		USER_NOT_FOUND(15, "No se encontró el usuario solicitado"),
		EMAIL_RESET_NOT_EXIST(16, "No se encontró una cuenta que contenga esa dirección de e-mail."),
		TOKEN_RESET_NOT_EXIST(17, "Este es un link de reseteo de contraseña inválido."),
		INTERNAL_SERVER_ERROR(18, "Error en tiempo de ejecución."),
		EMPRESA_NOT_FOUND(19, "No se encontró la empresa especificada."),
		AUTHORIZATION_EXCEPTION(20, "Error de autorización."),
		BAD_CREDENTIAL_EXCEPTION(21, "Error de credenciales."),
		ARGUMENT_NOT_VALID_EXCEPTION(22, "Argumento no válido."),
		NOT_FOUND_EXCEPTION(23, "Not Found Exception."),
		TIPO_PERFIL_NOT_FOUND(34, "No se encontró el Tipo de Perfil especificado"),
		FILE_NOT_FOUND(40, "No se encontró el archivo especificado"), 
		TOKEN_INVALID(41, "Token inválido."),
		COMERCIAL_NOT_FOUND(42, "Comercial no encontrado"),
		HUNTER_USER_INVALID(43,"Usuario sin Hunter"),
		HUNTER_NOT_FOUND(44, "Hunter no encontrado"),
		BUSQUEDA_NOT_FOUND(45, "Busqueda no encontrada"),
		HUNTER_BUSQUEDA_NOT_FOUND(46, "Busqueda/Hunter no se ha encontrado"),
		ARCHIVO_NOT_FOUND(47, "Archivo no encontrado"),
		ARCHIVO_BUSQUEDA_NOT_FOUND(48, "El Archvivo/Busqueda no se ha encontrado"),
		CANDIDATO_BUSQUEDA_NOT_FOUD(49, "No se encontro el Candidato/Busqueda");
		
		
		private int code;
		private String message;
		
		CodigoError(int code, String texto) {
			this.code = code;
			this.message = texto;
		}

		public int getCode() {
			return code;
		}

		public String getMessage() {
			return message;
		}
	}

	private HttpStatus status;
	private String message; // Mensaje de error adicional para soporte
	private int code;
	private String errorMessage;
	private StackTraceElement[] trace;

	private static final long serialVersionUID = 58545215L;

	public ExceptionDetail() {
	}

	public ExceptionDetail(HttpStatus status, CodigoError codigo, StackTraceElement[] trace) {
		this.status = status;
		this.code = codigo.getCode();
		this.message = codigo.getMessage();
		this.trace = trace;
	}

	public ExceptionDetail(HttpStatus status, CodigoError code, String errorMessage, StackTraceElement[] trace) {
		this.status = status;
		this.code = code.getCode();
		this.message = code.getMessage();
		this.errorMessage = errorMessage;
		this.trace = trace;
	}
	
	public HttpStatus getStatus() {
		return status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StackTraceElement[] getTrace() {
		return trace;
	}

	public void setTrace(StackTraceElement[] trace) {
		this.trace = trace;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
