package com.asap.rrhh.inout;

import java.sql.Timestamp;

import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.Candidato;
import com.asap.rrhh.model.Hunter;
import com.asap.rrhh.model.TipoStatus;

public class HistoriaStatusCandidato {

	private Candidato historiaCandidato;

	private Busqueda historiaBusqueda;

	private Hunter historiaHunter;

	private TipoStatus candidatoStatus;

	private double rbPretendida;

	private String historiaObservaciones;

	private Integer activoCandidatoBusqueda;

	private Timestamp fechaIngreso;

	public HistoriaStatusCandidato() {
		super();
	}

	public HistoriaStatusCandidato(Candidato historiaCandidato, Busqueda historiaBusqueda, Hunter historiaHunter,
			TipoStatus candidatoStatus, double rbPretendida, String historiaObservaciones,
			Integer activoCandidatoBusqueda) {
		super();
		this.historiaCandidato = historiaCandidato;
		this.historiaBusqueda = historiaBusqueda;
		this.historiaHunter = historiaHunter;
		this.candidatoStatus = candidatoStatus;
		this.rbPretendida = rbPretendida;
		this.historiaObservaciones = historiaObservaciones;
		this.activoCandidatoBusqueda = activoCandidatoBusqueda;
	}

	public HistoriaStatusCandidato(Candidato historiaCandidato , Busqueda historiaBusqueda , Hunter historiaHunter , TipoStatus candidatoStatus , double rbPretendida , String historiaObservaciones , Integer activoCandidatoBusqueda , Timestamp fechaIngreso) {
		this.historiaCandidato = historiaCandidato;
		this.historiaBusqueda = historiaBusqueda;
		this.historiaHunter = historiaHunter;
		this.candidatoStatus = candidatoStatus;
		this.rbPretendida = rbPretendida;
		this.historiaObservaciones = historiaObservaciones;
		this.activoCandidatoBusqueda = activoCandidatoBusqueda;
		this.fechaIngreso = fechaIngreso;
	}

	public Candidato getHistoriaCandidato() {
		return historiaCandidato;
	}

	public void setHistoriaCandidato(Candidato historiaCandidato) {
		this.historiaCandidato = historiaCandidato;
	}

	public String getHistoriaObservaciones() {
		return historiaObservaciones;
	}

	public void setHistoriaObservaciones(String historiaObservaciones) {
		this.historiaObservaciones = historiaObservaciones;
	}

	public Busqueda getHistoriaBusqueda() {
		return historiaBusqueda;
	}

	public void setHistoriaBusqueda(Busqueda historiaBusqueda) {
		this.historiaBusqueda = historiaBusqueda;
	}

	public Hunter getHistoriaHunter() {
		return historiaHunter;
	}

	public void setHistoriaHunter(Hunter historiaHunter) {
		this.historiaHunter = historiaHunter;
	}

	public TipoStatus getCandidatoStatus() {
		return candidatoStatus;
	}

	public void setCandidatoStatus(TipoStatus candidatoStatus) {
		this.candidatoStatus = candidatoStatus;
	}

	public Integer getActivoCandidatoBusqueda() {
		return activoCandidatoBusqueda;
	}

	public void setActivoCandidatoBusqueda(Integer activoCandidatoBusqueda) {
		this.activoCandidatoBusqueda = activoCandidatoBusqueda;
	}

	public double getRbPretendida() {
		return rbPretendida;
	}

	public void setRbPretendida(double rbPretendida) {
		this.rbPretendida = rbPretendida;
	}

	public Timestamp getFechaIngreso() {
		return fechaIngreso;
	}

	public HistoriaStatusCandidato setFechaIngreso(Timestamp fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
		return this;
	}
}
