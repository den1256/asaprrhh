package com.asap.rrhh.inout;

import lombok.Data;

@Data
public class CambioEstadoUsuarioRequest {

    protected int entityId;
    protected int entityActivo;

}
