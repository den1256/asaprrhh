package com.asap.rrhh.inout;

import lombok.Data;

@Data
public class RemplazoHunterResquest {
	
	 protected int busquedaId;
	 protected int hunterIdInhabilitar;
	 protected int hunterIdRemplazo;

}