package com.asap.rrhh.inout;

import com.asap.rrhh.model.Candidato;
import com.asap.rrhh.model.TipoStatus;
import com.fasterxml.jackson.annotation.JsonView;

public class CandidatoAmpliado {

	private Candidato candidatoAmpliado;

	private TipoStatus candidatoAmpliadoStatus;

	public CandidatoAmpliado() {
		super();
	}

	public CandidatoAmpliado(Candidato candidatoAmpliado, TipoStatus candidatoAmpliadoStatus) {
		super();
		this.candidatoAmpliado = candidatoAmpliado;
		this.candidatoAmpliadoStatus = candidatoAmpliadoStatus;
	}

	@JsonView(Views.Simple.class)
	public Candidato getCandidatoAmpliado() {
		return candidatoAmpliado;
	}

	public void setCandidatoAmpliado(Candidato candidatoAmpliado) {
		this.candidatoAmpliado = candidatoAmpliado;
	}

	@JsonView(Views.Simple.class)
	public TipoStatus getCandidatoAmpliadoStatus() {
		return candidatoAmpliadoStatus;
	}

	public void setCandidatoAmpliadoStatus(TipoStatus candidatoAmpliadoStatus) {
		this.candidatoAmpliadoStatus = candidatoAmpliadoStatus;
	}

}
