package com.asap.rrhh.inout;

import java.util.List;

import lombok.Data;

@Data
public class AsignarHunterBusqueda {

	protected Integer busquedaId;
	protected List<Integer> huntersIds;

}
