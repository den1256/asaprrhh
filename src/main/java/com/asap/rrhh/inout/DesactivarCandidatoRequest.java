package com.asap.rrhh.inout;

import lombok.Data;

@Data
public class DesactivarCandidatoRequest {

	protected int busquedaId;
	protected int hunterId;
	protected int candidatoId;

}
