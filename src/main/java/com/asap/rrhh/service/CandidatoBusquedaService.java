package com.asap.rrhh.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ArchivoRequest;
import com.asap.rrhh.inout.AsignarCvRequest;
import com.asap.rrhh.inout.CandidatoDetalleResponse;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.model.Archivo;
import com.asap.rrhh.model.ArchivoCvCandidato;
import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.CandidatoBusqueda;
import com.asap.rrhh.model.HistoriaStatus;
import com.asap.rrhh.repository.ArchivoCvCandidatoRepository;
import com.asap.rrhh.repository.ArchivoRepository;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.CandidatoBusquedaRepository;
import com.asap.rrhh.repository.HistoriaStatusRepository;
import com.asap.rrhh.util.DateUtil;

@Service
public class CandidatoBusquedaService extends BaseService {

	@Autowired
	CandidatoBusquedaRepository candidatoBusquedaRepository;

	@Autowired
	ArchivoRepository archivoRepository;

	@Autowired
	HistoriaStatusRepository historiaStatusRepository;

	@Autowired
	BusquedaRepository busquedaRepository;

	@Autowired
	ArchivoCvCandidatoRepository archivoCvCandidatoRepository;

	public boolean getCandidatoEnBusquedas(int candidatoId) {

		try {
			List<Integer> listaBusquedas = candidatoBusquedaRepository.getBusquedasPorCandidatoId(candidatoId);

			if (listaBusquedas.isEmpty()) {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public CandidatoDetalleResponse getDetallesCandidatoBusqueda(int busquedaId, int candidatoId, int hunterId) {

		Integer candidatoBusquedaId = candidatoBusquedaRepository.getIdCandBusq(busquedaId, candidatoId, hunterId);

		CandidatoBusqueda candidatoBusqueda = candidatoBusquedaRepository.getOne(candidatoBusquedaId);

		CandidatoDetalleResponse candidatoDetalle = new CandidatoDetalleResponse();
		candidatoDetalle.setCandidato(candidatoBusqueda.getCandidatoBusquedaCandidatoId());
		candidatoDetalle.setCandidatoBusquedaStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
		candidatoDetalle.setCandidatoBusquedaRBP(candidatoBusqueda.getCandidatoBusquedaRBP());
		candidatoDetalle.setCandidatoBusquedaActivo(candidatoBusqueda.getCandidatoBusquedaActivo());
		candidatoDetalle.setFechaIngreso(candidatoBusqueda.getFechaIngreso());
		return candidatoDetalle;

	}

	public void asignarCV(AsignarCvRequest asignarCv) throws NotFoundException {
		int candidatoBusquedaId = candidatoBusquedaRepository.getIdCandBusq(asignarCv.getBusquedaId(),
				asignarCv.getCandidatoId(), asignarCv.getHunterId());
		CandidatoBusqueda candidatoBusqueda = candidatoBusquedaRepository.findById(candidatoBusquedaId)
				.orElseThrow(() -> new NotFoundException(CodigoError.BUSQUEDA_NOT_FOUND));

		Integer nroArchivos = 0;

		List<Archivo> archivos = new ArrayList<>();

		for (ArchivoRequest archivoTemp : asignarCv.getArchivos()) {
			Integer archivoId = archivoTemp.getArchivoId();
			Archivo archivoLocal = new Archivo();
			archivoLocal.setArchivoId(archivoId);

			archivos.add(archivoLocal);
		}

		for (Archivo archivo : archivos) {
			archivoCvCandidatoRepository.save(new ArchivoCvCandidato(candidatoBusqueda, archivo));
			nroArchivos += 1;
		}

		if (nroArchivos > 1) {
			historiaStatusRepository
					.save(new HistoriaStatus(candidatoBusqueda, candidatoBusqueda.getCandidatoBusquedaStatusId(),
							"Se han asignado " + nroArchivos + " archivos al candidato", new DateUtil().timeNow(),
							candidatoBusqueda.getCandidatoBusquedaRBP()));
		} else {
			historiaStatusRepository
					.save(new HistoriaStatus(candidatoBusqueda, candidatoBusqueda.getCandidatoBusquedaStatusId(),
							"Se ha asignado " + nroArchivos + " archivo al candidato", new DateUtil().timeNow(),
							candidatoBusqueda.getCandidatoBusquedaRBP()));
		}
	}

	public int contarCandidatos(int idBusqueda) {
		return candidatoBusquedaRepository
				.countAllByCandidatoBusquedaBusquedaId_BusquedaIdAndCandidatoBusquedaStatusId_TipoStatusId(idBusqueda,
						7);
	}

	public void asignarCantCandidatos(int idBusqueda) throws NotFoundException {
		Busqueda busqueda = busquedaRepository.findById(idBusqueda)
				.orElseThrow(() -> new NotFoundException(CodigoError.BUSQUEDA_NOT_FOUND));
		int candidatos = this.contarCandidatos(idBusqueda);
		busqueda.setCantidadCandidatosSeleccionados(candidatos);

		busquedaRepository.save(busqueda);
	}

	public List<Archivo> findArchivosCvCandidatoById(int busquedaId, int hunterId, int candidatoId) {
		Integer candidatoBusquedaId = candidatoBusquedaRepository.getIdCandBusq(busquedaId, candidatoId, hunterId);
		Optional<List<Integer>> archivoscvId = archivoCvCandidatoRepository.getArchivoscv(candidatoBusquedaId);
		List<Archivo> archivos = new ArrayList<>();
		if (archivoscvId.isPresent()) {

			for (Integer archivoId : archivoscvId.get()) {

				String archivoNombre = archivoRepository.getArchivoNombre(archivoId);

				Archivo archivocv = new Archivo();
				archivocv.setArchivoId(archivoId);
				archivocv.setArchivoNombre(archivoNombre);

				archivos.add(archivocv);
			}
		}

		return archivos;
	}
}
