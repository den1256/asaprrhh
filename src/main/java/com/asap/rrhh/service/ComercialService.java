package com.asap.rrhh.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.AsignarComercialRequest;
import com.asap.rrhh.inout.ComercialResponse;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.Comercial;
import com.asap.rrhh.model.ComercialBusqueda;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.ComercialBusquedaRepository;
import com.asap.rrhh.repository.ComercialRepository;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.service.RolService;

@Service
public class ComercialService extends BaseService {

	@Autowired
	ComercialRepository comercialRepository;
	
	@Autowired
	ComercialBusquedaRepository comercialBusquedaRepository;
	
	@Autowired
	BusquedaRepository busquedaRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RolService rolService;

	public ComercialResponse findByid(int comercialId) throws NotFoundException {
		Comercial comercial = comercialRepository.findById(comercialId)
				.orElseThrow(() -> new NotFoundException(CodigoError.COMERCIAL_NOT_FOUND));

		ComercialResponse comercialResponse = new ComercialResponse(comercial.getComercialId(),
				comercial.getUsuario().getNombre(), comercial.getUsuario().getApellido(),
				comercial.getUsuario().getEmail(), comercial.getComercialActivo());

		return comercialResponse;
	}

	public void nuevoComercial(Usuario usuario) {
		Comercial comercial = new Comercial(1, usuario);

		comercialRepository.save(comercial);
	}

	public List<ComercialResponse> convertirListaComercial(List<Object[]> comercial) {
		List<ComercialResponse> comercialResponseList = new ArrayList<>();
		comercial.forEach(p -> {
			ComercialResponse comercialResponse = new ComercialResponse((int) p[0], (String) p[1], (String) p[2],
					(String) p[3], (int) p[4]);
			comercialResponseList.add(comercialResponse);
		});
		return comercialResponseList;
	}
	
	public Integer asignarComerciales(AsignarComercialRequest asignarComerciales) {
		
		List<Integer> comercialesExistentesId = new ArrayList<>();
		List<Integer> comercialBusquedaInactivo = comercialBusquedaRepository.getComercialesIdInactivos(asignarComerciales.getBusquedaId());
		List<Integer> comercialesLista = asignarComerciales.getComercialId();
		
		try {
			 comercialBusquedaRepository.getComercialesIdActuales(asignarComerciales.getBusquedaId());
		}
		catch (NullPointerException e) {
			comercialesExistentesId = null;
		}
		
		Busqueda busqueda = busquedaRepository.getOne(asignarComerciales.getBusquedaId());

		int cantidadComercialesNoAsignado = 0;

		for (Integer comercialId : comercialesLista) {

			if (comercialesExistentesId == null || !comercialesExistentesId.contains(comercialId) || comercialBusquedaInactivo.contains(comercialId)) {

				Comercial comercialActual = comercialRepository.getOne(comercialId);

				if (comercialBusquedaInactivo.contains(comercialId)) {

					ComercialBusqueda asignacion = comercialBusquedaRepository.getComercialBusquedaPorIds(busqueda.getBusquedaId(), comercialId).get(0);
					asignacion.setComercialBusquedaActivo(1);
					comercialBusquedaRepository.save(asignacion);

				} else {

					ComercialBusqueda asignacion = new ComercialBusqueda();
					asignacion.setBusquedaId(busqueda);
					asignacion.setComercialId(comercialActual);
					asignacion.setComercialBusquedaActivo(1);

					comercialBusquedaRepository.save(asignacion);
				}

			} else {
				cantidadComercialesNoAsignado = +1;
			}
		}
		return cantidadComercialesNoAsignado;
	}
	

}
