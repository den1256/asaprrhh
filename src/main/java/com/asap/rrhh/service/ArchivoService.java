package com.asap.rrhh.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.asap.rrhh.Exception.FileStorageException;
import com.asap.rrhh.model.Archivo;
import com.asap.rrhh.repository.ArchivoBusquedaRepository;
import com.asap.rrhh.repository.ArchivoCvCandidatoRepository;
import com.asap.rrhh.repository.ArchivoRepository;
import com.asap.rrhh.util.UniqueIdGenerator;

@Service
public class ArchivoService extends BaseService {

	@Value("${app.upload.dir:${user.home}}")
	public String uploadDir;

	@Autowired
	ArchivoRepository archivoRepository;

	@Autowired
	ArchivoCvCandidatoRepository archivoCvCandidatoRepository;

	@Autowired
	ArchivoBusquedaRepository archivoBusquedaRepository;

	public Archivo storeFile(MultipartFile file) {

		Path copyLocation;

		String newName = this.generateFileId();

		String fileName = StringUtils.cleanPath(newName + file.getOriginalFilename());

		try {

			copyLocation = Paths.get(this.uploadDir + File.separator + StringUtils.cleanPath(fileName));

			if (fileName.contains("..")) {
				throw new FileStorageException("Disculpa! La ruta del archivo no es valida " + fileName);
			}

			Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
			new File(copyLocation.toString()).setReadable(true, false);

			Archivo dbFile = new Archivo(fileName, file.getContentType());

			return archivoRepository.save(dbFile);
		} catch (IOException ex) {
			throw new FileStorageException("No se pudo guardar el archivo " + fileName + ". Por favor, intente nuevamente!", ex);
		}
	}

	public Archivo getFile(int fileId) throws FileNotFoundException {
		return archivoRepository.findById(fileId).orElseThrow(() -> new FileNotFoundException("No se encontró el archivo con el id: " + fileId));
	}

	public void eliminarArchivoCandidato(int archivoId, HttpServletRequest request) {
		try {
			archivoCvCandidatoRepository.eliminarArchivoCandidato(archivoId);
		} catch (NullPointerException e) {
			LOG_info(request, "Error que aun no sabemos resolver al borrar un archivo, pero si fue eliminado");
		}
	}

	public void eliminarArchivoBusqueda(int archivoId, HttpServletRequest request) {
		try {
			archivoBusquedaRepository.eliminarArchivoBusqueda(archivoId);
		} catch (NullPointerException e) {
			LOG_info(request, "Error que aun no sabemos resolver al borrar un archivo, pero si fue eliminado");
		}
	}

	public List<String> respaldarArchivos() {

		List<String> resultados = new ArrayList<>();

		List<Archivo> todosLosArchivos = archivoRepository.findAll();

		for (Archivo archivoActual : todosLosArchivos) {

			if (archivoActual.getArchivoDatos() != null) {
				String nombreRemplazo = this.generateFileId() + archivoActual.getArchivoNombre();
				String copyLocation = uploadDir + "/" + nombreRemplazo;

				try {

					writeBytesToFile(copyLocation, archivoActual.getArchivoDatos());

					archivoActual.setArchivoNombre(nombreRemplazo);
					archivoActual.setArchivoRespaldado(true);
					archivoRepository.save(archivoActual);

				} catch (IOException ex) {

					ex.printStackTrace();

					resultados.add("Falló al guardar el Archivo: " + archivoActual.getArchivoNombre() + " con Id: " + archivoActual.getArchivoId());

				}
			}

		}

		if (resultados.isEmpty()) {
			resultados.add("La tarea se completo sin Fallas");
		}

		return resultados;
	}

	public void eliminarArchivosBaseDatos() {

		byte[] bytes = null;

		List<Archivo> todosLosArchivos = archivoRepository.findAll();

		for (Archivo archivoActual : todosLosArchivos) {

			if ((archivoActual.getArchivoRespaldado() != null) && archivoActual.getArchivoRespaldado()) {

				archivoActual.setArchivoDatos(bytes);
				archivoRepository.save(archivoActual);

			}
		}
	}

	private String generateFileId() {
		return new UniqueIdGenerator().generateLongId().toString();
	}

	private void writeBytesToFile(String fileOutput, byte[] bytes) throws IOException {

		try (FileOutputStream fos = new FileOutputStream(fileOutput)) {
			fos.write(bytes);
			fos.flush();
		}

	}
}
