package com.asap.rrhh.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.inout.HunterResponse;
import com.asap.rrhh.model.Hunter;
import com.asap.rrhh.repository.HunterBusquedaRepository;
import com.asap.rrhh.repository.HunterRepository;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.jwt.JwtProvider;
import com.asap.rrhh.security.jwt.JwtTokenFilter;
import com.asap.rrhh.security.repository.UsuarioRepository;
import com.asap.rrhh.security.service.RolService;

@Service
public class HunterService extends BaseService {

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	JwtTokenFilter jwtTokenFilter;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RolService rolService;

	@Autowired
	HunterBusquedaRepository hunterBusquedaRepository;

	
	public void nuevoHunter(Usuario usuario) {
		Hunter hunter = new Hunter(1, usuario);

		hunterRepository.save(hunter);
	}

	public Hunter getHunter(HttpServletRequest req) throws NotFoundException {
		String token = jwtTokenFilter.getToken(req);
		String nombreUsuario = jwtProvider.getNombreUsuarioFromToken(token);

		return hunterRepository.findByUsuario_NombreUsuario(nombreUsuario).orElseThrow(() -> new NotFoundException(CodigoError.HUNTER_USER_INVALID));
	}

	public List<HunterResponse> convertirListaHunter(List<Object[]> hunter) {
		List<HunterResponse> hunterResponseList = new ArrayList<>();
		hunter.forEach(p -> {
			HunterResponse hunterResponse = new HunterResponse((int) p[0], (String) p[1], (String) p[2],
					(String) p[3], (int) p[4]);
			hunterResponseList.add(hunterResponse);
		});
		return hunterResponseList;
	}

	public HunterResponse convertirHunter(Hunter hunter) {
		return new HunterResponse(hunter.getHunterId(), hunter.getUsuario().getNombre(), hunter.getUsuario().getApellido(), hunter.getUsuario().getEmail(),
				hunter.getHunterActivo());
	}

}
