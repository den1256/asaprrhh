package com.asap.rrhh.service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asap.rrhh.model.Actividad;
import com.asap.rrhh.model.Comercial;
import com.asap.rrhh.model.Hunter;
import com.asap.rrhh.repository.ActividadRepository;
import com.asap.rrhh.repository.ComercialBusquedaRepository;
import com.asap.rrhh.repository.ComercialRepository;
import com.asap.rrhh.repository.HunterBusquedaRepository;
import com.asap.rrhh.repository.HunterRepository;
import com.asap.rrhh.security.service.EmailService;

@Service
public class NotificacionesService {

	@Autowired
	ActividadRepository actividadRepository;

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	HunterBusquedaRepository hunterBusquedaRepository;

	@Autowired
	ComercialRepository comercialRepository;

	@Autowired
	ComercialBusquedaRepository comercialBusquedaRepository;

	@Autowired
	private EmailService emailService;

	@SuppressWarnings("unchecked")
	public void enviarNotificacionesDiarias() throws Exception {

		LocalDate hoy = LocalDate.now();
		List<Integer> listaDeHuntersIds = new ArrayList<>();
		List<Integer> listaDeComercialesIds = new ArrayList<>();

		if (hoy.getDayOfWeek().getValue() != 6 || hoy.getDayOfWeek().getValue() != 7) {

			if (hoy.getDayOfWeek().getValue() == 1) {
				try {
					hoy = hoy.minusDays(2);
				} catch (DateTimeException e) {
					e.printStackTrace();
				}
			}

			List<Integer> busquedasIds = actividadRepository.getBusquedasIdDeActividades(hoy.minusDays(1).toString(),
					hoy.plusDays(1).toString());

			for (Integer busquedaId : busquedasIds) {

				List<Integer> huntersIds = hunterBusquedaRepository.getHuntersIdActuales(busquedaId);
				List<Integer> comercialesIds = comercialBusquedaRepository.getComercialesIdActuales(busquedaId);

				for (Integer hunterId : huntersIds) {
					if (!listaDeHuntersIds.contains(hunterId)) {
						listaDeHuntersIds.add(hunterId);
					}
				}

				for (Integer comercialId : comercialesIds) {
					if (!listaDeComercialesIds.contains(comercialId)) {
						listaDeComercialesIds.add(comercialId);
					}
				}

			}

			for (Integer hunterId : listaDeHuntersIds) {

				Hunter hunter = hunterRepository.getOne(hunterId);

				List<Object> todasLasActividades = new ArrayList<>();
				List<Integer> busquedasActivasDelHunter = hunterBusquedaRepository
						.getListaIdsBusquedasActivas(hunterId);

				for (Integer busquedaId : busquedasActivasDelHunter) {
					if (busquedasIds.contains(busquedaId)) {
						List<Actividad> actividadesPorBusqueda = actividadRepository.getActividadesDiarias(
								hoy.minusDays(1).toString(), hoy.plusDays(1).toString(), busquedaId);
						todasLasActividades.add(actividadesPorBusqueda);
					}

				}

				StringBuilder detallesActividades = new StringBuilder();

				for (Object actividades : todasLasActividades) {

					List<Actividad> listaActividades = (List<Actividad>) actividades;

					for (Actividad actividad : listaActividades) {
						detallesActividades.append("<p style=\"padding-left: 1rem;\"><strong>"
								+ actividad.getDescripcion() + " </strong>");
						detallesActividades.append(" " + actividad.getDescripcionDetalle() + " </p>");
						detallesActividades.append("<br>");
					}

				}

				String subject = "Notificaciones de Actividad Aplicación AsapCV RRHH";

				String body = "<body style=\"color: rgb(10, 8, 114); background-color: #dadeec\">\r\n"
						+ "    <p style=\"color: rgb(255, 255, 255); background-color: rgb(8, 6, 95); padding: 1rem;\"><strong>ASAP Consulting</strong></p>\r\n"
						+ "    <br>\r\n"
						+ "    <h2 style=\"padding-left: 1rem;\">Se han realizado los siguientes cambios:</h2>\r\n"
						+ "    <br>\r\n" + detallesActividades.toString() + "\r\n" + "</body>";

				emailService.sendEmail(hunter.getUsuario().getEmail(), subject, body);

			}

			for (Integer comercialId : listaDeComercialesIds) {

				Comercial comercial = comercialRepository.getOne(comercialId);

				List<Object> todasLasActividades = new ArrayList<>();
				List<Integer> busquedasActivasDelComercial = comercialBusquedaRepository
						.getListaIdsBusquedasActivas(comercialId);

				for (Integer busquedaId : busquedasActivasDelComercial) {
					if (busquedasIds.contains(busquedaId)) {
						List<Actividad> actividadesPorBusqueda = actividadRepository.getActividadesDiarias(
								hoy.minusDays(1).toString(), hoy.plusDays(1).toString(), busquedaId);
						todasLasActividades.add(actividadesPorBusqueda);
					}

				}

				StringBuilder detallesActividades = new StringBuilder();

				for (Object actividades : todasLasActividades) {

					List<Actividad> listaActividades = (List<Actividad>) actividades;

					for (Actividad actividad : listaActividades) {
						detallesActividades.append("<p style=\"padding-left: 1rem;\"><strong>"
								+ actividad.getDescripcion() + " </strong>");
						detallesActividades.append(" " + actividad.getDescripcionDetalle() + " </p>");
						detallesActividades.append("<br>");
					}

				}

				String subject = "Notificaciones de Actividad Aplicación AsapCV RRHH";

				String body = "<body style=\"color: rgb(10, 8, 114); background-color: #dadeec\">\r\n"
						+ "    <p style=\"color: rgb(255, 255, 255); background-color: rgb(8, 6, 95); padding: 1rem;\"><strong>ASAP Consulting</strong></p>\r\n"
						+ "    <br>\r\n"
						+ "    <h2 style=\"padding-left: 1rem;\">Se han realizado los siguientes cambios:</h2>\r\n"
						+ "    <br>\r\n" + detallesActividades.toString() + "\r\n" + "</body>";

				emailService.sendEmail(comercial.getUsuario().getEmail(), subject, body);

			}

		}

	}

}
