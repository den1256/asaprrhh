package com.asap.rrhh.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ArchivoRequest;
import com.asap.rrhh.inout.AsignarArchivoBusquedaRequest;
import com.asap.rrhh.inout.AsignarCandidatoBusqueda;
import com.asap.rrhh.inout.AsignarHunterBusqueda;
import com.asap.rrhh.inout.CandidatoAmpliado;
import com.asap.rrhh.inout.ComercialResponse;
import com.asap.rrhh.inout.DesactivarCandidatoRequest;
import com.asap.rrhh.inout.DetalleBusqueda;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.inout.HistoriaStatusCandidato;
import com.asap.rrhh.inout.HunterCandidato;
import com.asap.rrhh.inout.HunterResponse;
import com.asap.rrhh.inout.RemplazoHunterResquest;
import com.asap.rrhh.inout.TotalDetalleBusqueda;
import com.asap.rrhh.model.Archivo;
import com.asap.rrhh.model.ArchivoBusqueda;
import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.Candidato;
import com.asap.rrhh.model.CandidatoBusqueda;
import com.asap.rrhh.model.Comercial;
import com.asap.rrhh.model.HistoriaStatus;
import com.asap.rrhh.model.Hunter;
import com.asap.rrhh.model.HunterBusqueda;
import com.asap.rrhh.model.TipoStatus;
import com.asap.rrhh.repository.ArchivoBusquedaRepository;
import com.asap.rrhh.repository.ArchivoRepository;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.CandidatoBusquedaRepository;
import com.asap.rrhh.repository.CandidatoRepository;
import com.asap.rrhh.repository.ClienteRepository;
import com.asap.rrhh.repository.ComercialBusquedaRepository;
import com.asap.rrhh.repository.ComercialRepository;
import com.asap.rrhh.repository.HistoriaStatusRepository;
import com.asap.rrhh.repository.HunterBusquedaRepository;
import com.asap.rrhh.repository.HunterRepository;
import com.asap.rrhh.repository.TipoStatusRepository;
import com.asap.rrhh.security.entity.Usuario;
import com.asap.rrhh.security.jwt.JwtProvider;
import com.asap.rrhh.security.jwt.JwtTokenFilter;
import com.asap.rrhh.security.repository.UsuarioRepository;
import com.asap.rrhh.util.DateUtil;
import com.asap.rrhh.util.LocaleUtils;

@Service
public class BusquedaService extends BaseService {

	@Autowired
	CandidatoBusquedaRepository candidatoBusquedaRepository;

	@Autowired
	HunterBusquedaRepository hunterBusquedaRepository;

	@Autowired
	TipoStatusRepository tipoStatusRepository;

	@Autowired
	HistoriaStatusRepository historiaStatusRepository;

	@Autowired
	CandidatoRepository candidatoRepository;

	@Autowired
	BusquedaRepository busquedaRepository;

	@Autowired
	ArchivoBusquedaRepository archivoBusquedaRepository;

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	ArchivoRepository archivoRepository;

	@Autowired
	JwtTokenFilter jwtTokenFilter;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	HunterService hunterService;

	@Autowired
	CandidatoBusquedaService candidatoBusquedaService;

	@Autowired
	ActividadService actividadService;

	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	ComercialRepository comercialRepository;
	
	@Autowired
	ComercialBusquedaRepository comercialBusquedaRepository;

	public boolean buscarEnLista(List<Integer> ids, Integer idComparar) {

		Integer[] lista = new Integer[ids.size()];

		for (int i = 0; i < ids.size(); i++) {
			lista[i] = ids.get(i);
		}

		return Arrays.asList(lista).contains(idComparar);
	}

	public List<DetalleBusqueda> ordenarCronologia(List<DetalleBusqueda> detalleBusqueda) {

		List<DetalleBusqueda> detalleBusquedaOrdenada = new ArrayList<>();

		for (int i = detalleBusqueda.size(); i > 0; i--) {
			detalleBusquedaOrdenada.add(detalleBusqueda.get(i - 1));
		}
		return detalleBusquedaOrdenada;
	}

	public void candidatosReasignar(RemplazoHunterResquest reasignarInhabilitar) {

		if (reasignarInhabilitar.getHunterIdRemplazo() == 0) {

			Integer hunterBusquedaId = hunterBusquedaRepository.getHunterBusquedaId(
					reasignarInhabilitar.getBusquedaId(), reasignarInhabilitar.getHunterIdInhabilitar());

			HunterBusqueda hunterBusqueda = hunterBusquedaRepository.getOne(hunterBusquedaId);

			hunterBusqueda.setActivo(0);
			hunterBusquedaRepository.save(hunterBusqueda);

		} else {

			Hunter hunterOld = new Hunter();
			Hunter hunterNew = new Hunter();
			Busqueda busquedaOne = new Busqueda();
			hunterOld = hunterRepository.getOne(reasignarInhabilitar.getHunterIdInhabilitar());
			hunterNew = hunterRepository.getOne(reasignarInhabilitar.getHunterIdRemplazo());
			busquedaOne = busquedaRepository.getOne(reasignarInhabilitar.getBusquedaId());

			List<Integer> candidatos = candidatoBusquedaRepository
					.getCandidatosIdsDelHunter(busquedaOne.getBusquedaId(), hunterOld.getHunterId());

			Integer hunterBusquedaId = hunterBusquedaRepository.getHunterBusquedaId(busquedaOne.getBusquedaId(),
					hunterOld.getHunterId());

			HunterBusqueda hunterBusqueda = hunterBusquedaRepository.getOne(hunterBusquedaId);

			hunterBusqueda.setActivo(0);
			hunterBusquedaRepository.save(hunterBusqueda);

			List<Integer> busquedasIdsNuevoHunter = hunterBusquedaRepository
					.getListaIdsBusquedas(hunterNew.getHunterId());

			if (!buscarEnLista(busquedasIdsNuevoHunter, busquedaOne.getBusquedaId())) {

				hunterBusquedaRepository.save(new HunterBusqueda(hunterNew, busquedaOne, 1));

			} else {

				Integer hunterBusquedaNewId = hunterBusquedaRepository.getHunterBusquedaId(busquedaOne.getBusquedaId(),
						hunterNew.getHunterId());

				HunterBusqueda hunterBusquedaNew = hunterBusquedaRepository.getOne(hunterBusquedaNewId);

				if (hunterBusquedaNew.getActivo() == 0) {
					hunterBusquedaNew.setActivo(1);
					hunterBusquedaRepository.save(hunterBusquedaNew);
				}

			}

			CandidatoBusqueda candidatoBusqueda = new CandidatoBusqueda();

			for (Integer candidatoId : candidatos) {

				Integer candidatoBusquedaId = candidatoBusquedaRepository.getIdCandBusq(busquedaOne.getBusquedaId(),
						candidatoId, hunterOld.getHunterId());
				candidatoBusqueda = candidatoBusquedaRepository.getOne(candidatoBusquedaId);

				if (candidatoBusqueda.getCandidatoBusquedaActivo() == 1) {

					HistoriaStatus historiaCandidato = new HistoriaStatus();
					historiaCandidato.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
					historiaCandidato.setHistoriaStatusObservaciones(hunterNew.getUsuario().getNombre() + " "
							+ hunterNew.getUsuario().getApellido() + " es el nuevo recruiter del candidato");
					historiaCandidato.setHistoriaStatusFecha(new DateUtil().timeNow());
					historiaCandidato.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
					historiaStatusRepository.save(historiaCandidato);

					candidatoBusqueda.setCandidatoBusquedaHunterId(hunterNew);
					candidatoBusquedaRepository.save(candidatoBusqueda);

					HistoriaStatus historiaCandidato1 = new HistoriaStatus();
					historiaCandidato1.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
					historiaCandidato1.setHistoriaStatusObservaciones(hunterOld.getUsuario().getNombre() + " "
							+ hunterOld.getUsuario().getApellido() + " era el antiguo recruiter del candidato");
					historiaCandidato1.setHistoriaStatusFecha(new DateUtil().timeNow());
					historiaCandidato1.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
					historiaStatusRepository.save(historiaCandidato1);

				}

			}
		}
	}

	public TotalDetalleBusqueda getTotalDetalleBusqueda(Usuario usuarioActivo, List<Integer> listaBusquedasId) {

		List<Busqueda> busquedas = new ArrayList<>();

		if (listaBusquedasId.isEmpty()) {
			busquedas = busquedaRepository.findAll();
		} else {
			for (Integer id : listaBusquedasId) {
				if (id != 0) {
					Busqueda busquedaActual = busquedaRepository.getOne(id);
					busquedas.add(busquedaActual);
				}
			}
		}
		List<DetalleBusqueda> detalleBusquedaHunterActivas = new ArrayList<>();
		List<DetalleBusqueda> detalleBusquedaHunterCerradas = new ArrayList<>();
		List<DetalleBusqueda> detalleBusquedaActiva = new ArrayList<>();
		List<DetalleBusqueda> detalleBusquedaCerrada = new ArrayList<>();
		List<Object[]> cantidades = candidatoBusquedaRepository.getCandidatosBusqueda();
		List<Object[]> statusCandidatos = candidatoBusquedaRepository.getCandidatosStatusActual();
		TotalDetalleBusqueda totalDetalleBusqueda = new TotalDetalleBusqueda();
		Hunter hunter = hunterRepository.findByUsuario_Id(usuarioActivo.getId());

		if (hunter != null) {

			for (Busqueda busqueda : busquedas) {
				int busqId = busqueda.getBusquedaId();
				int archivo;
				List<HunterResponse> ListaDehunters = new ArrayList<>();
				List<ComercialResponse> ListaDeComerciales = new ArrayList<>();

				List<Integer> resultados = obtenerCantidades(busqId, cantidades, statusCandidatos);

				int cantCandidatos = resultados.get(0);
				int enviadoCliente = resultados.get(1);
				int candidatoEntrevista = resultados.get(2);

				try {
					archivo = Integer.parseInt(
							String.valueOf(archivoBusquedaRepository.getArchivoPorBusqueda(busqId).get(0)[0]));
				} catch (Exception e) {
					archivo = 0;
				}

				List<Integer> huntersIds;
				List<Integer> comercialesIds;

				try {
					huntersIds = hunterBusquedaRepository.getHuntersIdActuales(busqId);
					comercialesIds = comercialBusquedaRepository.getComercialesIdActuales(busqId);
					
					int hunterId = hunter.getHunterId();
					if (buscarEnLista(huntersIds, hunterId)) {
						
						huntersIds.forEach(p -> {
							Hunter hunterOne = hunterRepository.getOne(p);
							HunterResponse hunterResponse = new HunterResponse(hunterOne.getHunterId(),
									hunterOne.getUsuario().getNombre(), hunterOne.getUsuario().getApellido(),
									hunterOne.getUsuario().getEmail(), hunterOne.getHunterActivo());
							ListaDehunters.add(hunterResponse);
						});
						
						comercialesIds.forEach(p -> {
							Comercial comercialOne = comercialRepository.getOne(p);
							ComercialResponse comercialResponse = new ComercialResponse(comercialOne.getComercialId(),
									comercialOne.getUsuario().getNombre(), comercialOne.getUsuario().getApellido(),
									comercialOne.getUsuario().getEmail(), comercialOne.getComercialActivo());
							ListaDeComerciales.add(comercialResponse);
						});
						
						if (busqueda.getTipoEstado().getTipoEstadoId() == 1) {
							detalleBusquedaHunterActivas.add(new DetalleBusqueda(cantCandidatos, enviadoCliente,
									candidatoEntrevista, archivo, ListaDehunters, ListaDeComerciales, busqueda));
						} else {
							detalleBusquedaHunterCerradas.add(new DetalleBusqueda(cantCandidatos, enviadoCliente,
									candidatoEntrevista, archivo, ListaDehunters, ListaDeComerciales, busqueda));
						}

					}

					if (!buscarEnLista(huntersIds, hunterId) && busqueda.getTipoEstado().getTipoEstadoId() == 1) {
						
						huntersIds.forEach(p -> {
							Hunter hunterOne = hunterRepository.getOne(p);
							HunterResponse hunterResponse = new HunterResponse(hunterOne.getHunterId(),
									hunterOne.getUsuario().getNombre(), hunterOne.getUsuario().getApellido(),
									hunterOne.getUsuario().getEmail(), hunterOne.getHunterActivo());
							ListaDehunters.add(hunterResponse);
						});
						
						comercialesIds.forEach(p -> {
							Comercial comercialOne = comercialRepository.getOne(p);
							ComercialResponse comercialResponse = new ComercialResponse(comercialOne.getComercialId(),
									comercialOne.getUsuario().getNombre(), comercialOne.getUsuario().getApellido(),
									comercialOne.getUsuario().getEmail(), comercialOne.getComercialActivo());
							ListaDeComerciales.add(comercialResponse);
						});
						
						detalleBusquedaActiva.add(new DetalleBusqueda(cantCandidatos, enviadoCliente,
								candidatoEntrevista, archivo, ListaDehunters, ListaDeComerciales, busqueda));
					}

					if (!buscarEnLista(huntersIds, hunterId) && busqueda.getTipoEstado().getTipoEstadoId() != 1) {
						
						huntersIds.forEach(p -> {
							Hunter hunterOne = hunterRepository.getOne(p);
							HunterResponse hunterResponse = new HunterResponse(hunterOne.getHunterId(),
									hunterOne.getUsuario().getNombre(), hunterOne.getUsuario().getApellido(),
									hunterOne.getUsuario().getEmail(), hunterOne.getHunterActivo());
							ListaDehunters.add(hunterResponse);
						});
						
						comercialesIds.forEach(p -> {
							Comercial comercialOne = comercialRepository.getOne(p);
							ComercialResponse comercialResponse = new ComercialResponse(comercialOne.getComercialId(),
									comercialOne.getUsuario().getNombre(), comercialOne.getUsuario().getApellido(),
									comercialOne.getUsuario().getEmail(), comercialOne.getComercialActivo());
							ListaDeComerciales.add(comercialResponse);
						});
						
						detalleBusquedaCerrada.add(new DetalleBusqueda(cantCandidatos, enviadoCliente,
								candidatoEntrevista, archivo, ListaDehunters, ListaDeComerciales, busqueda));
					}
				} catch (IndexOutOfBoundsException e) {
				}
			}

			totalDetalleBusqueda.setBusquedasHunterActivas(ordenarCronologia(detalleBusquedaHunterActivas));
			totalDetalleBusqueda.setBusquedasHunterCerradas(ordenarCronologia(detalleBusquedaHunterCerradas));
			totalDetalleBusqueda.setBusquedasActivas(ordenarCronologia(detalleBusquedaActiva));
			totalDetalleBusqueda.setBusquedasCerradas(ordenarCronologia(detalleBusquedaCerrada));

		} else {

			for (Busqueda busqueda : busquedas) {

				int busqId = busqueda.getBusquedaId();
				int archivo;
				List<HunterResponse> ListaDehunters = new ArrayList<>();
				List<ComercialResponse> ListaDeComerciales = new ArrayList<>();
				List<Integer> resultados = obtenerCantidades(busqId, cantidades, statusCandidatos);

				int cantCandidatos = resultados.get(0);
				int enviadoCliente = resultados.get(1);
				int candidatoEntrevista = resultados.get(2);

				try {
					archivo = Integer.parseInt(
							String.valueOf(archivoBusquedaRepository.getArchivoPorBusqueda(busqId).get(0)[0]));
				} catch (Exception e) {
					archivo = 0;
				}

				List<Integer> huntersIds = hunterBusquedaRepository.getHuntersIdActuales(busqId);
				huntersIds.forEach(p -> {
					Hunter hunterOne = hunterRepository.getOne(p);
					HunterResponse hunterResponse = new HunterResponse(hunterOne.getHunterId(),
							hunterOne.getUsuario().getNombre(), hunterOne.getUsuario().getApellido(),
							hunterOne.getUsuario().getEmail(), hunterOne.getHunterActivo());
					ListaDehunters.add(hunterResponse);
				});

				List<Integer> comercialesIds = comercialBusquedaRepository.getComercialesIdActuales(busqId);
				comercialesIds.forEach(p -> {
					Comercial comercialOne = comercialRepository.getOne(p);
					ComercialResponse comercialResponse = new ComercialResponse(comercialOne.getComercialId(),
							comercialOne.getUsuario().getNombre(), comercialOne.getUsuario().getApellido(),
							comercialOne.getUsuario().getEmail(), comercialOne.getComercialActivo());
					ListaDeComerciales.add(comercialResponse);
				});
				
				if (busqueda.getTipoEstado().getTipoEstadoId() == 1) {
					detalleBusquedaActiva.add(new DetalleBusqueda(cantCandidatos, enviadoCliente, candidatoEntrevista,
							archivo, ListaDehunters, ListaDeComerciales, busqueda));

				} else {
					detalleBusquedaCerrada.add(new DetalleBusqueda(cantCandidatos, enviadoCliente, candidatoEntrevista,
							archivo, ListaDehunters, ListaDeComerciales, busqueda));
				}
			}

			totalDetalleBusqueda.setBusquedasActivas(ordenarCronologia(detalleBusquedaActiva));
			totalDetalleBusqueda.setBusquedasCerradas(ordenarCronologia(detalleBusquedaCerrada));

		}
		return totalDetalleBusqueda;
	}

	public List<HunterCandidato> getHunterCandidatos(int busquedaId, HttpServletRequest req) throws NotFoundException {
		List<Object[]> listaHunterCandidatos;
		List<Integer> huntersIdsDesordenado;
		List<Integer> huntersIds = new ArrayList<>();
		List<HunterCandidato> listaFinal = new ArrayList<>();

		candidatoBusquedaService.asignarCantCandidatos(busquedaId);

		try {

			String token = jwtTokenFilter.getToken(req);
			String nombreUsuario = jwtProvider.getNombreUsuarioFromToken(token);
			Usuario usuarioActivo = usuarioRepository.findByNombreUsuario(nombreUsuario)
					.orElseThrow(() -> new NotFoundException(CodigoError.USER_NOT_FOUND));
			Hunter hunterOne = hunterRepository.findByUsuario_Id(usuarioActivo.getId());
			HunterResponse hunter = hunterService.convertirHunter(hunterOne);
			Integer usuarioId = hunter.getHunterId();

			huntersIdsDesordenado = hunterBusquedaRepository.getHuntersIdActuales(busquedaId);

			if (buscarEnLista(huntersIdsDesordenado, usuarioId)) {
				huntersIds.add(usuarioId);
			}

			for (Integer hunterId : huntersIdsDesordenado) {
				if (hunterId != usuarioId) {
					huntersIds.add(hunterId);
				}
			}

		} catch (Exception e) {
			huntersIds = hunterBusquedaRepository.getHuntersIdActuales(busquedaId);
		}

		listaHunterCandidatos = candidatoBusquedaRepository.getCandidatosDelHunter(busquedaId);

		for (int i = 0; i < huntersIds.size(); i++) {
			Hunter hunterOne = hunterRepository.getOne(huntersIds.get(i));
			HunterResponse hunterResponse = this.hunterService.convertirHunter(hunterOne);
			List<CandidatoAmpliado> listaCandidatos = new ArrayList<>();
			for (Object[] hunterCandidato : listaHunterCandidatos) {
				int hunterId = Integer.parseInt(String.valueOf(hunterCandidato[0]));
				if (huntersIds.get(i) == hunterId) {
					int id = Integer.parseInt(String.valueOf(hunterCandidato[1]));
					Candidato candidato = candidatoRepository.getOne(id);
					List<Object[]> statusId = candidatoBusquedaRepository.getIdCandStatus(busquedaId, id, hunterId);
					int idStatus = Integer.parseInt(String.valueOf(statusId.get(0)[0]));
					listaCandidatos.add(new CandidatoAmpliado(candidato, tipoStatusRepository.getOne(idStatus)));
				}
			}
			listaFinal.add(new HunterCandidato(hunterResponse, listaCandidatos));
		}

		return listaFinal;
	}

	public Integer getHunterCandidate(AsignarHunterBusqueda asignarHunterBusqueda) {
		List<Integer> huntersExistentesId = new ArrayList<>();
		List<Integer> hunterBusquedaInactivo = hunterBusquedaRepository
				.getHuntersIdInactivos(asignarHunterBusqueda.getBusquedaId());
		List<Integer> huntersLista = asignarHunterBusqueda.getHuntersIds();
		Busqueda busqueda = busquedaRepository.getOne(asignarHunterBusqueda.getBusquedaId());

		try {
			huntersExistentesId = hunterBusquedaRepository.getHuntersIdActuales(asignarHunterBusqueda.getBusquedaId());
		} catch (NullPointerException e) {
			huntersExistentesId = null;
		}

		int cantidadHunterNoAsignado = 0;

		for (Integer hunterId : huntersLista) {

			if (huntersExistentesId == null || !huntersExistentesId.contains(hunterId)
					|| hunterBusquedaInactivo.contains(hunterId)) {

				Hunter hunterActual = hunterRepository.getOne(hunterId);

				if (hunterBusquedaInactivo.contains(hunterId)) {

					HunterBusqueda asignacion = hunterBusquedaRepository
							.getHunterBusquedaPorIds(busqueda.getBusquedaId(), hunterId).get(0);
					asignacion.setActivo(1);
					hunterBusquedaRepository.save(asignacion);

				} else {

					HunterBusqueda asignacion = new HunterBusqueda();
					asignacion.setHunterBusquedaBusquedaId(busqueda);
					asignacion.setHunterBusquedaHunterId(hunterActual);
					asignacion.setActivo(1);

					hunterBusquedaRepository.save(asignacion);
				}

			} else {
				cantidadHunterNoAsignado = +1;
			}
		}
		return cantidadHunterNoAsignado;
	}

	public void candidatoBusqueda(AsignarCandidatoBusqueda asignarcandidatoBusqueda) {
		HistoriaStatus historiaCandidato = new HistoriaStatus();
		CandidatoBusqueda candidatoBusqueda = new CandidatoBusqueda();

		TipoStatus statusCandidato = tipoStatusRepository.getOne(2);

		candidatoBusqueda.setCandidatoBusquedaBusquedaId(
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaBusquedaId());
		candidatoBusqueda.setCandidatoBusquedaHunterId(
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaHunterId());
		candidatoBusqueda.setCandidatoBusquedaCandidatoId(
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaCandidatoId());
		candidatoBusqueda.setCandidatoBusquedaStatusId(statusCandidato);
		candidatoBusqueda.setCandidatoBusquedaRBP(0);
		candidatoBusqueda.setCandidatoBusquedaFechaCambio(new DateUtil().timeNow());
		candidatoBusqueda.setCandidatoBusquedaActivo(1);

		candidatoBusquedaRepository.save(candidatoBusqueda);

		historiaCandidato.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
		historiaCandidato.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
		historiaCandidato.setHistoriaStatusObservaciones("Asignado a la búsqueda");
		historiaCandidato.setHistoriaStatusFecha(new DateUtil().timeNow());

		historiaStatusRepository.save(historiaCandidato);

		if (!asignarcandidatoBusqueda.getObservacion().trim().isEmpty()) {
			HistoriaStatus historiaCandidato1 = new HistoriaStatus();
			historiaCandidato1.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
			historiaCandidato1.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
			historiaCandidato1.setHistoriaStatusObservaciones(asignarcandidatoBusqueda.getObservacion());
			historiaCandidato1.setHistoriaStatusFecha(new DateUtil().timeNow());
			historiaStatusRepository.save(historiaCandidato1);
		}
	}

	public void activarCandidatoBusqueda(AsignarCandidatoBusqueda asignarcandidatoBusqueda) {
		HistoriaStatus historiaCandidato = new HistoriaStatus();
		TipoStatus statusCandidato = tipoStatusRepository.getOne(2);

		int candBusqId = candidatoBusquedaRepository.getIdCandBusq(
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaBusquedaId().getBusquedaId(),
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaCandidatoId().getCandidatoId(),
				asignarcandidatoBusqueda.getCandidatoBusqueda().getCandidatoBusquedaHunterId().getHunterId());

		CandidatoBusqueda candidatoBusqueda = candidatoBusquedaRepository.getOne(candBusqId);
		candidatoBusqueda.setCandidatoBusquedaStatusId(statusCandidato);
		candidatoBusqueda.setCandidatoBusquedaRBP(0);
		candidatoBusqueda.setCandidatoBusquedaFechaCambio(new DateUtil().timeNow());
		candidatoBusqueda.setCandidatoBusquedaActivo(1);

		candidatoBusquedaRepository.save(candidatoBusqueda);

		historiaCandidato.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
		historiaCandidato.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
		historiaCandidato.setHistoriaStatusObservaciones("Reasignado a la búsqueda");
		historiaCandidato.setHistoriaStatusFecha(new DateUtil().timeNow());

		historiaStatusRepository.save(historiaCandidato);

		if (!asignarcandidatoBusqueda.getObservacion().trim().isEmpty()) {
			HistoriaStatus historiaCandidato1 = new HistoriaStatus();
			historiaCandidato1.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
			historiaCandidato1.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
			historiaCandidato1.setHistoriaStatusObservaciones(asignarcandidatoBusqueda.getObservacion());
			historiaCandidato1.setHistoriaStatusFecha(new DateUtil().timeNow());
			historiaStatusRepository.save(historiaCandidato1);
		}
	}

	public DetalleBusqueda getDetalleBusqueda(int busquedaId) throws NotFoundException {
		Busqueda busqueda = busquedaRepository.findById(busquedaId)
				.orElseThrow(() -> new NotFoundException(CodigoError.BUSQUEDA_NOT_FOUND));

		List<Integer> huntersIds = hunterBusquedaRepository.getHuntersIdActuales(busquedaId);
		int cantCandidatos = candidatoBusquedaRepository.getCantCandidatosUnaBusqueda(busquedaId);

		List<HunterResponse> listaDehunters = new ArrayList<>();
		for (int hunterId : huntersIds) {
			Hunter esteHunter = hunterRepository.findById(hunterId).get();
			HunterResponse hunterResponse = new HunterResponse(esteHunter.getHunterId(),
					esteHunter.getUsuario().getNombre(), esteHunter.getUsuario().getApellido(),
					esteHunter.getUsuario().getEmail(), esteHunter.getHunterActivo());
			listaDehunters.add(hunterResponse);
		}

		return new DetalleBusqueda(cantCandidatos, listaDehunters, busqueda);
	}

	public void newHistoriaCandidato(HistoriaStatusCandidato historiaStatusCandidato, String HunterName,
			HttpServletRequest request) {
		HistoriaStatus historiaCandidato = new HistoriaStatus();
		LOG_info(request, "historiaStatusCandidato: " + gson.toJson(historiaStatusCandidato));

		int CandBusqID = candidatoBusquedaRepository.getIdCandBusq(
				historiaStatusCandidato.getHistoriaBusqueda().getBusquedaId(),
				historiaStatusCandidato.getHistoriaCandidato().getCandidatoId(),
				historiaStatusCandidato.getHistoriaHunter().getHunterId());

		CandidatoBusqueda candidatoBusqueda = candidatoBusquedaRepository.getOne(CandBusqID);

		TipoStatus ts = tipoStatusRepository.getOne(historiaStatusCandidato.getCandidatoStatus().getTipoStatusId());
		String status = ts.getTipoStatusDescripcion();

		if (candidatoBusqueda.getCandidatoBusquedaStatusId() != historiaStatusCandidato.getCandidatoStatus()) {
			switch (historiaStatusCandidato.getCandidatoStatus().getTipoStatusId()) {
			case 3:
			case 4:
			case 5:
				actividadService.guardarActividad(candidatoBusqueda.getCandidatoBusquedaBusquedaId().getBusquedaId(),
						candidatoBusqueda.getCandidatoBusquedaCandidatoId().getCandidatoNombre() + " "
								+ candidatoBusqueda.getCandidatoBusquedaCandidatoId().getCandidatoApellido(),
						HunterName + " cambió el status del candidato a '" + status + "'");
				break;

			case 7:
				actividadService.guardarActividad(candidatoBusqueda.getCandidatoBusquedaBusquedaId().getBusquedaId(),
						candidatoBusqueda.getCandidatoBusquedaCandidatoId().getCandidatoNombre() + " "
								+ candidatoBusqueda.getCandidatoBusquedaCandidatoId().getCandidatoApellido(),
						HunterName + " cambió el status del candidato a '" + status + "' con RB: "
								+ new LocaleUtils().formatDecimal(historiaStatusCandidato.getRbPretendida())
								+ " y la fecha de ingreso es "
								+ formatearFecha(historiaStatusCandidato.getFechaIngreso()));
				break;

			default:
				break;
			}
		}

		candidatoBusqueda.setCandidatoBusquedaStatusId(historiaStatusCandidato.getCandidatoStatus());
		if (historiaStatusCandidato.getRbPretendida() > 0) {
			candidatoBusqueda.setCandidatoBusquedaRBP(historiaStatusCandidato.getRbPretendida());
		}
		candidatoBusqueda.setCandidatoBusquedaFechaCambio(new DateUtil().timeNow());

		if (historiaStatusCandidato.getCandidatoStatus().getTipoStatusId() == 7) {

			if (historiaStatusCandidato.getFechaIngreso() != null) {
				// Asigna la fecha que el candidato ha sido seleccionado.
				candidatoBusqueda.setFechaIngreso(historiaStatusCandidato.getFechaIngreso());
			}
		}

		candidatoBusquedaRepository.save(candidatoBusqueda);

		historiaCandidato.setHistoriaCandidatoBusquedaId(candidatoBusqueda);
		historiaCandidato.setHistoriaTipoStatus(candidatoBusqueda.getCandidatoBusquedaStatusId());
		historiaCandidato.setHistoriaStatusObservaciones(historiaStatusCandidato.getHistoriaObservaciones());
		historiaCandidato.setHistoriaStatusFecha(new DateUtil().timeNow());
		historiaCandidato.setHistoriaCandidatoBusquedaRBP(historiaStatusCandidato.getRbPretendida());

		historiaStatusRepository.save(historiaCandidato);
	}

	public void desactivarCandidatoDeBusqueda(DesactivarCandidatoRequest desactivarCandidato,
			HttpServletRequest request) {

		int CandBusqID = candidatoBusquedaRepository.getIdCandBusq(desactivarCandidato.getBusquedaId(),
				desactivarCandidato.getCandidatoId(), desactivarCandidato.getHunterId());

		CandidatoBusqueda candidatoBusqueda = candidatoBusquedaRepository.findById(CandBusqID)
				.orElseThrow(() -> new NotFoundException(CodigoError.CANDIDATO_BUSQUEDA_NOT_FOUD));

		candidatoBusqueda.setCandidatoBusquedaActivo(0);
		candidatoBusqueda.setCandidatoBusquedaFechaCambio(new DateUtil().timeNow());

		candidatoBusquedaRepository.save(candidatoBusqueda);

		try {
			LOG_info(request, "Borrado hitorial del CandidatoBusqueda id: " + CandBusqID);
			historiaStatusRepository.borrarHistoriaCandidato(CandBusqID);
		} catch (Exception e) {
			LOG_info(request,
					"Error que aun no sabemos resolver al borrar el historial del candidato, pero si se eliminaron las historias");
		}

		LOG_info(request,
				"Se le ha dado de baja al candidato con id: " + desactivarCandidato.getCandidatoId()
						+ " asignado al Recruiter id: " + desactivarCandidato.getHunterId() + " en la busqueda id: "
						+ desactivarCandidato.getBusquedaId());
	}

	public void cerrarBusqueda(Busqueda busqueda, String HunterName) {
		Busqueda busquedaCerrada = busquedaRepository.getOne(busqueda.getBusquedaId());

		busquedaCerrada.setTipoEstado(busqueda.getTipoEstado());
		busquedaCerrada.setMotivoCierre(busqueda.getMotivoCierre());
		busquedaCerrada.setBusquedaFechaCierre(new DateUtil().timeNow());

		busquedaRepository.save(busquedaCerrada);
		actividadService.guardarActividad(busquedaCerrada.getBusquedaId(),
				busquedaCerrada.getPosicion() + " ( " + busquedaCerrada.getBusquedaId() + " )",
				HunterName + " cerró la búsqueda el día " + busquedaCerrada.getBusquedaFechaCierre());
	}

	public void asignarBusqueda(AsignarArchivoBusquedaRequest archivoBusqueda) throws NotFoundException {

		Busqueda busqueda = busquedaRepository.findById(archivoBusqueda.getBusquedaId())
				.orElseThrow(() -> new NotFoundException(CodigoError.BUSQUEDA_NOT_FOUND));

		List<ArchivoRequest> archivos = archivoBusqueda.getArchivosId();

		for (ArchivoRequest archivo : archivos) {

			Archivo archivoInterfaz = new Archivo();
			archivoInterfaz.setArchivoId(archivo.getArchivoId());

			ArchivoBusqueda asignar = new ArchivoBusqueda();
			asignar.setBusqueda(busqueda);
			asignar.setArchivo(archivoInterfaz);
			archivoBusquedaRepository.save(asignar);
		}

	}

	public List<Integer> findIdBusqueda(String param) {
		return candidatoBusquedaRepository.findBusquedaCandidato(param);
	}

	public Busqueda findById(int idBusqueda) throws NotFoundException {
		return busquedaRepository.findById(idBusqueda)
				.orElseThrow(() -> new NotFoundException(CodigoError.BUSQUEDA_NOT_FOUND));
	}

	public List<Archivo> findArchivosById(int idBusqueda) {
		Optional<List<ArchivoBusqueda>> archivoBusqueda = archivoBusquedaRepository
				.findAllByBusqueda_BusquedaId(idBusqueda);
		List<Archivo> archivos = new ArrayList<>();
		if (archivoBusqueda.isPresent()) {
			archivoBusqueda.get().forEach(p -> archivos.add(p.getArchivo()));
		}
		return archivos;
	}

	public void eliminarBusqueda(int busquedaId, HttpServletRequest request) {

		Busqueda busqueda = busquedaRepository.getOne(busquedaId);

		if (busqueda != null) {
			String posicion = busqueda.getPosicion();
			try {
				int a = busquedaRepository.eliminarBusqueda(busquedaId);
			} catch (NullPointerException e) {
				LOG_info(request, "Error que aun no sabemos resolver al borrar una busqueda, pero si fue eliminada");
			}

			LOG_info(request, "La búsqueda para la posición " + posicion + " ha sido eliminada. ");
		}
		else {
			LOG_info(request, "La búsqueda no existe por lo tanto no ha sido eliminada. ");
		}
	}

	public String formatearFecha(Date fecha) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fecha);
	}

	public List<Integer> obtenerCantidades(int busquedaIdActual, List<Object[]> candidatos, List<Object[]> estatus) {

		// Esta funcion devuelve >>> [cantCandidatos, enviadoCliente,
		// candidatoEntrevista]

		int cantCandidatos = 0;
		int enviadoCliente = 0;
		int candidatoEntrevista = 0;

		for (Object[] cant : candidatos) {
			int busquedaId = Integer.parseInt(String.valueOf(cant[0]));
			if (busquedaId == busquedaIdActual) {
				cantCandidatos = Integer.parseInt(String.valueOf(cant[1]));
				break;
			}
		}

		for (Object[] status : estatus) {
			int busquedaId = Integer.parseInt(String.valueOf(status[0]));
			if (busquedaId == busquedaIdActual) {
				switch (Integer.parseInt(String.valueOf(status[1]))) {
				case 3:
					enviadoCliente += 1;
					break;
				case 4:
					candidatoEntrevista += 1;
					break;
				}
			}
		}

		List<Integer> cantidades = Arrays.asList(cantCandidatos, enviadoCliente, candidatoEntrevista);

		return cantidades;
	}

	public List<Integer> buscarEntreLasBusquedas(String buscar) {

		List<Integer> resultadoEncontrados = new ArrayList<>();
		String[] palabras = buscar.split(" ");

		for (String palabra : palabras) {

			List<Integer> resultadosParaCombinar = new ArrayList<>();
			palabra = "%" + palabra + "%";

			if (palabra != " ") {

				try {
					List<Integer> resultadoEnBusquedas = busquedaRepository.getBusquedasIdByPosicion(palabra);
					if (resultadoEnBusquedas.size() > 0) {

						for (Integer rslBusqueda : resultadoEnBusquedas) {
							if (!resultadosParaCombinar.contains(rslBusqueda)) {
								resultadosParaCombinar.add(rslBusqueda);
							}
						}
					}
				} catch (Exception e) {
				}

				try {
					List<Integer> resultadoEnclientes = clienteRepository.getBusquedasIdByClienteNomb(palabra);
					if (resultadoEnclientes.size() > 0) {
						for (Integer rslCliente : resultadoEnclientes) {
							if (!resultadosParaCombinar.contains(rslCliente)) {
								resultadosParaCombinar.add(rslCliente);
							}
						}
					}
				} catch (Exception e) {
				}

				try {
					List<Integer> resultadoEnHunters = hunterRepository.getBusquedasIdByHunterNombreApellido(palabra);
					if (resultadoEnHunters.size() > 0) {
						for (Integer rslHunter : resultadoEnHunters) {
							if (!resultadosParaCombinar.contains(rslHunter)) {
								resultadosParaCombinar.add(rslHunter);
							}
						}
					}
				} catch (Exception e) {
				}

				try {
					List<Integer> resultadoEnCandidatos = candidatoRepository.getBusquedasIdByCandidato(palabra);
					if (resultadoEnCandidatos.size() > 0) {
						for (Integer rslCandidato : resultadoEnCandidatos) {
							if (!resultadosParaCombinar.contains(rslCandidato)) {
								resultadosParaCombinar.add(rslCandidato);
							}
						}
					}
				} catch (Exception e) {
				}

				if (resultadoEncontrados.isEmpty()) {

					resultadoEncontrados.addAll(resultadosParaCombinar);

				} else {

					resultadoEncontrados = filtrarLista(resultadoEncontrados, resultadosParaCombinar);

				}

			}

		}

		return resultadoEncontrados;
	}

	public List<Integer> filtrarLista(List<Integer> primeraLista, List<Integer> segundaLista) {
		List<Integer> listaResultante = new ArrayList<>();

		for (Integer numeroSegundaLista : segundaLista) {
			if (primeraLista.contains(numeroSegundaLista)) {
				listaResultante.add(numeroSegundaLista);
			}
		}

		return listaResultante;
	}

}
