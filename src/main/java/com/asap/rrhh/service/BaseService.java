package com.asap.rrhh.service;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.asap.rrhh.RRHHAplication;
import com.asap.rrhh.security.jwt.JwtProvider;
import com.asap.rrhh.security.jwt.JwtTokenFilter;
import com.google.gson.Gson;

public class BaseService {

	@Autowired
	Environment env;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	JwtTokenFilter jwtTokenFilter;

	protected static Logger LOG = LoggerFactory.getLogger(RRHHAplication.class);
	String nombreUsuario;

	protected Gson gson = new Gson();
	// private Properties config;
	private String versionValue;
	private String ambiente;

	public BaseService() {
		// config = new Properties();
		// try {
		// config.load(this.getClass().getResourceAsStream("/application-default.properties"));
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// ambiente = config.getProperty("ambiente", "");
		// versionValue = config.getProperty("version", "");
		// ambiente = env.getProperty("ambiente");
		// versionValue = env.getProperty("version");
	}

	public String getAmbiente() {
		ambiente = env.getProperty("ambiente");
		return ambiente;
	}

	public String getVersionValue() {
		versionValue = env.getProperty("version");
		return versionValue;
	}

	protected String getVersion() {
		return versionValue;
	}

	protected void LOG_info(HttpServletRequest req, String logval) {
		String token = jwtTokenFilter.getToken(req);
		nombreUsuario = jwtProvider.getNombreUsuarioFromToken(token);
		LOG.info(getAmbiente() + " @" + nombreUsuario + "-" + logval);
	}
}
