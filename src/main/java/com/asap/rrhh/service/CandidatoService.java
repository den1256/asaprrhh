package com.asap.rrhh.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asap.rrhh.inout.CandidatoAsignaciones;
import com.asap.rrhh.inout.HistoriaCandidatoStatusResponse;
import com.asap.rrhh.inout.TotalCandidatoAsignaciones;
import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.model.Candidato;
import com.asap.rrhh.model.HistoriaStatus;
import com.asap.rrhh.model.Hunter;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.repository.CandidatoBusquedaRepository;
import com.asap.rrhh.repository.CandidatoRepository;
import com.asap.rrhh.repository.HistoriaStatusRepository;
import com.asap.rrhh.repository.HunterRepository;

@Service
public class CandidatoService extends BaseService {

	@Autowired
	BusquedaRepository busquedaRepository;

	@Autowired
	CandidatoBusquedaRepository candidatoBusquedaRepository;

	@Autowired
	HunterRepository hunterRepository;

	@Autowired
	HistoriaStatusRepository historiaStatusRepository;

	@Autowired
	HunterService hunterService;

	@Autowired
	CandidatoRepository candidatoRepository;

	public TotalCandidatoAsignaciones getTotalCandidatoAsignaciones(int candidatoId) {
		TotalCandidatoAsignaciones totalAsignaciones = new TotalCandidatoAsignaciones();

		List<CandidatoAsignaciones> listaAsignaciones = new ArrayList<>();

		List<Integer> busquedaIds = candidatoBusquedaRepository.getBusquedasPorCandidatoId(candidatoId);

		for (Integer busquedaId : busquedaIds) {
			CandidatoAsignaciones asignaciones = new CandidatoAsignaciones();
			Busqueda datosBusqueda = busquedaRepository.getOne(busquedaId);
			List<Object[]> tipoStatusCandidatoEnBusqueda = new ArrayList<>();

			List<Integer> hunterIds = candidatoBusquedaRepository.getHuntersPorBusquedaYCandidato(busquedaId,
					candidatoId);
			List<HistoriaCandidatoStatusResponse> statusConHunter = new ArrayList<>();

			for (Integer hunterId : hunterIds) {
				
				tipoStatusCandidatoEnBusqueda = candidatoBusquedaRepository.getCandidatoTipoStatusDescripcion(busquedaId, candidatoId, hunterId);
				
				Hunter hunter = hunterRepository.getOne(hunterId);
				HistoriaCandidatoStatusResponse status = new HistoriaCandidatoStatusResponse();
				status.setHunterId(hunter.getHunterId());
				status.setHunterNombre(hunter.getUsuario().getNombre());
				status.setCandidatoStatus((String)tipoStatusCandidatoEnBusqueda.get(0)[0]);
				statusConHunter.add(status);
				
				
				
			}
			asignaciones.setBusquedaId(busquedaId);
			asignaciones.setBusquedaNombre(datosBusqueda.getPosicion());
			asignaciones.setStatusPorHunter(statusConHunter);
			asignaciones.setBusquedaStatus(datosBusqueda.getTipoEstado().getTipoEstadoDescripcion());
			asignaciones.setClienteNombre(datosBusqueda.getCliente().getClienteNombre());

			listaAsignaciones.add(asignaciones);
		}

		List<CandidatoAsignaciones> asignacionesOrdenadas = new ArrayList<>();
		for (int i = listaAsignaciones.size(); i > 0; i--) {
			asignacionesOrdenadas.add(listaAsignaciones.get(i - 1));
		}

		totalAsignaciones.setCantidadBusquedas(busquedaIds.size());
		totalAsignaciones.setAsignaciones(asignacionesOrdenadas);
		return totalAsignaciones;
	}

	public List<HistoriaStatus> getHistoriaStatuses(int busquedaId, int hunterId, int candidatoId) {
		List<Object[]> historiasIds = new ArrayList<>();
		List<HistoriaStatus> historias = new ArrayList<>();
		List<Object[]> candidatoHistoriaId = candidatoBusquedaRepository.getIdCandidatoStatus(busquedaId, candidatoId, hunterId);

		int candHistoriaId = Integer.parseInt(String.valueOf(candidatoHistoriaId.get(0)[0]));
		historiasIds = historiaStatusRepository.getHistoriaCandidatoByBusq(candHistoriaId);

		for (Object[] histoId : historiasIds) {
			int histId = Integer.parseInt(String.valueOf(histoId[0]));
			HistoriaStatus historia = historiaStatusRepository.getOne(histId);

			historias.add(historia);
		}

		List<HistoriaStatus> historiasOrdenadas = new ArrayList<>();
		for (int i = historias.size(); i > 0; i--)
			historiasOrdenadas.add(historias.get(i - 1));
		return historiasOrdenadas;
	}

	public String getFechaIngreso(int busquedaId, int hunterId, int candidatoId) {

		String fechaIngreso = "";

		try {
			List<Object[]> fechaBD = candidatoBusquedaRepository.getFechaDeIngreso(busquedaId, hunterId, candidatoId);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date fecha = new Date();
			fecha = (Date) fechaBD.get(0)[0];
			fechaIngreso = sdf.format(fecha);

		} catch (Exception e) {

		}

		return fechaIngreso;
	}

	public Candidato findByEmail(String email) {
		return candidatoRepository.findByCandidatoEmail(email);
	}

	public Candidato findByCelular(String celular) {
		return candidatoRepository.findByCandidatoCelularLike(celular);
	}

	public List<Candidato> buscarCandidatos(String buscar) {
		List<Candidato> candidatosEncontrados = new ArrayList<>();
		String[] palabras = buscar.split(" ");

		for (String palabra : palabras) {
			palabra = "%" + palabra + "%";

			List<Candidato> candidatos = new ArrayList<>();

			try {
				candidatos = candidatoRepository.buscarCandidatos(palabra).get();
			} catch (NoSuchElementException e) {

			}

			for (Candidato candidato : candidatos) {
				int id = candidato.getCandidatoId();

				if (!existeEnListaCandidatos(id, candidatosEncontrados)) {
					candidatosEncontrados.add(candidato);
				}
			}
		}
		return candidatosEncontrados;
	}

	public boolean existeEnListaCandidatos(int id, List<Candidato> candidatos) {
		for (Candidato candidato : candidatos) {
			if (candidato.getCandidatoId() == id) {
				return true;
			}
		}
		return false;
	}
}
