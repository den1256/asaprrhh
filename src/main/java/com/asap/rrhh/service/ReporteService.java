package com.asap.rrhh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.asap.rrhh.inout.Diferencial;
import com.asap.rrhh.repository.ReporteRepository;

public class ReporteService extends BaseService {

	@Autowired
	ReporteRepository reporteRepository;

	public List<Diferencial> getDiferencialSemanal(int busquedaId) {
		return reporteRepository.getDiferenciaSemanal(busquedaId);
	}

}
