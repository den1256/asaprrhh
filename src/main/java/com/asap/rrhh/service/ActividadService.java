package com.asap.rrhh.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asap.rrhh.Exception.NotFoundException;
import com.asap.rrhh.inout.ActividadResponse;
import com.asap.rrhh.inout.ExceptionDetail.CodigoError;
import com.asap.rrhh.model.Actividad;
import com.asap.rrhh.model.Busqueda;
import com.asap.rrhh.repository.ActividadRepository;
import com.asap.rrhh.repository.BusquedaRepository;
import com.asap.rrhh.util.DateUtil;

@Service
public class ActividadService extends BaseService {

	@Autowired
	ActividadRepository actividadRepository;

	@Autowired
	BusquedaRepository busquedaRepository;

	public ActividadResponse getActividadesBusqueda(int busquedaId) throws NotFoundException {

		Busqueda busqueda = busquedaRepository.findById(busquedaId).orElseThrow(() -> new NotFoundException(CodigoError.BUSQUEDA_NOT_FOUND));

		ActividadResponse actividades = new ActividadResponse();

		List<Actividad> actividadesBusqueda = actividadRepository.getActividadPorBusqueda(busquedaId);

		Collections.reverse(actividadesBusqueda);

		actividades.setActividadesBusqueda(actividadesBusqueda);
		actividades.setNombreBusqueda(busqueda.getPosicion());
		actividades.setNombreCliente(busqueda.getCliente().getClienteNombre());

		return actividades;
	}

	public void guardarActividad(int busquedaId, String descripcionActividad, String descripcionDetalleActividad) {

		Busqueda busqueda = busquedaRepository.getOne(busquedaId);

		actividadRepository.save(new Actividad(busqueda, descripcionActividad, descripcionDetalleActividad, new DateUtil().timeNow()));

	}

}
