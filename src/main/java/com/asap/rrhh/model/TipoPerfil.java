package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_perfil")
public class TipoPerfil {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "perfil_id")
	private int perfilId;

	@Column(name = "perfil_nombre")
	private String perfilNombre;

	public TipoPerfil() {

	}

	public TipoPerfil(int perfilId, String perfilNombre) {
		super();
		this.perfilId = perfilId;
		this.perfilNombre = perfilNombre;
	}

	@JsonView(Views.Simple.class)
	public int getPerfilId() {
		return perfilId;
	}

	public void setPerfilId(int perfilId) {
		this.perfilId = perfilId;
	}

	@JsonView(Views.Simple.class)
	public String getPerfilNombre() {
		return perfilNombre;
	}

	public void setPerfilNombre(String perfilNombre) {
		this.perfilNombre = perfilNombre;
	}

}
