package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_estado")
public class TipoEstado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tipo_estado_id")
	private int tipoEstadoId;

	@Column(name = "tipo_estado_descripcion")
	private String tipoEstadoDescripcion;

	public TipoEstado() {
		super();
	}

	public TipoEstado(int tipoEstadoId, String tipoEstadoDescripcion) {
		super();
		this.tipoEstadoId = tipoEstadoId;
		this.tipoEstadoDescripcion = tipoEstadoDescripcion;
	}

	@JsonView(Views.Simple.class)
	public int getTipoEstadoId() {
		return tipoEstadoId;
	}

	public void setTipoEstadoId(int tipoEstadoId) {
		this.tipoEstadoId = tipoEstadoId;
	}

	@JsonView(Views.Simple.class)
	public String getTipoEstadoDescripcion() {
		return tipoEstadoDescripcion;
	}

	public void setTipoEstadoDescripcion(String tipoEstadoDescripcion) {
		this.tipoEstadoDescripcion = tipoEstadoDescripcion;
	}

}
