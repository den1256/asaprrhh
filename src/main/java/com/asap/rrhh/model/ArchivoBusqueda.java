package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@Table(name = "archivo_busqueda")
public class ArchivoBusqueda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "archivo_busqueda_id")
	private int ArchivoBusquedaId;

	@ManyToOne
	@JoinColumn(name = "busqueda_id")
	private Busqueda busqueda;

	@ManyToOne
	@JoinColumn(name = "archivo_id")
	private Archivo archivo;

	@JsonView(Views.Simple.class)
	public Busqueda getBusqueda() {
		return busqueda;
	}

	@JsonView(Views.Simple.class)
	public Archivo getArchivo() {
		return archivo;
	}
}
