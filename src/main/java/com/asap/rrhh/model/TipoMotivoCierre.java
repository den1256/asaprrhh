package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_motivo_cierre")
public class TipoMotivoCierre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tipo_motivo_cierre_id")
	private int tipoMotivoCierreId;

	@Column(name = "tipo_motivo_cierre_descripcion")
	private String tipoMotivoCierreDescripcion;

	public TipoMotivoCierre() {
		super();
	}

	public TipoMotivoCierre(int tipoMotivoCierreId, String tipoMotivoCierreDescripcion) {
		super();
		this.tipoMotivoCierreId = tipoMotivoCierreId;
		this.tipoMotivoCierreDescripcion = tipoMotivoCierreDescripcion;
	}

	@JsonView(Views.Simple.class)
	public int getTipoMotivoCierreId() {
		return tipoMotivoCierreId;
	}

	public void setTipoMotivoCierreId(int tipoMotivoCierreId) {
		this.tipoMotivoCierreId = tipoMotivoCierreId;
	}

	@JsonView(Views.Simple.class)
	public String getTipoMotivoCierreDescripcion() {
		return tipoMotivoCierreDescripcion;
	}

	public void setTipoMotivoCierreDescripcion(String tipoMotivoCierreDescripcion) {
		this.tipoMotivoCierreDescripcion = tipoMotivoCierreDescripcion;
	}

}
