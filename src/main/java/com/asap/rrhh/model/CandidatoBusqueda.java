package com.asap.rrhh.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "candidato_busqueda")
public class CandidatoBusqueda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "candidato_busqueda_id")
	private int candidatoBusquedaId;

	@ManyToOne
	@JoinColumn(name = "hunter_id")
	private Hunter candidatoBusquedaHunterId;

	@ManyToOne
	@JoinColumn(name = "busqueda_id")
	private Busqueda candidatoBusquedaBusquedaId;

	@ManyToOne
	@JoinColumn(name = "candidato_id")
	private Candidato candidatoBusquedaCandidatoId;

	@ManyToOne
	@JoinColumn(name = "tipo_status_id")
	private TipoStatus candidatoBusquedaStatusId;

	@Column(name = "candidato_busqueda_fecha_cambio")
	private Timestamp candidatoBusquedaFechaCambio;

	@Column(name = "candidato_busqueda_RBP")
	private double candidatoBusquedaRBP;

	@Column(name = "candidato_busqueda_activo")
	private int candidatoBusquedaActivo;

	@Column(name = "fecha_ingreso")
	private Timestamp fechaIngreso;

	@Column(name = "candidato_busqueda_fecha_asignacion", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
	private Timestamp candidatoBusquedaFechaAsignacion;

	public CandidatoBusqueda() {

	}

	public CandidatoBusqueda(int candidatoBusquedaId, Hunter candidatoBusquedaHunterId,
			Busqueda candidatoBusquedaBusquedaId, Candidato candidatoBusquedaCandidatoId,
			TipoStatus candidatoBusquedaStatusId, Timestamp candidatoBusquedaFechaCambio, int candidatoBusquedaActivo) {
		super();
		this.candidatoBusquedaId = candidatoBusquedaId;
		this.candidatoBusquedaHunterId = candidatoBusquedaHunterId;
		this.candidatoBusquedaBusquedaId = candidatoBusquedaBusquedaId;
		this.candidatoBusquedaCandidatoId = candidatoBusquedaCandidatoId;
		this.candidatoBusquedaStatusId = candidatoBusquedaStatusId;
		this.candidatoBusquedaFechaCambio = candidatoBusquedaFechaCambio;
		this.candidatoBusquedaActivo = candidatoBusquedaActivo;
	}

	public CandidatoBusqueda(Hunter candidatoBusquedaHunterId, Busqueda candidatoBusquedaBusquedaId,
			Candidato candidatoBusquedaCandidatoId, TipoStatus candidatoBusquedaStatusId,
			Timestamp candidatoBusquedaFechaCambio, double candidatoBusquedaRBP, int candidatoBusquedaActivo,
			Timestamp fechaIngreso) {
		this.candidatoBusquedaHunterId = candidatoBusquedaHunterId;
		this.candidatoBusquedaBusquedaId = candidatoBusquedaBusquedaId;
		this.candidatoBusquedaCandidatoId = candidatoBusquedaCandidatoId;
		this.candidatoBusquedaStatusId = candidatoBusquedaStatusId;
		this.candidatoBusquedaFechaCambio = candidatoBusquedaFechaCambio;
		this.candidatoBusquedaRBP = candidatoBusquedaRBP;
		this.candidatoBusquedaActivo = candidatoBusquedaActivo;
		this.fechaIngreso = fechaIngreso;
	}

	public CandidatoBusqueda(Hunter candidatoBusquedaHunterId, Busqueda candidatoBusquedaBusquedaId,
			Candidato candidatoBusquedaCandidatoId, TipoStatus candidatoBusquedaStatusId,
			Timestamp candidatoBusquedaFechaCambio, double candidatoBusquedaRBP, int candidatoBusquedaActivo) {
		super();
		this.candidatoBusquedaHunterId = candidatoBusquedaHunterId;
		this.candidatoBusquedaBusquedaId = candidatoBusquedaBusquedaId;
		this.candidatoBusquedaCandidatoId = candidatoBusquedaCandidatoId;
		this.candidatoBusquedaStatusId = candidatoBusquedaStatusId;
		this.candidatoBusquedaFechaCambio = candidatoBusquedaFechaCambio;
		this.candidatoBusquedaRBP = candidatoBusquedaRBP;
		this.candidatoBusquedaActivo = candidatoBusquedaActivo;
	}

	@JsonView(Views.Simple.class)
	public int getCandidatoBusquedaId() {
		return candidatoBusquedaId;
	}

	public void setCandidatoBusquedaId(int candidatoBusquedaId) {
		this.candidatoBusquedaId = candidatoBusquedaId;
	}

	@JsonView(Views.Simple.class)
	public Hunter getCandidatoBusquedaHunterId() {
		return candidatoBusquedaHunterId;
	}

	public void setCandidatoBusquedaHunterId(Hunter candidatoBusquedaHunterId) {
		this.candidatoBusquedaHunterId = candidatoBusquedaHunterId;
	}

	@JsonView(Views.Simple.class)
	public Busqueda getCandidatoBusquedaBusquedaId() {
		return candidatoBusquedaBusquedaId;
	}

	public void setCandidatoBusquedaBusquedaId(Busqueda candidatoBusquedaBusquedaId) {
		this.candidatoBusquedaBusquedaId = candidatoBusquedaBusquedaId;
	}

	@JsonView(Views.Simple.class)
	public Candidato getCandidatoBusquedaCandidatoId() {
		return candidatoBusquedaCandidatoId;
	}

	public void setCandidatoBusquedaCandidatoId(Candidato candidatoBusquedaCandidatoId) {
		this.candidatoBusquedaCandidatoId = candidatoBusquedaCandidatoId;
	}

	@JsonView(Views.Simple.class)
	public TipoStatus getCandidatoBusquedaStatusId() {
		return candidatoBusquedaStatusId;
	}

	public void setCandidatoBusquedaStatusId(TipoStatus candidatoBusquedaStatusId) {
		this.candidatoBusquedaStatusId = candidatoBusquedaStatusId;
	}

	@JsonView(Views.Simple.class)
	public int getCandidatoBusquedaActivo() {
		return candidatoBusquedaActivo;
	}

	public void setCandidatoBusquedaActivo(int candidatoBusquedaActivo) {
		this.candidatoBusquedaActivo = candidatoBusquedaActivo;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getCandidatoBusquedaFechaCambio() {
		return candidatoBusquedaFechaCambio;
	}

	public void setCandidatoBusquedaFechaCambio(Timestamp candidatoBusquedaFechaCambio) {
		this.candidatoBusquedaFechaCambio = candidatoBusquedaFechaCambio;
	}

	@JsonView(Views.Simple.class)
	public double getCandidatoBusquedaRBP() {
		return candidatoBusquedaRBP;
	}

	public void setCandidatoBusquedaRBP(double candidatoBusquedaRBP) {
		this.candidatoBusquedaRBP = candidatoBusquedaRBP;
	}

	@JsonView(Views.Detail.class)
	public Timestamp getFechaIngreso() {
		return fechaIngreso;
	}

	public CandidatoBusqueda setFechaIngreso(Timestamp fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
		return this;
	}

	@JsonView(Views.Detail.class)
	public Timestamp getCandidatoBusquedaFechaAsignacion() {
		return candidatoBusquedaFechaAsignacion;
	}

	public void setCandidatoBusquedaFechaAsignacion(Timestamp candidatoBusquedaFechaAsignacion) {
		this.candidatoBusquedaFechaAsignacion = candidatoBusquedaFechaAsignacion;
	}

}
