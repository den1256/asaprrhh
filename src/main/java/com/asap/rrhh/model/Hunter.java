package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.asap.rrhh.security.entity.Usuario;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "hunter")
public class Hunter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hunter_id")
	private int hunterId;

	@Column(name = "hunter_activo")
	private int hunterActivo;

	@OneToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	private Usuario usuario;

	public Hunter(int hunterActivo , Usuario usuario) {
		this.hunterActivo = hunterActivo;
		this.usuario = usuario;
	}

	//////////////////////////// JSON VIEW'S
	@JsonView(Views.Simple.class)
	public int getHunterId() {
		return hunterId;
	}

	@JsonView(Views.Simple.class)
	public int getHunterActivo() {
		return hunterActivo;
	}

}
