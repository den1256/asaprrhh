package com.asap.rrhh.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.asap.rrhh.security.entity.Usuario;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "comercial")
public class Comercial implements Serializable {

	private static final long serialVersionUID = -7967359949661004775L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comercial_id")
	private int comercialId;

	@OneToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	private Usuario usuario;

	@Column(name = "comercial_activo")
	private int comercialActivo;

	public Comercial(int comercialActivo, Usuario usuario) {
		this.usuario = usuario;
		this.comercialActivo = comercialActivo;
	}

	public int getComercialId() {
		return comercialId;
	}

	public int getComercialActivo() {
		return comercialActivo;
	}
}
