package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_busqueda")
public class TipoBusqueda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tipo_busqueda_id")
	private int tipoBusquedaId;

	@Column(name = "tipo_busqueda_descripcion")
	private String tipoBusquedaDescripcion;

	public TipoBusqueda() {
		super();
	}

	public TipoBusqueda(int tb_id, String tb_descripcion) {
		super();
		this.tipoBusquedaId = tb_id;
		this.tipoBusquedaDescripcion = tb_descripcion;
	}

	@JsonView(Views.Simple.class)
	public int getTipoBusquedaId() {
		return tipoBusquedaId;
	}

	public void setTipoBusquedaId(int tb_id) {
		this.tipoBusquedaId = tb_id;
	}

	@JsonView(Views.Simple.class)
	public String getTipoBusquedaDescripcion() {
		return tipoBusquedaDescripcion;
	}

	public void setTipoBusquedaDescripcion(String tb_descripcion) {
		this.tipoBusquedaDescripcion = tb_descripcion;
	}

}
