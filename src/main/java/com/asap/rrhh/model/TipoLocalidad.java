package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_localidad")
public class TipoLocalidad {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "localidad_id")
	private int localidadId;

	@Column(name = "localidad_descripcion")
	private String localidadDescripcion;
	
	@Column(name = "localidad_activa", nullable = false, columnDefinition="BOOLEAN DEFAULT false")
	private Boolean localidadActiva;

	public TipoLocalidad() {
		super();
	}

	public TipoLocalidad(int localidad_id, String localidad_descripcion) {
		super();
		this.localidadId = localidad_id;
		this.localidadDescripcion = localidad_descripcion;
	}

	@JsonView(Views.Simple.class)
	public int getLocalidadId() {
		return localidadId;
	}

	public void setLocalidadId(int localidad_id) {
		this.localidadId = localidad_id;
	}

	@JsonView(Views.Simple.class)
	public String getLocalidadDescripcion() {
		return localidadDescripcion;
	}

	public void setLocalidadDescripcion(String localidad_descripcion) {
		this.localidadDescripcion = localidad_descripcion;
	}

}
