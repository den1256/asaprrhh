package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "perfil_clasificacion")
public class PerfilClasificacion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "perfil_clasificacion_id")
	private int perfilClasificacionId;

	@Column(name = "perfil_clasificacion_nombre")
	private String perfilClasificacionNombre;

	@Column(name = "perfil_id")
	private int perfilId;

	public PerfilClasificacion() {

	}

	public PerfilClasificacion(int perfilClasificacionId, String perfilClasificacionNombre, int perfilId) {
		super();
		this.perfilClasificacionId = perfilClasificacionId;
		this.perfilClasificacionNombre = perfilClasificacionNombre;
		this.perfilId = perfilId;
	}

	@JsonView(Views.Simple.class)
	public int getPerfilClasificacionId() {
		return perfilClasificacionId;
	}

	public void setPerfilClasificacionId(int perfilClasificacionId) {
		this.perfilClasificacionId = perfilClasificacionId;
	}

	@JsonView(Views.Simple.class)
	public String getPerfilClasificacionNombre() {
		return perfilClasificacionNombre;
	}

	public void setPerfilClasificacionNombre(String perfilClasificacionNombre) {
		this.perfilClasificacionNombre = perfilClasificacionNombre;
	}

	public int getPerfilId() {
		return perfilId;
	}

	public void setPerfilId(int perfilId) {
		this.perfilId = perfilId;
	}

}
