package com.asap.rrhh.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REPORTEBASE")
public class ReporteBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int Id;

	@Column(name = "martes")
	private Timestamp martes;

	@ManyToOne
	@JoinColumn(name = "cliente_id", nullable = false)
	private Candidato reporteClienteId;

	@ManyToOne
	@JoinColumn(name = "busqueda_id", nullable = false)
	private Candidato reporteBusquedaId;

	@ManyToOne
	@JoinColumn(name = "hunter_id", nullable = false)
	private Candidato reporteHunterId;

	@ManyToOne
	@JoinColumn(name = "tipo_estado_id")
	private TipoEstado reporteTipoEstadoId;

	@Column(name = "Hunteados_Sem")
	private int Hunteados_Sem;

	@Column(name = "Presentados_Sem")
	private int Presentados_Sem;

	@Column(name = "Entrevistados_Sem")
	private int Entrevistados_Sem;

	@Column(name = "Candidatos_Netos")
	private int Candidatos_Netos;

	@Column(name = "Hunteados_Mie")
	private int Hunteados_Mie;

	@Column(name = "Hunteados_Jue")
	private int Hunteados_Jue;

	@Column(name = "Hunteados_Vie")
	private int Hunteados_Vie;

	@Column(name = "Hunteados_Lun")
	private int Hunteados_Lun;

	@Column(name = "Hunteados_Mar")
	private int Hunteados_Mar;

}
