package com.asap.rrhh.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "actividad")
public class Actividad {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "actividad_id")
	private int actividadId;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "descripcion_detalle", columnDefinition = "varchar(250)", nullable = true)
	private String descripcionDetalle;

	@Column(name = "fecha_hora")
	private Timestamp fechaHora;

	@ManyToOne
	@JoinColumn(name = "busqueda_id", nullable = false)
	private Busqueda busqueda;

	public Actividad() {

	}

	public Actividad(Busqueda busqueda, String descripcion, Timestamp fechaHora) {
		super();
		this.descripcion = descripcion;
		this.fechaHora = fechaHora;
		this.busqueda = busqueda;
	}

	public Actividad(Busqueda busqueda, String descripcion, String descripcionDetalle, Timestamp fechaHora) {
		super();
		this.descripcion = descripcion;
		this.descripcionDetalle = descripcionDetalle;
		this.fechaHora = fechaHora;
		this.busqueda = busqueda;
	}

	@JsonView(Views.Simple.class)
	public int getActividadId() {
		return actividadId;
	}

	public void setActividadId(int actividadId) {
		this.actividadId = actividadId;
	}

	@JsonView(Views.Simple.class)
	public String getDescripcion() {
		return descripcion;
	}

	@JsonView(Views.Simple.class)
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getFechaHora() {
		return fechaHora;
	}

	@JsonView(Views.Simple.class)
	public String getDescripcionDetalle() {
		return descripcionDetalle;
	}

	public void setFechaHora(Timestamp fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Busqueda getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(Busqueda busqueda) {
		this.busqueda = busqueda;
	}

	public void setDescripcionDetalle(String descripcionDetalle) {
		this.descripcionDetalle = descripcionDetalle;
	}

}
