package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_fuente")
public class TipoFuente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tipo_fuente_id")
	private int tipoFuenteId;

	@Column(name = "tipo_fuente_descripcion")
	private String tipoFuenteDescripcion;

	public TipoFuente() {
		super();
	}

	public TipoFuente(int tipoFuenteId, String tipoFuenteDescripcion) {
		super();
		this.tipoFuenteId = tipoFuenteId;
		this.tipoFuenteDescripcion = tipoFuenteDescripcion;
	}

	@JsonView(Views.Simple.class)
	public int getTipoFuenteId() {
		return tipoFuenteId;
	}

	public void setTipoFuenteId(int tipoFuenteId) {
		this.tipoFuenteId = tipoFuenteId;
	}

	@JsonView(Views.Simple.class)
	public String getTipoFuenteDescripcion() {
		return tipoFuenteDescripcion;
	}

	public void setTipoFuenteDescripcion(String tipoFuenteDescripcion) {
		this.tipoFuenteDescripcion = tipoFuenteDescripcion;
	}

}
