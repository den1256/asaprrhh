package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
@Data
@Entity
@Table(name = "archivo")
public class Archivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "archivo_id")
	private int archivoId;

	private String archivoNombre;

	private String archivoTipo;

	@Lob
	private byte[] archivoDatos;

	@Column(name = "archivo_respaldado")
	private Boolean archivoRespaldado;

	public Archivo() {
		super();
	}

	public Archivo(String archivoNombre, String archivoTipo) {
		this.archivoNombre = archivoNombre;
		this.archivoTipo = archivoTipo;
	}

	public Archivo(String archivoNombre, String archivoTipo, byte[] archivoDatos) {
		this.archivoNombre = archivoNombre;
		this.archivoTipo = archivoTipo;
		this.archivoDatos = archivoDatos;
	}

	public Archivo(int archivoId, String archivoNombre, String archivoTipo, byte[] archivoDatos,
			Boolean archivoRespaldado) {
		this.archivoId = archivoId;
		this.archivoNombre = archivoNombre;
		this.archivoTipo = archivoTipo;
		this.archivoDatos = archivoDatos;
		this.archivoRespaldado = archivoRespaldado;
	}

	@JsonView(Views.Simple.class)
	public int getArchivoId() {
		return archivoId;
	}

	@JsonView(Views.Simple.class)
	public String getArchivoNombre() {
		return archivoNombre;
	}

}
