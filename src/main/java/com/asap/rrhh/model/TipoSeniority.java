package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_seniority")
public class TipoSeniority {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seniority_id")
	private int seniorityId;

	@Column(name = "seniority_descripcion")
	private String seniorityDescripcion;

	public TipoSeniority() {

	}

	public TipoSeniority(int seniorityId, String seniorityDescripcion) {
		super();
		this.seniorityId = seniorityId;
		this.seniorityDescripcion = seniorityDescripcion;
	}

	@JsonView(Views.Simple.class)
	public int getSeniorityId() {
		return seniorityId;
	}

	public void setSeniorityId(int seniorityId) {
		this.seniorityId = seniorityId;
	}

	@JsonView(Views.Simple.class)
	public String getSeniorityDescripcion() {
		return seniorityDescripcion;
	}

	public void setSeniorityDescripcion(String seniorityDescripcion) {
		this.seniorityDescripcion = seniorityDescripcion;
	}

}
