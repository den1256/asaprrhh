package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_status")
public class TipoStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tipo_status_id")
	private int tipoStatusId;

	@Column(name = "tipo_status_descripcion")
	private String tipoStatusDescripcion;

	@Column(name = "tipo_status_leyenda")
	private String tipoStatusLeyenda;

	@Column(name = "tipo_status_prioridad")
	private int tipoStatusPrioridad;

	@Column(name = "tipo_status_orden")
	private int tipoStatusOrden;

	public TipoStatus() {
		super();
	}

	public TipoStatus(int tipoStatusId, String tipoStatusDescripcion, String tipoStatusLeyenda, int tipoStatusPrioridad,
			int tipoStatusOrden) {
		super();
		this.tipoStatusId = tipoStatusId;
		this.tipoStatusDescripcion = tipoStatusDescripcion;
		this.tipoStatusLeyenda = tipoStatusLeyenda;
		this.tipoStatusPrioridad = tipoStatusPrioridad;
		this.tipoStatusOrden = tipoStatusOrden;
	}

	public TipoStatus(int tipoStatusId, String tipoStatusDescripcion, int tipoStatusOrden) {
		super();
		this.tipoStatusId = tipoStatusId;
		this.tipoStatusDescripcion = tipoStatusDescripcion;
		this.tipoStatusOrden = tipoStatusOrden;
	}

	@JsonView(Views.Simple.class)
	public int getTipoStatusId() {
		return tipoStatusId;
	}

	public void setTipoStatusId(int tipoStatusId) {
		this.tipoStatusId = tipoStatusId;
	}

	@JsonView(Views.Simple.class)
	public String getTipoStatusDescripcion() {
		return tipoStatusDescripcion;
	}

	public void setTipoStatusDescripcion(String tipoStatusDescripcion) {
		this.tipoStatusDescripcion = tipoStatusDescripcion;
	}

	public int getTipoStatusOrden() {
		return tipoStatusOrden;
	}

	public void setTipoStatusOrden(int tipoStatusOrden) {
		this.tipoStatusOrden = tipoStatusOrden;
	}

	@JsonView(Views.Simple.class)
	public String getTipoStatusLeyenda() {
		return tipoStatusLeyenda;
	}

	public void setTipoStatusLeyenda(String tipoStatusLeyenda) {
		this.tipoStatusLeyenda = tipoStatusLeyenda;
	}

	public int getTipoStatusPrioridad() {
		return tipoStatusPrioridad;
	}

	public void setTipoStatusPrioridad(int tipoStatusPrioridad) {
		this.tipoStatusPrioridad = tipoStatusPrioridad;
	}

}
