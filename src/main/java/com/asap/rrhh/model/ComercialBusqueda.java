package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "comercial_busqueda")
public class ComercialBusqueda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comercial_busqueda_id")
    private int comercialBusquedaId;

    @ManyToOne
    @JoinColumn(name = "comercial_id", nullable = false)
    private Comercial comercialId;

    @ManyToOne
    @JoinColumn(name = "busqueda_id", nullable = false)
    private Busqueda busquedaId;
    
    @Column(name = "comercial_busqueda_activo")
    private int comercialBusquedaActivo;
}
