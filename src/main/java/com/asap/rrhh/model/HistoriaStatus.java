package com.asap.rrhh.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@Table(name = "historia_status")
public class HistoriaStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "historia_status_id")
	private int historiaStatusId;

	@ManyToOne
	@JoinColumn(name = "candidato_busqueda_id")
	private CandidatoBusqueda historiaCandidatoBusquedaId;

	@ManyToOne
	@JoinColumn(name = "tipo_status_id")
	TipoStatus historiaTipoStatus;

	@Column(name = "historia_status_observaciones", columnDefinition = "TEXT")
	private String historiaStatusObservaciones;

	@Column(name = "historia_status_fecha")
	private Timestamp historiaStatusFecha;

	@Column(name = "historia_candidato_busqueda_RBP", nullable = false, columnDefinition = "double default 0")
	private double historiaCandidatoBusquedaRBP;

	public HistoriaStatus() {
		super();
	}

	public HistoriaStatus(CandidatoBusqueda historiaCandidatoBusquedaId, TipoStatus historiaTipoStatus, String historiaStatusObservaciones,
			Timestamp historiaStatusFecha, Double historiaCandidatoBusquedaRBP) {
		super();
		this.historiaCandidatoBusquedaId = historiaCandidatoBusquedaId;
		this.historiaTipoStatus = historiaTipoStatus;
		this.historiaStatusObservaciones = historiaStatusObservaciones;
		this.historiaStatusFecha = historiaStatusFecha;
		this.historiaCandidatoBusquedaRBP = historiaCandidatoBusquedaRBP;
	}

	@JsonView(Views.Simple.class)
	public int getHistoriaStatusId() {
		return historiaStatusId;
	}

	@JsonView(Views.Simple.class)
	public TipoStatus getHistoriaTipoStatus() {
		return historiaTipoStatus;
	}

	@JsonView(Views.Simple.class)
	public String getHistoriaStatusObservaciones() {
		return historiaStatusObservaciones;
	}

	@JsonView(Views.Simple.class)
	public Date getHistoriaStatusFecha() {
		return historiaStatusFecha;
	}

	@JsonView(Views.Simple.class)
	public Double getHistoriaCandidatoBusquedaRBP() {
		return historiaCandidatoBusquedaRBP;
	}

}
