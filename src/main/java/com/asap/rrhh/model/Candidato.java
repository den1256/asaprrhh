package com.asap.rrhh.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "candidato")
public class Candidato implements Serializable {

	private static final long serialVersionUID = 71410001L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "candidato_id")
	private int candidatoId;

	@Column(name = "candidato_nombre")
	private String candidatoNombre;

	@Column(name = "candidato_apellido")
	private String candidatoApellido;

	@Column(name = "candidato_email")
	private String candidatoEmail;

	@Column(name = "candidato_celular")
	private String candidatoCelular;

	@Column(name = "candidato_linkedin")
	private String candidatoLinkedin;

	@Column(name = "candidato_fecha_contacto")
	private Timestamp candidatoFechaContacto;

	@ManyToOne
	@JoinColumn(name = "tipo_fuente")
	private TipoFuente candidatoFuente;

	@ManyToOne
	@JoinColumn(name = "tipo_localidad")
	private TipoLocalidad candidatoLocalidad;

	public Candidato() {
		super();
	}

	public Candidato(int candidatoId, String candidatoNombre, String candidatoApellido, String candidatoEmail,
			String candidatoCelular, String candidatoLinkedin, Timestamp candidatoFechaContacto,
			TipoFuente candidatoFuente, TipoLocalidad candidatoLocalidad) {
		super();
		this.candidatoId = candidatoId;
		this.candidatoNombre = candidatoNombre;
		this.candidatoApellido = candidatoApellido;
		this.candidatoEmail = candidatoEmail;
		this.candidatoCelular = candidatoCelular;
		this.candidatoLinkedin = candidatoLinkedin;
		this.candidatoFechaContacto = candidatoFechaContacto;
		this.candidatoFuente = candidatoFuente;
		this.candidatoLocalidad = candidatoLocalidad;
	}

	@JsonView(Views.Simple.class)
	public int getCandidatoId() {
		return candidatoId;
	}

	public void setCandidatoId(int candidato_id) {
		this.candidatoId = candidato_id;
	}

	@JsonView(Views.Simple.class)
	public String getCandidatoNombre() {
		return candidatoNombre;
	}

	public void setCandidatoNombre(String candidato_nombre) {
		this.candidatoNombre = candidato_nombre;
	}

	@JsonView(Views.Simple.class)
	public String getCandidatoApellido() {
		return candidatoApellido;
	}

	public void setCandidatoApellido(String candidato_apellido) {
		this.candidatoApellido = candidato_apellido;
	}

	@JsonView(Views.Simple.class)
	public String getCandidatoEmail() {
		return candidatoEmail;
	}

	public void setCandidatoEmail(String candidato_email) {
		this.candidatoEmail = candidato_email;
	}

	@JsonView(Views.Simple.class)
	public String getCandidatoCelular() {
		return candidatoCelular;
	}

	public void setCandidatoCelular(String candidato_celular) {
		this.candidatoCelular = candidato_celular;
	}

	@JsonView(Views.Simple.class)
	public String getCandidatoLinkedin() {
		return candidatoLinkedin;
	}

	public void setCandidatoLinkedin(String candidato_linkedin) {
		this.candidatoLinkedin = candidato_linkedin;
	}

	@JsonView(Views.Detail.class)
	public TipoLocalidad getCandidatoLocalidad() {
		return candidatoLocalidad;
	}

	public void setCandidatoLocalidad(TipoLocalidad candidato_localidad) {
		this.candidatoLocalidad = candidato_localidad;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getCandidatoFechaContacto() {

		return candidatoFechaContacto;
	}

	public void setCandidatoFechaContacto(Timestamp candidato_fecha_contacto) {
		this.candidatoFechaContacto = candidato_fecha_contacto;
	}

	@JsonView(Views.Simple.class)
	public TipoFuente getCandidatoFuente() {
		return candidatoFuente;
	}

	public void setCandidatoFuente(TipoFuente candidatoFuente) {
		this.candidatoFuente = candidatoFuente;
	}

}