package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tipo_tecnologia")
public class TipoTecnologia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tecnologia_id")
	private int tecnologiaId;

	@Column(name = "tecnologia_nombre")
	private String tecnologiaNombre;

	public TipoTecnologia() {

	}

	public TipoTecnologia(int tecnologiaId, String tecnologiaNombre) {
		super();
		this.tecnologiaId = tecnologiaId;
		this.tecnologiaNombre = tecnologiaNombre;
	}

	@JsonView(Views.Simple.class)
	public int getTecnologiaId() {
		return tecnologiaId;
	}

	public void setTecnologiaId(int tecnologiaId) {
		this.tecnologiaId = tecnologiaId;
	}

	@JsonView(Views.Simple.class)
	public String getTecnologiaNombre() {
		return tecnologiaNombre;
	}

	public void setTecnologiaNombre(String tecnologiaNombre) {
		this.tecnologiaNombre = tecnologiaNombre;
	}

}
