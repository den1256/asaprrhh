package com.asap.rrhh.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "hunter_busqueda")
public class HunterBusqueda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hunter_busqueda_id")
	private int hunterBusquedaId;

	@ManyToOne
	@JoinColumn(name = "hunter_id")
	private Hunter hunterBusquedaHunterId;

	@ManyToOne
	@JoinColumn(name = "busqueda_id")
	private Busqueda hunterBusquedaBusquedaId;

	@Column(name = "hunter_busqueda_activo")
	private int activo;

	@Column(name = "hunter_busqueda_fecha", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
	private Timestamp hunterBusquedaFecha;

	public HunterBusqueda() {
		super();
	}

	public HunterBusqueda(int hunterBusquedaId, Hunter hunterBusquedaHunterId, Busqueda hunterBusquedaBusquedaId, int activo) {
		super();
		this.hunterBusquedaId = hunterBusquedaId;
		this.hunterBusquedaHunterId = hunterBusquedaHunterId;
		this.hunterBusquedaBusquedaId = hunterBusquedaBusquedaId;
		this.activo = activo;
	}

	public HunterBusqueda(Hunter hunterBusquedaHunterId, Busqueda hunterBusquedaBusquedaId, int activo) {
		super();
		this.hunterBusquedaHunterId = hunterBusquedaHunterId;
		this.hunterBusquedaBusquedaId = hunterBusquedaBusquedaId;
		this.activo = activo;
	}

	@JsonView(Views.Simple.class)
	public int getHunterBusquedaId() {
		return hunterBusquedaId;
	}

	public void setHunterBusquedaId(int hunterBusquedaId) {
		this.hunterBusquedaId = hunterBusquedaId;
	}

	@JsonView(Views.Simple.class)
	public Hunter getHunterBusquedaHunterId() {
		return hunterBusquedaHunterId;
	}

	public void setHunterBusquedaHunterId(Hunter hunterBusquedaHunterId) {
		this.hunterBusquedaHunterId = hunterBusquedaHunterId;
	}

	@JsonView(Views.Simple.class)
	public Busqueda getHunterBusquedaBusquedaId() {
		return hunterBusquedaBusquedaId;
	}

	public void setHunterBusquedaBusquedaId(Busqueda hunterBusquedaBusquedaId) {
		this.hunterBusquedaBusquedaId = hunterBusquedaBusquedaId;
	}

	@JsonView(Views.Simple.class)
	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getHunterBusquedaFecha() {
		return hunterBusquedaFecha;
	}

	public void setHunterBusquedaFecha(Timestamp hunterBusquedaFecha) {
		this.hunterBusquedaFecha = hunterBusquedaFecha;
	}

}
