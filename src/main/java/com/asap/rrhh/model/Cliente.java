package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cliente_id")
	private int clienteId;

	@Column(name = "cliente_nombre")
	private String clienteNombre;

	@Column(name = "cliente_contacto", columnDefinition = "varchar(20)")
	private String clienteContacto;

	@Column(name = "cliente_direccion")
	private String clienteDireccion;

	@Column(name = "cliente_activo")
	private int clienteActivo;

	public Cliente() {
		super();
	}

	public Cliente(int clienteId, String clienteNombre, String clienteContacto, String clienteDireccion,
			int clienteActivo) {
		super();
		this.clienteId = clienteId;
		this.clienteNombre = clienteNombre;
		this.clienteContacto = clienteContacto;
		this.clienteDireccion = clienteDireccion;
		this.clienteActivo = clienteActivo;
	}

	@JsonView(Views.Simple.class)
	public int getClienteId() {
		return clienteId;
	}

	public void setClienteId(int cliente_id) {
		this.clienteId = cliente_id;
	}

	@JsonView(Views.Simple.class)
	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String cliente_nombre) {
		this.clienteNombre = cliente_nombre;
	}

	@JsonView(Views.Simple.class)
	public int getClienteActivo() {
		return clienteActivo;
	}

	public void setClienteActivo(int cliente_activo) {
		this.clienteActivo = cliente_activo;
	}

	@JsonView(Views.Simple.class)
	public String getClienteContacto() {
		return clienteContacto;
	}

	public void setClienteContacto(String clienteContacto) {
		this.clienteContacto = clienteContacto;
	}

	@JsonView(Views.Simple.class)
	public String getClienteDireccion() {
		return clienteDireccion;
	}

	public void setClienteDireccion(String clienteDireccion) {
		this.clienteDireccion = clienteDireccion;
	}

}
