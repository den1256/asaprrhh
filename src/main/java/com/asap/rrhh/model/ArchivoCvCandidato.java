package com.asap.rrhh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "archivocv_candidato")
public class ArchivoCvCandidato {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "archivocv_candidato_id")
	private int archivoCvCandidatoId;

	@ManyToOne
	@JoinColumn(name = "candidato_busqueda_id")
	private CandidatoBusqueda candidatoBusquedaId;

	@ManyToOne
	@JoinColumn(name = "archivo_id")
	private Archivo archivo;

	public ArchivoCvCandidato(CandidatoBusqueda candidatoBusquedaId, Archivo archivo) {
		super();
		this.candidatoBusquedaId = candidatoBusquedaId;
		this.archivo = archivo;
	}

}
