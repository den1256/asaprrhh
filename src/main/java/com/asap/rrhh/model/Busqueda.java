package com.asap.rrhh.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.asap.rrhh.inout.Views;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@Table(name = "busqueda")
public class Busqueda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "busqueda_id")
	private int busquedaId;

	@ManyToOne
	@JoinColumn(name = "tipo_busqueda_id")
	private TipoBusqueda tipoBusqueda;

	@ManyToOne
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "tipo_estado_id")
	private TipoEstado tipoEstado;

	@ManyToOne
	@JoinColumn(name = "tipo_motivo_cierre_id")
	private TipoMotivoCierre motivoCierre;

	@Column(name = "busqueda_posicion")
	private String posicion;

	@Column(name = "busqueda_rbp")
	private double busquedaRbp;

	@ManyToOne
	@JoinColumn(name = "seniority")
	private TipoSeniority busquedaTipoSeniority;

	@ManyToOne
	@JoinColumn(name = "tecnologia")
	private TipoTecnologia busquedaTipoTecnologia;

	@ManyToOne
	@JoinColumn(name = "perfil")
	private TipoPerfil busquedaTipoPerfil;

	@ManyToOne
	@JoinColumn(name = "perfil_clasificacion")
	private PerfilClasificacion busquedaPerfilClasificacion;

	// @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "busqueda_fecha_inicio"/* ,columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP" */)
	private Timestamp busquedaFechaInicio;

	@Column(name = "busqueda_fecha_cierre")
	private Timestamp busquedaFechaCierre;

	@Column(name = "busqueda_observaciones", columnDefinition = "TEXT")
	private String busquedaObservaciones;

	@Column(name = "cantidad_candidatos_seleccionados", columnDefinition = "int default 0", nullable = false)
	private int cantidadCandidatosSeleccionados;

	@Column(name = "cantidad_vacantes", columnDefinition = "int default 0", nullable = false)
	private int cantidadVacantes;

	public Busqueda() {
		super();
	}

	public Busqueda(int busquedaId, TipoBusqueda tipoBusqueda, Cliente cliente, TipoEstado tipoEstado,
			TipoMotivoCierre motivoCierre, String posicion, double busquedaRbp, TipoSeniority busquedaTipoSeniority,
			TipoTecnologia busquedaTipoTecnologia, TipoPerfil busquedaTipoPerfil,
			PerfilClasificacion busquedaPerfilClasificacion, Timestamp busquedaFechaInicio,
			Timestamp busquedaFechaCierre, String busquedaObservaciones) {
		super();
		this.busquedaId = busquedaId;
		this.tipoBusqueda = tipoBusqueda;
		this.cliente = cliente;
		this.tipoEstado = tipoEstado;
		this.motivoCierre = motivoCierre;
		this.posicion = posicion;
		this.busquedaRbp = busquedaRbp;
		this.busquedaTipoSeniority = busquedaTipoSeniority;
		this.busquedaTipoTecnologia = busquedaTipoTecnologia;
		this.busquedaTipoPerfil = busquedaTipoPerfil;
		this.busquedaPerfilClasificacion = busquedaPerfilClasificacion;
		this.busquedaFechaInicio = busquedaFechaInicio;
		this.busquedaFechaCierre = busquedaFechaCierre;
		this.busquedaObservaciones = busquedaObservaciones;
	}

	@JsonView(Views.Simple.class)
	public int getBusquedaId() {
		return busquedaId;
	}

	@JsonView(Views.Detail.class)
	public TipoBusqueda getTipoBusqueda() {
		return tipoBusqueda;
	}

	@JsonView(Views.Detail.class)
	public Cliente getCliente() {
		return cliente;
	}

	@JsonView(Views.Detail.class)
	public TipoEstado getTipoEstado() {
		return tipoEstado;
	}

	@JsonView(Views.Simple.class)
	public String getPosicion() {
		return posicion;
	}

	@JsonView(Views.Simple.class)
	public double getBusquedaRbp() {
		return busquedaRbp;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getBusquedaFechaInicio() {
		return busquedaFechaInicio;
	}

	@JsonView(Views.Simple.class)
	public Timestamp getBusquedaFechaCierre() {
		return busquedaFechaCierre;
	}

	@JsonView(Views.Simple.class)
	public String getBusquedaObservaciones() {
		return busquedaObservaciones;
	}

	@JsonView(Views.Detail.class)
	public TipoMotivoCierre getMotivoCierre() {
		return motivoCierre;
	}

	@JsonView(Views.Detail.class)
	public TipoSeniority getBusquedaSeniority() {
		return busquedaTipoSeniority;
	}

	@JsonView(Views.Detail.class)
	public TipoTecnologia getBusquedaTecnologia() {
		return busquedaTipoTecnologia;
	}

	@JsonView(Views.Detail.class)
	public TipoPerfil getBusquedaPerfil() {
		return busquedaTipoPerfil;
	}

	@JsonView(Views.Detail.class)
	public PerfilClasificacion getBusquedaPerfilClasificacion() {
		return busquedaPerfilClasificacion;
	}

	@JsonView(Views.Detail.class)
	public int getCantidadCandidatosSeleccionados() {
		return cantidadCandidatosSeleccionados;
	}

	@JsonView(Views.Detail.class)
	public int getCantidadVacantes() {
		return cantidadVacantes;
	}
}
